// Copyright (C) 2019, 2020, 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <getopt.h>
#include <unistd.h> // close()
#include <netinet/in.h>
#include <sys/socket.h>
#include "threadjobrunner.h"
#include "parameters.cpp"
#include "outputformat.h"
#include "tcp.h"

const int MAX_QUERY_RESPONSE = 262144;

int main(int argc, char **argv) {

    int X = 0, Y = 0, WORLD_SEED = 0;
    std::vector<int> Xs, Ys;
    bool OPTION_PRETTY = false;
    bool OPTION_FULL = false;
    bool OPTION_INFO = false;
    bool OPTION_SERIALIZE = false;
    bool USE_SERVER = false;
    int PORT = 8089;

    int choice;
    while (1)
    {
        static struct option long_options[] =
        {
            /* Use flags like so:
               {"verbose", no_argument, &verbose_flag, 'V'}*/
            /* Argument styles: no_argument, required_argument, optional_argument */
            {"version",  no_argument, 0, 'v'},
            {"help",     no_argument, 0, 'h'},
            {"info",     no_argument, 0, 'i'},
            {"pretty",   no_argument, 0, 'p'},
            {"full",     no_argument, 0, 'f'},
            {"serialize",no_argument, 0, 's'},
            {"port",     required_argument, 0, 'P'},

            {0,0,0,0}
        };

        int option_index = 0;

        /* Argument parameters:
no_argument: " "
required_argument: ":"
optional_argument: "::" */

        choice = getopt_long( argc, argv, "vhipfsP:",
                long_options, &option_index);

        if (choice == -1)
            break;

        switch( choice )
        {
            case 'v':
                printf("aagrinder-terrain version %s\n", AAGRINDER_TERRAIN_VERSION.c_str());
                return EXIT_SUCCESS;
                break;

            case 'h':

                printf("AAGRINDER-terrain  Copyright (C) 2019, 2020, 2024  MRAAGH\n");
                printf("This program comes with ABSOLUTELY NO WARRANTY\n");
                printf("This is free software, and you are welcome to redistribute it\n");
                printf("under certain conditions; for details see:\n");
                printf("https://www.gnu.org/licenses/agpl-3.0-standalone.html\n");
                printf("The source code for this program can be found at:\n");
                printf("https://gitlab.com/MRAAGH/aagrinder-terrain\n");
                printf("libnoise, which can be found at http://libnoise.sourceforge.net/\n");
                printf("and is licensed under GNU Lesser General Public License\n");
                printf("is used and included in this project.\n\n\n");

                printf("Usage: %s [--version] [--help] [--pretty] [--full] [--serialize] [--info] [--port <port>] -- X Y WORLDSEED [X2] [Y2] [X3] [Y3] ...\n", argv[0]);
                printf("\n");
                printf("-v, --version    print version and exit\n\n");
                printf("-h, --help       print this help and exit\n\n");
                printf("-p, --pretty     show blocks of chunk\n\n");
                printf("-f, --full       show blocks of chunk and margin\n\n");
                printf("-s, --serialize  output serialized chunk data\n\n");
                printf("-i, --info       output statistical information\n\n");
                printf("-P, --port       connect to daemon on port\n\n");
                printf("X                chunk x coordinate\n\n");
                printf("Y                chunk y coordinate\n\n");
                printf("WORLDSEED        world seed\n\n");
                printf("X2               additional chunk x coordinate\n\n");
                printf("Y2               additional chunk y coordinate\n\n");
                printf("The \"--\" allows you to use negative coordinates\n");
                printf("and should be considered mandatory.\n\n");
                return EXIT_SUCCESS;
                break;

            case 'i':
                OPTION_INFO = true;
                break;

            case 'p':
                OPTION_PRETTY = true;
                break;

            case 'f':
                OPTION_FULL = true;
                break;

            case 's':
                OPTION_SERIALIZE = true;
                break;

            case 'P':
                USE_SERVER = true;
                PORT = atoi(optarg);
                break;

            case '?':
                /* getopt_long will have already printed an error */
                break;

            default:
                /* Not sure how to get here... */
                return EXIT_FAILURE;
        }
    }

    /* Deal with non-option arguments here */
    int which = 0;
    while ( optind < argc )
    {
        // first is main X
        if(which == 0) X = atoi(argv[optind]);
        // second is main Y
        else if(which == 1) Y = atoi(argv[optind]);
        // third is world seed (shared)
        else if(which == 2) WORLD_SEED = atoi(argv[optind]);
        // remaining odd-indexed are additional Xs
        else if(which % 2 == 1) Xs.push_back(atoi(argv[optind]));
        // remaining even-indexed are additional Ys
        else Ys.push_back(atoi(argv[optind]));

        optind++;
        which++;
    }
    if(which % 2 != 1 || which < 3){
        fprintf(stderr, "Usage: %s [--version] [--help] [--pretty] [--full] [--serialize] [--info] [--port <port>] -- X Y WORLDSEED [X2] [Y2] [X3] [Y3] ...\n", argv[0]);
        return EXIT_FAILURE;
    }

    if (USE_SERVER) {

        if (OPTION_FULL || OPTION_FULL || OPTION_INFO || OPTION_PRETTY) {
            fprintf(stderr, "Invalid flags! The only compatible flag with -P is -s\n");
            return EXIT_FAILURE;
        }

        if (!OPTION_SERIALIZE) {
            fprintf(stderr, "Invalid flags! -P requires -s\n");
            return EXIT_FAILURE;
        }

        // creating socket
        int sockfd = socket(AF_INET, SOCK_STREAM, 0);

        // specifying address
        sockaddr_in serverAddress;
        serverAddress.sin_family = AF_INET;
        serverAddress.sin_port = htons(PORT);
        serverAddress.sin_addr.s_addr = INADDR_ANY;

        /* int maxbuffsize = 131072; */
        /* setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &maxbuffsize, sizeof(maxbuffsize)); */

        // sending connection request
        connect(sockfd, (struct sockaddr*)&serverAddress,
                sizeof(serverAddress));

        Tcp tcp(MAX_QUERY_RESPONSE);

        // sending data
        std::stringstream ss;
        ss << "c " << WORLD_SEED << " " << X << " " << Y;
        for (size_t i = 0; i < Xs.size(); i++) {
            ss << " " << Xs[i] << " " << Ys[i];
        }
        std::string strquery = ss.str();
        tcp.send_string(sockfd, strquery);

        int n_packets = Xs.size()+1;
        bool chunkcomma = false;
        std::cout << '[';
        for (int i = 0; i < n_packets; i++) {
            if (chunkcomma) std::cout << ",";
            chunkcomma = true;
            // receive response for 1 chunk
            const char *buff = tcp.receive(sockfd);
            std::cout << buff;
        }
        std::cout << ']' << std::endl;

        // closing socket
        close(sockfd);

        return 0;
    }

    std::vector<ChunkJob*> chunk_jobs;

    {  // first chunk job
        ChunkJob *chunk_job = new ChunkJob;
        chunk_job->X = X;
        chunk_job->Y = Y;
        chunk_job->WORLD_SEED = WORLD_SEED;
        chunk_jobs.push_back(chunk_job);
    }

    for (size_t i = 0; i < Xs.size(); i++) {
        // additional chunk jobs
        ChunkJob *chunk_job = new ChunkJob;
        chunk_job->X = Xs[i];
        chunk_job->Y = Ys[i];
        chunk_job->WORLD_SEED = WORLD_SEED;
        chunk_jobs.push_back(chunk_job);
    }

    std::vector<Stage1Job*> stage1_jobs;
    processChunkJobs(chunk_jobs, stage1_jobs);

    print(&std::cout, chunk_jobs, OPTION_FULL, OPTION_PRETTY, OPTION_SERIALIZE, OPTION_INFO);

    for (auto &j : stage1_jobs) delete j;

    for (auto &cj : chunk_jobs) delete cj;

    return EXIT_SUCCESS;
}
