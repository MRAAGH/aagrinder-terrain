// Copyright (C) 2019, 2020, 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "outputformat.h"

void serializeChunk(std::ostream *out, ChunkJob* cj) {
    // start of JSON
    (*out) << "{\"x\":" << cj->X
        << ",\"y\":" << cj->Y
        << ",\"t\":\"";
    // terrain as string
    for (int i = 128; i < 384; ++i) {
        for (int j = 128; j < 384; ++j) {
            (*out) << block_codes.at((*cj->blocks)[i][j]).first;
        }
    }
    // middle of JSON
    (*out) << "\",\"b\":\"";
    for (int i = 16; i < 48; ++i) {
        for (int j = 32; j < 96; ++j) {
            (*out) << biome_props.at((*cj->biome_map)[i][j]).serial_code;
        }
    }
    // middle of JSON
    (*out) << "\",\"e\":[";
    // tile entities
    int minx = (cj->X*256);
    int miny = (cj->Y*256);
    int maxx = minx+256;
    int maxy = miny+256;
    bool comma = false;
    for (auto it : cj->tileEntities) {
        if (it->x >= minx && it->x < maxx
                && it->y >= miny && it->y < maxy) {
            if (comma) (*out) << ',';
            comma = true;
            it->print(out);
        }
    }
    // middle of JSON
    (*out) << "],\"w\":[";
    // water updates
    comma = false;
    for (auto it : cj->waterUpdates) {
        if (it->x >= minx && it->x < maxx
                && it->y >= miny && it->y < maxy) {
            if (comma) (*out) << ',';
            comma = true;
            (*out) << "{\"x\":" << it->x
                << ",\"y\":" << it->y
                << "}";
        }
    }
    // middle of JSON
    (*out) << "],\"c\":[";
    // water updates
    comma = false;
    for (auto it : cj->claims) {
        if (it->x2 >= minx && it->x < maxx
                && it->y2 >= miny && it->y < maxy) {
            if (comma) (*out) << ',';
            comma = true;
            (*out) << "{\"x\":" << it->x
                << ",\"y\":" << it->y
                << ",\"x2\":" << it->x2
                << ",\"y2\":" << it->y2
                << ",\"p\":\" four forg\"}";
        }
    }
    // end of JSON
    (*out) << "]}";
}

void print (
        std::ostream *out,
        std::vector<ChunkJob*> &chunk_jobs,
        bool OPTION_FULL,
        bool OPTION_PRETTY,
        bool OPTION_SERIALIZE,
        bool OPTION_INFO
        ) {

    if (OPTION_FULL) {
        for (auto cj : chunk_jobs) {
        for (int i = 511; i >= 0; --i) {
            for (int j = 100; j < 270; ++j) {
                (*out) << block_codes.at((*cj->blocks)[i][j]).second;
            }
            (*out) << '\n';
        }
        }
    }

    if (OPTION_PRETTY) {
        for (auto cj : chunk_jobs) {
        for (int i = 383; i >= 128; --i) {
            for (int j = 128; j < 385; ++j) {
                (*out) << block_codes.at((*cj->blocks)[i][j]).second;
            }
            (*out) << '\n';
        }
    }
    }

    if (OPTION_SERIALIZE) {
        (*out) << '[';
        bool chunkcomma = false;
        for (auto cj : chunk_jobs) {
            if (chunkcomma) (*out) << ",";
            chunkcomma = true;
            serializeChunk(out, cj);
        }
        (*out) << "]";
    }

    if (OPTION_INFO) {
        (*out) << "[";
        bool comma = false;
        for (auto cj : chunk_jobs) {
            (*out) << (comma ? ",\n" : "\n");
            (*out) << " {\n";
            (*out) << "  \"chunkx\": " << cj->X << ",\n";
            (*out) << "  \"chunky\": " << cj->Y << ",\n";
            for (auto info : cj->infos) {
                (*out) << "  \"" << info.name << "\": {\"x\": " << info.x << ", \"y\": " << info.y << "},\n";
            }
            (*out) << "  \"seed\": " << cj->WORLD_SEED << "\n";
            (*out) << " }";
            comma = true;
        }
        (*out) << "\n]\n";
    }

}
