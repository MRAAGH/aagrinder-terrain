// Copyright (C) 2019, 2020, 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "perlin2.h"

Perlin2::Perlin2(noise::module::Perlin *perlin){
    this->perlin = perlin;
}

Perlin2::~Perlin2(){}

double Perlin2::GetValue(double a, double b, double c){
    double v1 = perlin->GetValue(a, b, c);
    return v1*1.5;
}
