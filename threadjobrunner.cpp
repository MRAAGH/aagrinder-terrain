// Copyright (C) 2019, 2020, 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "threadjobrunner.h"
#include <thread>
#include <mutex>
#include "noise/module/perlin.h"
#include "perlin2.h"
#include "terrain.h"

// wrapper for generate_stage1 from terrain.cpp
void start_thread_stage1(std::mutex &MTX, Perlin2 *perlin2, Stage1Job *&stage1_job_unsafe) {
    // make local copy of pointer
    Stage1Job *stage1_job = stage1_job_unsafe;
    // now allow main thread to continue
    MTX.unlock();
    // generation in this thread
    generate_stage1(perlin2, stage1_job);
}

// wrapper for generate_chunk from terrain.cpp
void start_thread_chunk(std::mutex &MTX, Perlin2 *perlin2, std::vector<Stage1Job*> &stage1_jobs, ChunkJob *&chunk_job_unsafe) {
    // make local copy of pointer
    ChunkJob *chunk_job = chunk_job_unsafe;
    // now allow main thread to continue
    MTX.unlock();
    // generation in this thread
    generate_chunk(perlin2, stage1_jobs, chunk_job);
}

// chunk_jobs: vector of jobs that will be processed. Output is stored in each job.
// stage1_jobs: vector of known/cached stage1 job results, which may be re-used while processing chunks. As a side effect, additional entries may be added into stage1_jobs, to be cached for future use.
void processChunkJobs(
        std::vector<ChunkJob*> &chunk_jobs,
        std::vector<Stage1Job*> &stage1_jobs) {

    if (chunk_jobs.size() < 1) return;

    const int WORLD_SEED = chunk_jobs[0]->WORLD_SEED;

    noise::module::Perlin *perlin = new noise::module::Perlin();
    perlin->SetSeed(WORLD_SEED);
    perlin->SetOctaveCount(4);
    Perlin2 *perlin2 = new Perlin2(perlin);

    std::vector<Stage1Job*> fresh_stage1_jobs;

    for (auto &chunk_job : chunk_jobs) {
        for (int dY = 0; dY <= 1; dY++) {
            for (int dX = 0; dX <= 1; dX++) {
                int jX = chunk_job->X + dX;
                int jY = chunk_job->Y + dY;
                // check if this stage1 job already exists
                bool job_exists = false;
                for (auto &job : stage1_jobs) {
                    if (job->X == jX && job->Y == jY
                            && job->WORLD_SEED == chunk_job->WORLD_SEED) {
                        // this job already exists.
                        // no need for duplicate job
                        job_exists = true;
                        break;
                    }
                }
                // skip this job because it would be a duplicate
                if (job_exists) continue;
                Stage1Job *stage1_job = new Stage1Job;
                stage1_job->X = jX;
                stage1_job->Y = jY;
                stage1_job->WORLD_SEED = WORLD_SEED;
                stage1_jobs.push_back(stage1_job);
                fresh_stage1_jobs.push_back(stage1_job);
            }
        }
    }

    std::vector<std::thread> stage1_threads;
    std::mutex MTX;

    for (auto &stage1_job : fresh_stage1_jobs) {
        MTX.lock();
        // start new thread
        stage1_threads.push_back(std::thread([&]{
                    start_thread_stage1(MTX, perlin2, stage1_job);}));
        // now wait until the thread has finished initializing:
        MTX.lock();
        MTX.unlock();
    }

    for (auto &thread : stage1_threads) {
        thread.join();
    }

    std::vector<std::thread> chunk_threads;

    for (auto &chunk_job : chunk_jobs) {
        MTX.lock();
        // start new thread
        chunk_threads.push_back(std::thread([&]{
                    start_thread_chunk(MTX, perlin2, stage1_jobs, chunk_job);}));
        // now wait until the thread has finished initializing:
        MTX.lock();
        MTX.unlock();
    }

    for (auto &thread : chunk_threads) {
        thread.join();
    }

    delete perlin;
    delete perlin2;

}
