#!/bin/bash
./aagrinderterrain -p -- $1 $2 $3 >example.txt \
&& convert -size 3700x7448 xc:black -gravity center -font "DejaVu-Sans-Mono" -pointsize 24 \
-fill '#7f7f7f' -draw "text 0,0 \"$(<example.txt sed 's/[^B]/ /g' )\"" \
-fill '#1a8239' -draw "text 0,0 \"$(<example.txt sed 's/[^A]/ /g' )\"" \
-fill '#91702a' -draw "text 0,0 \"$(<example.txt sed 's/[^H]/ /g' )\"" \
-fill '#2faebf' -draw "text 0,0 \"$(<example.txt sed 's/[^D]/ /g' )\"" \
-fill '#34962a' -draw "text 0,0 \"$(<example.txt sed 's/[B|A|H|D]/ /g' )\"" \
image_x${1}_y${2}_w${3}.png

