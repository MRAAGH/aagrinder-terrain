// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CELLULARRAW_H
#define CELLULARRAW_H

#include <random>
#include <vector>
#include "noise/module/perlin.h"

class CellularRaw
{
    private:
        std::vector<std::vector<bool>> *map, *map2;
        std::uint64_t seed;
        std::mt19937_64 gen;
        std::uniform_real_distribution<double> dis;

    public:

        CellularRaw(
                std::vector<std::vector<bool>> *map,
                std::uint64_t seed
                );

        virtual ~CellularRaw();

        std::vector<std::vector<bool>>* iteration(
                const std::vector<double> &probstrue,
                const std::vector<double> &probsfalse
                );

        std::vector<std::vector<bool>>* iteration_weighted(
                const std::vector<double> &probstrue,
                const std::vector<double> &probsfalse,
                const std::vector<double> &weights
                );

};

#endif /* CELLULARRAW_H */

