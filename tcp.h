// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TCP_H
#define TCP_H

#include <string>
#include <cstdint>
/* #include <unistd.h>  // read(), write() */

class Tcp
{
    public:
        Tcp(int buffer_size);
        ~Tcp();

        void send_string(int fd, std::string buff);

        const char* receive(int fd);

    private:

        static uint64_t extract_uint64(char *buff);

        void send_buffer(int fd, const char *buff, uint64_t buff_len);

        int m_buffer_size;
        char* m_buffer;

};

#endif /* TCP_H */
