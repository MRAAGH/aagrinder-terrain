// Copyright (C) 2019, 2020, 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../block.h"
#include <vector>
#include <array>

#ifndef STRUCTURES_H
#define STRUCTURES_H

struct FortConnection {
    int offsetx, offsety; // offset describes position of next attachment point relative to top left corner
    int orientation;
};

struct FortSection {
    std::vector<std::vector<Block>> blocks;
    int orientation;
    int offsetx, offsety; // offset describes position of attachment point relative to top left corner
    int mindepth, maxdepth;
    int limit;
    double mindensity, maxdensity;
    std::vector<FortConnection> connections;
};

extern const int WARP_STRUCTURE_OFFSET_X;
extern const int WARP_STRUCTURE_OFFSET_Y;
extern const std::vector<std::vector<Block>> WARP_STRUCTURE;
extern const std::vector<FortSection> FORT_SECTIONS;
extern const std::vector<std::vector<Block>> SKYBLOCK_STRUCTURE;
extern const std::vector<int> GROUND_BUSHES;
extern const std::vector<int> CEILING_BUSHES;
extern const std::vector<std::array<std::array<Block,7>,4>> BUSH_STRUCTURES;
extern const std::vector<int> SURFACE_BOULDERS;
extern const std::vector<int> CAVE_BOULDERS;
extern const std::vector<int> OCEAN_BOULDERS;
extern const std::vector<int> UNDERGROUND_BOULDERS;
extern const std::vector<int> BLOCKADE_BOULDERS;
extern const std::vector<std::array<std::array<Block,9>,9>> BOULDER_STRUCTURES;
extern const std::vector<std::array<std::array<Block,13>,18>> PINE_STRUCTURES;
extern const std::vector<std::array<std::array<Block,13>,18>> PALM_STRUCTURES;
extern const std::vector<std::array<std::array<Block,13>,18>> OASISTREE_STRUCTURES;
extern const std::vector<std::array<std::array<Block,13>,18>> TENTACLE_STRUCTURES;
extern const std::vector<std::array<std::array<Block,10>,18>> ROCK_TENTACLE_STRUCTURES;
extern const std::vector<std::array<std::array<Block,13>,18>> DEADTREE_STRUCTURES;
extern const std::vector<std::array<std::array<Block,37>,37>> DIRT_STRUCTURES;
extern const std::vector<std::array<std::array<Block,37>,37>> DESERT_PILLAR_STRUCTURES;
extern const std::array<std::array<Block,7>,7> EYE_WARP;
extern const std::vector<std::array<std::array<Block,9>,5>> EYES_RUBY;
extern const std::vector<std::array<std::array<Block,9>,9>> MAGNETITE_STRUCTURES;
extern const std::vector<std::array<std::array<Block,9>,9>> GRAPHITE_STRUCTURES;
extern const std::array<std::array<Block,45>,20> SHIP_FOREST;
extern const std::array<std::array<Block,45>,20> SHIP_DESERT;
extern const std::array<std::array<Block,45>,20> SHIP_FROZEN;
extern const int SHIP_OFFSET_Y;

#endif /* STRUCTURES_H */
