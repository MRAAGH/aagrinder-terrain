// Copyright (C) 2019, 2020 MRAAGH

/*
 * This file is licensed under a
 * Creative Commons Attribution 4.0 International License.
 *
 * You should have received a copy of the license along with this
 * work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
 */

#include "structures.h"

#define B Block::B,
#define d Block::d,
#define c Block::WB,
#define _ Block::VOID,
#define a Block::air,
#define W Block::WW,
#define w Block::water,
#define R Block::R,
#define I Block::WI,
#define H Block::Ha,
#define A Block::Aa,
#define h Block::chest,
#define V Block::grass10,
#define G Block::grass0,
#define g Block::grass12,

const int WARP_STRUCTURE_OFFSET_X = 12;
const int WARP_STRUCTURE_OFFSET_Y = 10;
const std::vector<std::vector<Block>> WARP_STRUCTURE {

    { _ _ _ _ _ _ _ _ _ a a a a a a a _ _ _ _ _ _ _ _ _ },
    { _ _ _ _ _ _ _ a a a a a c a a a a a _ _ _ _ _ _ _ },
    { _ _ _ _ _ a a a a a a c B c a a a a a a _ _ _ _ _ },
    { _ _ _ _ a a a a a B B c c B B c a a a a a _ _ _ _ },
    { _ _ _ a a a a B c c B c R c B B c c a a a a _ _ _ },
    { _ _ _ a a a a a a B c B I c B c a a a a a a _ _ _ },
    { _ _ _ a a a B B c B B c B c B c B B B a a a _ _ _ },
    { _ _ _ B B B c B B c c B c c c B c B B c B B _ _ _ },
    { _ _ B B B B B c B a a a a a a a B c c B B B B _ _ },
    { _ _ B B B a a a a a a a a a a a a a a a B B B _ _ },
    { _ _ _ B a a a a a a a a W a a a a a a a a B _ _ _ },
    { _ _ _ a a a a a a a a B c B a a a a a a a a _ _ _ },
    { _ _ a a a a a a a B B c c B B B a a a a a a a _ _ },
    { _ B B B B c B c c B B c B c c B c c B c B B B B _ },
    { B B B B B B B B B c c B B B c c B B c B B B B B B },
    { _ B B B B B B c B B B B B B B B B c B B B B B B _ },
    { _ B _ B _ B B B B B _ B B B B _ B B B B _ B _ B _ },
    { _ _ _ B _ _ B B B _ _ B B _ B _ B _ B B _ _ _ B _ },
    { _ _ _ _ _ _ B _ B _ _ _ B _ B _ _ _ B _ _ _ _ _ _ },
    { _ _ _ _ _ _ _ _ B _ _ _ B _ _ _ _ _ _ _ _ _ _ _ _ },

};

const std::vector<std::vector<Block>> SKYBLOCK_STRUCTURE {

    { _ _ _ _ _ _ _ _ _ _ _ A A A _ _ _ _ },
    { _ _ _ _ _ _ _ _ _ _ A A H A A _ _ _ },
    { _ _ _ _ _ _ _ _ _ _ A A H A A A A _ },
    { _ _ _ _ _ _ _ _ _ _ _ _ _ H A H A A },
    { _ _ _ _ _ _ _ _ _ A A A A A H A A A },
    { _ _ _ _ _ _ _ _ _ A H H H A H _ _ _ },
    { _ _ _ _ _ _ _ _ _ _ A H A A H A _ _ },
    { _ _ _ _ _ _ A A A A A A H H A A _ _ },
    { _ _ _ _ A A A H H H H A H A A _ _ _ },
    { _ _ _ _ _ A A A H A A H A _ _ _ _ _ },
    { _ _ _ _ _ _ _ _ _ H H _ _ _ _ _ _ _ },
    { _ _ _ _ _ _ _ _ H H _ _ _ _ _ _ _ _ },
    { _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ _ _ },
    { _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ _ _ },
    { _ h G G G G G G H G g _ _ _ _ _ _ _ },
    { _ d d d d d d d d d V _ _ _ _ _ _ _ },
    { _ d d d d w d d d d V _ _ _ _ _ _ _ },
    { _ d d d d d d d d d _ _ _ _ _ _ _ _ },
    { _ d d d d d d d d d _ _ _ _ _ _ _ _ },

};

