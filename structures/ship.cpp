// Copyright (C) 2024 MRAAGH

/*
 * This file is licensed under a
 * Creative Commons Attribution 4.0 International License.
 *
 * You should have received a copy of the license along with this
 * work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
 */

#include "structures.h"

#define H Block::Ha,
#define h Block::Hb,
#define A Block::Ab,
#define S Block::sand,
#define c Block::chest,
#define V Block::grass10,
#define _ Block::VOID,
#define a Block::air,
#define w Block::water,
#define i Block::stick,
#define T Block::icicle,
#define I Block::iceblock,
#define n Block::snow,
#define N Block::snowblock,
#define b Block::transform_into_b,

const int SHIP_OFFSET_Y = 3;

const std::array<std::array<Block,45>,20> SHIP_FOREST {{

    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ A A _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H H H A _ _ _ _ _ _ A A _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ A H _ _ _ _ _ H H H A _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ V _ H _ _ _ H _ _ A H _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ V _ H _ _ _ V _ _ _ A _ _ _ _ _ _ _ _ _ _ _ _ _ H H _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ H _ H _ _ _ _ _ _ H A _ _ V _ _ _ H _ _ _ _ _ _ _ _ _ _ _ _ H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ A H _ _ V _ _ H _ _ _ _ _ _ _ A _ _ _ _ _ H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ A H _ _ _ _ _ _ _ _ h _ _ _ _ _ H _ _ _ _ _ A _ _ A A _ _ h H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ A _ _ _ _ _ _ _ _ h _ _ _ _ _ _ H _ _ _ _ H A A H H H H H h h _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ h _ _ _ _ _ _ _ _ _ h _ _ _ _ _ H H H H H A V a a i a a H h _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ h A _ _ A _ _ _ _ h H H H H H H H a a a a V a a i a a a h _ _ _ }},
    {{ H H H h h H H h h _ _ _ _ h A A A _ _ _ H H H H a i a a a a a i a a V a a i a a A H _ _ _ }},
    {{ V _ H a a V a i h H H H H A H h H h h H H a i a a i a a a a a i a a V a a i a a H A _ _ _ }},
    {{ V _ _ h a i a i a V a a a a a a a a a a a a i a a a h a a a a i a a V a a i a a H A _ _ _ }},
    {{ _ _ _ h a i a i a V a a a h a a i a a h a a i a a h a h a a a i a a a a a i A A h _ A _ _ }},
    {{ _ _ A H h H A a a V a a h a h a i a h a h a i a a a h a b a a i _ a _ H a a H H h _ _ _ _ }},
    {{ _ _ _ A A A H A a V a a a h a a i a a h a a a a a a a b b b a i _ _ _ H h h H _ _ _ _ _ _ }},
    {{ _ _ _ _ _ A _ H h h H a a a a c i a a a i a a a a a b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ H H h h H h h H H H h H H _ H H b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},

}};

const std::array<std::array<Block,45>,20> SHIP_DESERT {{

    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H H H _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ i H _ _ _ _ _ H H H _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ i _ H _ _ _ H _ _ _ H _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ i _ H _ _ _ i _ _ _ i _ _ _ _ _ _ _ _ _ _ _ _ _ H H _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ H _ H _ _ _ _ _ _ H _ _ _ i _ _ _ H _ _ _ _ _ _ _ _ _ _ _ _ H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ _ H _ _ i _ _ H _ _ _ _ _ _ _ _ _ _ _ _ S H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ _ h _ _ _ _ _ H _ _ _ _ _ _ _ _ S S S S h H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ i _ _ _ _ _ _ _ _ h S _ _ _ _ _ H _ _ _ _ H _ S H H H H H h h _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ h _ _ _ _ _ _ _ _ _ h S S _ S S H H H H H a a S a i a a H h _ _ _ }},
    {{ _ _ _ _ _ S S S S _ _ _ _ h _ _ _ _ _ _ _ _ h H H H H H H H a a a a a S a i a a a h _ _ _ }},
    {{ H H H h h H H h h S S _ _ h S S S S _ _ H H H H a i a a a a a i a a a S a i a a S H _ _ _ }},
    {{ i _ H a a i a a h H H H H a S h H h h H H a i a a i a a a a a i a a a a a i a a H _ _ _ _ }},
    {{ i _ _ h a i a a a i a a a a S a a a a a a a i a a a h a a a a i a a a a a i a a H _ _ _ _ }},
    {{ _ _ _ h a i a a a i a a a h S a i a a h a a i a a h a h a a a i a a a a a S S S h _ _ _ _ }},
    {{ _ _ _ H h H S S S i a a h a h S i a h a h a i a a a h a b a a i _ a _ H S S H H h _ _ _ _ }},
    {{ _ _ _ _ _ _ H S S S S S a h S S S S a h a a a a a a a b b b a i _ _ _ H h h H _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ H h h H S S S S c S S S S i a a a a a b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ H H h h H h h H H H h H H _ H H b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},

}};

const std::array<std::array<Block,45>,20> SHIP_FROZEN {{

    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H n n n _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ n H H H n _ _ _ _ _ n n n _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ T H n _ _ _ n H H H n _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ _ H _ _ _ H _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H }},
    {{ _ _ _ _ _ _ _ _ _ _ _ n _ n _ _ _ _ _ _ H _ _ _ T _ _ _ i _ _ _ _ _ _ _ _ _ _ _ _ _ H H _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ H n H _ _ _ _ _ _ H n _ _ _ _ _ n H _ _ _ _ _ _ _ _ _ _ _ _ H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ H T _ _ _ _ _ _ _ H _ _ _ _ _ H _ _ _ _ _ _ _ _ _ _ _ _ I H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ _ i n _ _ _ _ H n _ _ _ _ _ _ I I I I I h H H _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ i _ _ _ _ _ _ _ _ h N n n n _ _ H n n n _ H I I H H H H H h h _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ h n _ _ _ _ _ _ _ n h N N N n n H H H H H a T a a i a a H h _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ h n n _ _ _ _ n N h H H H H H H H a a a a a a a i a a a h _ _ _ }},
    {{ H H H h h H H h h N n n n h N N n n n n H H H H a i T T a a a i a a a a a i a a a H _ _ _ }},
    {{ T _ H a a T a i h H H H H N H h H h h H H a i a a i a a a a a i a a a a a i a a H _ _ _ _ }},
    {{ _ _ _ h a i a i a T a a a a a a a a a a a a i a a a h a a a a i a a a a a i a I H _ _ _ _ }},
    {{ _ _ _ h a i a i a a a a a h a a i a a h a a i a a h a h a a a i a a a a a a I I h _ _ _ _ }},
    {{ _ _ _ H h H I i a a a a h a h a i a h a h a i a a a h a b a a i _ a _ H I I H H h _ _ _ _ }},
    {{ _ _ _ _ _ _ H I I I I a a h a a a a a h a a a a a a a b b b a i _ _ _ H h h H _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ H h h H I I a a c n n n n i n a a a a b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ H H h h H h h H H H h H H _ H H b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},
    {{ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ }},

}};
