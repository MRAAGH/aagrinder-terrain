// Copyright (C) 2019, 2020 MRAAGH

/*
 * This file is licensed under a
 * Creative Commons Attribution 4.0 International License.
 *
 * You should have received a copy of the license along with this
 * work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
 */

#include "structures.h"

#define _ Block::VOID,
#define A Block::Aa,
#define V Block::grass10,

const std::vector<int> GROUND_BUSHES = {0,1,2,3,4,5,6,7};
const std::vector<int> CEILING_BUSHES = {4,5,6,8,9,10,11,12};

const std::vector<std::array<std::array<Block,7>,4>> BUSH_STRUCTURES {

    {{ // 0
        { _ _ _ _ _ _ _ },
        { _ A A A A A _ },
        { A A A _ A A A },
        { _ A A A A A _ },
    }},

    {{ // 1
        { _ _ _ _ _ _ _ },
        { _ _ A A A A _ },
        { A A A _ A A A },
        { _ A A A A A _ },
    }},

    {{ // 2
        { _ _ _ _ _ _ _ },
        { _ A A A A _ _ },
        { A A A _ A A A },
        { _ A A A A A _ },
    }},

    {{ // 3
        { _ _ A A A _ _ },
        { _ A A A A A _ },
        { A A A _ A A A },
        { _ A A A A A _ },
    }},

    {{ // 4
        { _ _ _ A A _ _ },
        { _ A A A A A _ },
        { _ A A _ A A A },
        { _ _ A A A A _ },
    }},

    {{ // 5
        { _ _ A A _ _ _ },
        { _ A A A A A _ },
        { A A A _ A A _ },
        { _ A A A A _ _ },
    }},

    {{ // 6
        { _ _ A A A _ _ },
        { _ A A A A A _ },
        { _ A A _ A A _ },
        { _ _ A A A _ _ },
    }},

    {{ // 7
        { _ _ _ _ _ _ _ },
        { _ _ A A A _ _ },
        { _ A A _ A A _ },
        { A A A A A A A },
    }},

    {{ // 8
        { _ _ _ _ _ _ _ },
        { _ _ A A A _ _ },
        { _ A A _ A A _ },
        { _ _ A A A _ _ },
    }},

    {{ // 9
        { _ _ _ _ _ _ _ },
        { _ _ A A A _ _ },
        { _ _ A _ A A _ },
        { _ _ V A A _ _ },
    }},

    {{ // 10
        { _ _ _ _ _ _ _ },
        { _ _ A A A _ _ },
        { _ A A _ A _ _ },
        { _ _ A A V _ _ },
    }},

    {{ // 11
        { _ _ A A A _ _ },
        { _ A A A A A _ },
        { _ V A _ A A _ },
        { _ _ A A V _ _ },
    }},

    {{ // 12
        { _ _ A A A _ _ },
        { _ A A A A A _ },
        { _ A A _ A V _ },
        { _ _ V A A _ _ },
    }},

};
