// Copyright (C) 2024 MRAAGH

/*
 * This file is licensed under a
 * Creative Commons Attribution 4.0 International License.
 *
 * You should have received a copy of the license along with this
 * work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
 */

#include "structures.h"

#define B Block::B,
#define b Block::b,
#define d Block::d,
#define c Block::WB,
#define _ Block::VOID,
#define a Block::air,
#define F Block::forg,
#define w Block::water,
#define R Block::R,
#define D Block::DD,
#define H Block::Ha,
#define h Block::chest,
#define V Block::grass10,
#define S Block::SSPOOKY,
#define Q Block::quicksand,
#define I Block::WI,
#define s Block::stoneslab,
#define K Block::keepclear,


/* struct FortConnection { */
/*     int offsetx, offsety; // offset describes position of next attachment point relative to top left corner */
/*     int orientation; */
/* }; */

/* struct FortSection { */
/*     std::vector<std::vector<Block>> blocks; */
/*     int orientation; */
/*     int offsetx, offsety; // offset describes position of attachment point relative to top left corner */
/*     int mindepth, maxdepth; */
/*     int limit; */
/*     double mindensity, maxdensity; */
/*     std::vector<FortConnection> connections; */
/* }; */


const std::vector<FortSection> FORT_SECTIONS {

    {
        {
            { _ _ B B B B B B B B B B B B B _ _ },
            { B B B B B B B B B B B B B B B B B },
            { B B B B B a I a a a I a B B B B B },
            { B B B B a a I a a a I a a B B B B },
            { B B B B a a I a a a I a a B B B B },
            { B B B a a a a a a a I a a a B B B },
            { B B a a a a a a a a a a a a a B B },
            { B a a a a a a a a a a a a a a a B },
            { a a a a a a a a a a a a a a a a a },
            { a a a a a a a a F a a a a a a a a },
            { a a a a a a b b b b b a a a a a a },
            { a a a b b b b b b b b b b b a a a },
            { b b b b b b b b b b b b b b b b b },
            { B B B B B B B B B B B B B B B B B },
            { B B B B B B B B B B B B B B B B B },
            { B B B B B B B B B B B B B B B B B },
            { B B B B B B B B B B B B B B B B B },
            { B B B B B B B B B B B B B B B B B },
        },
        0,
        8, 9,
        0, 0,
        1,
        -1.0, 1.0,
        {
            {
                -1, 10,
                2,
            },
            {
                17, 10,
                0,
            },
        },
    },

    {
        {
            { _ _ _ _ B B B B B B B B B _ _ _ _ },
            { _ _ B B B B B B B B B B B B B _ _ },
            { _ B B B B B a a a a a B B B B B _ },
            { _ B B B a a a a a a a a a B B B _ },
            { B B B s s s s s s s s s s s B B B },
            { B B B a I a a I a I a a I a B B B },
            { B B a a I a a I a I a a I a a B B },
            { B B a a I a a I a I a a a a a B B },
            { B B a a I a a c c c a a a a a B B },
            { B B a a a a a c S c a a a a a B B },
            { B B a a a a a c c c a a a a a B B },
            { a a a a a a a I a I a a a a a B B },
            { a a a a a a a a a I a a a a B B B },
            { a a a a a a a a a a a a a a B B B },
            { a a a a a a a a a a a a a B B B _ },
            { B B B B B a a a a a a B B B B B _ },
            { B B B B B B B B B B B B B B B _ _ },
            { _ _ _ B B B B B B B B B B B _ _ _ },
        },
        0,
        0, 13,
        5, 12,
        1,
        0.28, 0.75,
        {},
    },

    {
        {
            { B B B B B B B B B B B },
            { B B B B B B B B B B B },
            { a a a a a a a a a a a },
            { a a a a a a a a a a a },
            { a a a a a a a a a a a },
            { a a a a a a a a a a a },
            { B B B B B B B B B B B },
            { B B B B B B B B B B B },
        },
        0,
        0, 4,
        1, 27,
        9999,
        -0.35, 0.75,
        {
            {
                11, 4,
                0,
            },
        },
    },

    {
        {
            { B B K a _ B B },
            { B B K K a a B },
            { a a K a a a a },
            { a a K B a a a },
            { a K a B b a a },
            { a a B b B a a },
            { B B B B B B B },
            { B B B B B B B },
        },
        0,
        0, 4,
        13, 29,
        2,
        0.0, 0.75,
        {
            {
                7, 4,
                0,
            },
        },
    },

    {
        {
            { B B B B _ K B B B },
            { B B B a a K a B B },
            { a a a K K K a a a },
            { a a K a a K a a a },
            { a a a b B a a a a },
            { a a a B B b B a a },
            { B B B B B B B B B },
            { B B B B B B B B B },
        },
        0,
        0, 4,
        15, 29,
        2,
        0.0, 0.75,
        {
            {
                9, 4,
                0,
            },
        },
    },

    {
        {
            { B B B B _ K a B B B B },
            { B B B a a K a a B B B },
            { a a a K K K a a a a a },
            { a a K a a a a a a a a },
            { a K a b B a a a a a a },
            { a a B b b b a a b a a },
            { B B B B b a a B B B B },
            { B B B B B B a B B B B },
        },
        0,
        0, 4,
        17, 29,
        1,
        0.0, 0.75,
        {
            {
                11, 4,
                0,
            },
        },
    },

    {
        {
            { B B B B B B B _ a _ },
            { B B B B B B _ a a a },
            { a a a a a a a a a a },
            { a a a a a a a a a _ },
            { a a a a a a a a a a },
            { a a a a a a _ a _ _ },
            { B B B B B B B B B _ },
            { B B B B B B B B _ _ },
        },
        0,
        0, 4,
        22, 30,
        5,
        -0.5, 0.85,
        {
            // intentionally an impossible attachment
            // just to prevent this from being recognized as dead end
            {
                7, 4,
                0,
            },
        },
    },

    {
        {
            { B B a _ _ a a _ _ },
            { B a a a _ a a a a },
            { a a a a a a a a _ },
            { a a a a a a _ a _ },
            { a a a a a _ _ _ _ },
            { a a a a B B B B B },
            { B B B B B B B B _ },
            { B B B B B B _ _ _ },
        },
        0,
        0, 4,
        22, 30,
        3,
        -0.5, 0.85,
        {
            // intentionally an impossible attachment
            // just to prevent this from being recognized as dead end
            {
                7, 4,
                0,
            },
        },
    },

    {
        {
            { B B B B },
            { B B B B },
            { a a B B },
            { a a w B },
            { a a B B },
            { a a B B },
            { B B B B },
            { B B B B },
        },
        0,
        0, 4,
        9, 13,
        2,
        -0.2, 0.75,
        {},
    },

    {
        {
            { B B B B B B },
            { B B B B B B },
            { a a a a B B },
            { a a a a B B },
            { a a a a B B },
            { a a a a B B },
            { B B B B B B },
            { B B B B B B },
        },
        0,
        0, 4,
        9, 13,
        1,
        -0.2, 0.75,
        {},
    },

    {
        {
            { B B B B B B B B B B B B },
            { B B B B B B B B B B B B },
            { a a a a a a a a a a B B },
            { a a a a a a a a a a B B },
            { a a a a a a a a b b B B },
            { a a a a a a a b D b B B },
            { B B B B B B B B B B B B },
            { B B B B B B B B B B B B },
        },
        0,
        0, 4,
        8, 15,
        1,
        -0.2, 0.75,
        {},
    },

    {
        {
            { B B B B B B },
            { B B B B B B },
            { a a a a B B },
            { a a a a B B },
            { a a a a B B },
            { a a h a B B },
            { B B B B B B },
            { B B B B B B },
        },
        0,
        0, 4,
        9, 16,
        1,
        -0.2, 0.75,
        {},
    },

    {
        {
            { _ B B B B B B B B B },
            { _ B B B B B B B B B },
            { _ a a a a a a a B B },
            { _ a a a a a a a B B },
            { _ a a a a a a a B B },
            { _ a a a a a a a B B },
            { _ B B a a a a B B B },
            { _ B B B a a a B B B },
            { B _ _ _ _ _ _ _ _ B },
        },
        0,
        1, 4,
        1, 15,
        5,
        -0.35, 0.75,
        {
            {
                5, 8,
                3,
            },
        },
    },

    {
        {
            { _ B B B B B B B B B },
            { _ B B B B B B B B B },
            { _ a a a a a a a a a },
            { _ a a a a a a a a a },
            { _ a a a a a a a a a },
            { _ a a a a a a a a a },
            { _ B B a a a a a B B },
            { _ B B B a a a B B B },
            { B _ _ _ _ _ _ _ _ B },
        },
        0,
        1, 4,
        1, 15,
        4,
        -0.35, 0.75,
        {
            {
                10, 4,
                0,
            },
            {
                5, 8,
                3,
            },
        },
    },

    {
        {
            { B B B B B B B B B },
            { B B B B B B B B B },
            { B B a a a a a B B },
            { a I a a a a a I a },
            { a I a a a a a I a },
            { a I a a a a a I a },
            { a I a a h a a I a },
            { B B B B B B B B B },
            { B B B B B B B B B },
        },
        0,
        0, 5,
        1, 15,
        3,
        -0.35, 0.75,
        {
            {
                9, 5,
                0,
            },
        },
    },

    {
        {
            { _ _ _ B B B B },
            { _ _ B B B B B },
            { _ B B B a a a },
            { B B B a a a a },
            { B B a a a a a },
            { B a a a a a a },
            { a a a a a B B },
            { a a a a B B B },
            { a a a B B B _ },
            { a a B B B _ _ },
            { B B B B _ _ _ },
            { B B B _ _ _ _ },
        },
        0,
        0, 8,
        1, 27,
        9999,
        -0.35, 0.75,
        {
            {
                7, 4,
                0,
            },
        },
    },

    {
        {
            { B B B B B B B B B B B B B B _ _ _ },
            { B B B B B B B B B B B B B B B B B },
            { a a a a a a a a a a a a B B B B B },
            { a a a a a a a a K a a a a a a a a },
            { a a a a a a K K a K K K a a a a a },
            { a a a a a a a a b a a a K K a a a },
            { B B B a a a a a a a a a a a a a a },
            { B B B Q Q Q Q Q Q Q Q Q Q B B B B },
            { _ B B Q Q Q Q Q Q Q Q B B B B B B },
            { _ B B Q Q Q Q Q Q B B B B B _ _ _ },
            { _ B B B Q Q Q Q B B B B _ _ _ _ _ },
            { _ B B B B B B B B B _ _ _ _ _ _ _ },
        },
        0,
        0, 4,
        1, 15,
        2,
        0.0, 0.7,
        {
            {
                17, 5,
                0,
            },
        },
    },

    {
        {
            { B B a a a a a B _ },
            { B B H H a a a B B },
            { B B a a a a a B B },
            { B B a a a a a B B },
            { B B a a a H H B B },
            { B B a a a a a B B },
            { B B a a a a a B B },
        },
        3,
        4, 0,
        1, 14,
        6,
        -0.35, 0.75,
        {
            {
                4, 7,
                3,
            },
        },
    },

    {
        {
            { B B H H a a a B _ },
            { B B B H H H B B B },
            { _ B a a V H a B _ },
            { _ _ a a V a a _ _ },
            { _ _ a _ V a _ _ _ },
        },
        3,
        4, 0,
        22, 30,
        3,
        -0.5, 0.2,
        {
            // intentionally an impossible attachment
            // just to prevent this from being recognized as dead end
            {
                4, 2,
                3,
            },
        },
    },

    {
        {
            { B B a a a w w B _ },
            { B B B w w w B B B },
            { _ B B B a B B B _ },
            { _ _ a a a a a _ _ },
            { _ a a _ a a _ _ _ },
            { _ _ _ _ a _ _ _ _ },
        },
        3,
        4, 0,
        22, 30,
        2,
        0.2, 0.9,
        {
            // intentionally an impossible attachment
            // just to prevent this from being recognized as dead end
            {
                4, 2,
                3,
            },
        },
    },

    {
        {
            { _ B B B B H a a B _ _ },
            { B B B V V V a a B B B },
            { B B a V V V a B B B B },
            { B B a a V V a B B B B },
            { B B a a V a a a B B B },
            { B B a a a a a a B B B },
            { B B a a a a a a a B B },
            { B B s a a a a a a B B },
            { B B B a a a a a a a B },
            { B B B a a a a B B a B },
            { B B a a a a a B B a a },
            { B B s s a a a a a a a },
            { a B B a a a a K K a a },
            { a a a a a a a a a a a },
            { a K K a a a B B B B B },
            { a a a B B B B B B B B },
            { B B B B B B B B B _ _ },
            { B B B B B B B B B _ _ },
        },
        3,
        5, 0,
        1, 18,
        4,
        -0.35, 0.75,
        {
            {
                -1, 14,
                2,
            },
            {
                11, 12,
                0,
            },
        },
    },

    {
        {
            { B B a a a a B B _ },
            { B B a a a a a B B },
            { B B B B a a a B B },
            { B B B a a a a B B },
            { B B B B B a a B B },
            { B B B B B a a B B },
            { B B a a a a a B B },
            { B B a a a a B B B },
            { B B a a a a B B B },
            { B B a a a a B B B },
            { B B a a a B B B B },
            { B B a a a a a a a },
            { B B B a a a a a a },
            { B B B a a a a a a },
            { _ B B a a a a a a },
            { _ B B B B B B B B },
            { _ B B B B B B B B },
        },
        3,
        4, 0,
        1, 23,
        9999,
        -0.35, 0.75,
        {
            {
                9, 13,
                0,
            },
        },
    },

    {
        {
            { _ _ _ _ _ _ _ _ B B B B },
            { _ _ _ _ _ _ _ B B B B B },
            { _ _ _ _ _ _ B B H a a a },
            { _ _ _ _ _ B B B a a a a },
            { _ _ _ _ B B B H a a a a },
            { _ _ _ B B B H H a a a a },
            { _ _ B B B a a H H H B B },
            { _ B B B a a a H a B B B },
            { B B B a a a a H B B B _ },
            { B B a a a a a B B B _ _ },
            { B B a a a a B B B _ _ _ },
            { B B H H a B B B _ _ _ _ },
            { B B a H a B B B _ _ _ _ },
            { B B a H H H H B B _ _ _ },
            { B B a H a a a B B B _ _ },
            { B B a H a a a B B B B _ },
            { B B B H a a a a B B B _ },
            { _ B B B a H a a a B B _ },
            { _ _ B B B H a a a B B _ },
            { _ _ B B B H H a a B B _ },
            { _ B B B a a H H H B B _ },
            { B B B a a a H a B B B _ },
            { B B a a a a H B B B _ _ },
            { B a a a a a B B B _ _ _ },
            { a a a a a B B B _ _ _ _ },
            { a a a a B B B _ _ _ _ _ },
            { a a a B B B _ _ _ _ _ _ },
            { a a B B B _ _ _ _ _ _ _ },
            { B B B B _ _ _ _ _ _ _ _ },
            { B B B _ _ _ _ _ _ _ _ _ },
        },
        0,
        0, 26,
        9, 22,
        2,
        -0.35, 0.75,
        {
            {
                12, 4,
                0,
            },
        },
    },

};

        /*         there should be at least 1 available section with no limit of number of appearances */
