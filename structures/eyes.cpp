// Copyright (C) 2019, 2020 MRAAGH

/*
 * This file is licensed under a
 * Creative Commons Attribution 4.0 International License.
 *
 * You should have received a copy of the license along with this
 * work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
 */

#include "structures.h"

#define c Block::WB,
#define A Block::WA,
#define _ Block::VOID,
#define a Block::air,
#define W Block::WW,
#define R Block::R,
#define I Block::WI,

const std::array<std::array<Block,7>,7> EYE_WARP {{
    {{ _ _ _ I _ _ _ }},
    {{ _ _ c c c _ _ }},
    {{ _ c c a c c _ }},
    {{ c c a a a c c }},
    {{ c a a a a a c }},
    {{ c c a W a c c }},
    {{ _ c c c c c _ }},
}};

const std::vector<std::array<std::array<Block,9>,5>> EYES_RUBY {

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a a a A A _ }},
         {{ A A a a R a a A A }},
         {{ _ A A a a a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a a a A A _ }},
         {{ A A a a a a a A A }},
         {{ _ A A a R a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a a a A A _ }},
         {{ A A a a a a a A A }},
         {{ _ A A R a a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a a a A A _ }},
         {{ A A a R a a a A A }},
         {{ _ A A a a a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A R a a A A _ }},
         {{ A A a a a a a A A }},
         {{ _ A A a a a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a R a A A _ }},
         {{ A A a a a a a A A }},
         {{ _ A A a a a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a a R A A _ }},
         {{ A A a a a a a A A }},
         {{ _ A A a a a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a a a A A _ }},
         {{ A A a a a R a A A }},
         {{ _ A A a a a A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

    {{
         {{ _ _ A A A A A _ _ }},
         {{ _ A A a a a A A _ }},
         {{ A A a a a a a A A }},
         {{ _ A A a a R A A _ }},
         {{ _ _ A A A A A _ _ }},
    }},

};
