#!/bin/bash

rm biomestat
for x in {10..31}; do
    ./aagrinderterrain -i -- "$x" -100 123456 "$x" -101 "$x" -102 "$x" -103 "$x" -104 | grep -E '^.{128}$' >>biomestat
    echo "$x"
done

<biomestat python3 -c '

KNOWN_BIOMES = {
    "a": "apple",
    "A": "apple forest",
    "j": "apple jungle",
    "p": "apricot",
    "P": "apricot forest",
    "J": "jungle",
    "t": "taiga",
    "T": "snowy taiga",
    "d": "desert",
    "O": "oasis",
    "D": "dunes",
    "r": "mountain",
    "z": "green mountain",
    "i": "ice mountain",
    "g": "glacier",
    "m": "mixed forest",
    "C": "tentacle island",
    "y": "yay island",
    "f": "flower island",
    "u": "underground",
    "c": "cave",
    "I": "ice cave",
    "o": "ocean",
    "s": "sky",
    "R": "rainbow",
    "W": "warp",
};

import sys
counters = {}
sum = 0
for line in sys.stdin:
    for ch in line.strip():
        if ch in counters:
            counters[ch] += 1
        else:
            counters[ch] = 1
        sum += 1
counters = {k: v for k, v in sorted(counters.items(), key=lambda item: -item[1])}
for ch in counters:
    name = ch
    if ch in KNOWN_BIOMES:
        name = KNOWN_BIOMES[ch]
    print("{:.1f}% {:<15}".format(counters[ch]/sum*100, name))
'
