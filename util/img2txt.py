#!/usr/bin/python3

# script for converting images to ascii art
# for aagrinder-terrain.

# first argument: path to image with
# vertically arranged subimages, each
# of the same square dimension.

# second argument: character used for white pixels

# third argument: character used for black pixels

import numpy
import sys
import matplotlib.pyplot as pyplot

img = pyplot.imread(sys.argv[1])

block = sys.argv[2]
block2 = sys.argv[3]

if len(img.shape) > 2:
    img = img[...,0]

# width and height of full image
h, w = img.shape

# width determines subimage height
# calculate num of subimages
n = h // w

for i in range(n):
    subim = img[i*w:(i+1)*w,:]
    string = '    {{ // ' + str(i) + '\n' + '\n'.join([
        '        {{ ' +
        ' '.join([block if pix>0.5 else block2 for pix in row])
        +' }},'
        for row in subim]) + '\n    }},\n'
    print(string)
