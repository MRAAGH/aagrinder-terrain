#!/usr/bin/python3

import matplotlib.pyplot as pyplot
import matplotlib.patches as mpatches
import numpy as np


names = [
        'snowtaiga',
        'glacier',
        'apricot',
        'jungle',
        'greenmountain',
        'apple',
        'appleforest',
        'applejungle',
        'icemountain',
        'apricotforest',
        'mountain',
        'dunes',
        'desert',
        'oasis',
        'mixedforest',
        'taiga',
        ]


def pick_biome(temperature, humidity):

    precipitation = humidity

    if temperature < -1.2:
        return 'glacier'
    if temperature < -0.7:
        return 'icemountain'
    if precipitation < -0.7:
        if temperature < -0.5:
            return 'icemountain'
        elif temperature < 0.25:
            if precipitation < -0.85:
                return 'mountain'
            else:
                return 'greenmountain'
        if precipitation < -1:
            return 'dunes'
        elif precipitation < -0.85 and temperature > 0.5:
            return 'oasis'
        else:
            return 'desert'
    else:
        if temperature < -0.5:
            return 'snowtaiga'
        elif temperature < -0.4:
            return 'taiga'
        elif temperature < -0.3:
            return 'mixedforest'
        elif temperature < 0.35:
            if precipitation < 0.0:
                return 'apple'
            elif precipitation < 0.8:
                return 'appleforest'
            else:
                return 'applejungle'
        else:
            if precipitation < 0.2:
                return 'apricot'
            elif precipitation < 1.0:
                return 'apricotforest'
            else:
                return 'jungle'



data = np.zeros((401,401))
for t in range(401):
    for h in range(401):
        temperature = t/100-2
        humidity = h/100-2
        biome = pick_biome(temperature, humidity)
        data[t,h] = names.index(biome)


# get the unique values from data
# i.e. a sorted list of all values in data
values = np.unique(data.ravel())

fig = pyplot.figure(figsize=(8,4))
im = pyplot.imshow(data, origin='lower', cmap='tab20', interpolation='none')

ticks = np.arange(0,401,50)
pyplot.xticks(ticks, ticks/100-2)
pyplot.yticks(ticks, ticks/100-2)

pyplot.title('AAGRINDER biomes as picked by terrain generator')

pyplot.xlabel('humidity')
pyplot.ylabel('temperature')

# get the colors of the values, according to the colormap used by imshow
colors = [ im.cmap(im.norm(value)) for value in values]

# create a patch (proxy artist) for every color
patches = [ mpatches.Patch(color=colors[i], label=names[i] ) for i in range(len(values)) ]
# put those patched as legend-handles into the legend
pyplot.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0)


def on_plot_hover(event):
    if event.xdata == None:
        return
    t = event.ydata
    h = event.xdata
    temperature = t/100-2
    humidity = h/100-2
    biome = pick_biome(temperature, humidity)
    print(biome)

fig.canvas.mpl_connect('motion_notify_event', on_plot_hover)

pyplot.show()











