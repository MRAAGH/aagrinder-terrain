// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include "block.h"
#include "treegenerator.h"
#include "parameters.cpp"

int main(int argc, char **argv){

    TreeType TREE_TYPE;
    int TREE_SEED = 0;
    bool OPTION_PRETTY = false;
    bool OPTION_INFO = false;
    bool OPTION_SERIALIZE = false;

    int choice;
    while (1)
    {
        static struct option long_options[] =
        {
            /* Use flags like so:
               {"verbose", no_argument, &verbose_flag, 'V'}*/
            /* Argument styles: no_argument, required_argument, optional_argument */
            {"version",  no_argument, 0, 'v'},
            {"help",     no_argument, 0, 'h'},
            {"info",     no_argument, 0, 'i'},
            {"pretty",   no_argument, 0, 'p'},
            {"serialize",no_argument, 0, 's'},

            {0,0,0,0}
        };

        int option_index = 0;

        /* Argument parameters:
no_argument: " "
required_argument: ":"
optional_argument: "::" */

        choice = getopt_long( argc, argv, "vhips",
                long_options, &option_index);

        if (choice == -1)
            break;

        switch( choice )
        {
            case 'v':
                printf("aagrinder-terrain version %s\n", AAGRINDER_TERRAIN_VERSION.c_str());
                return EXIT_SUCCESS;
                break;

            case 'h':

                printf("AAGRINDER-terrain  Copyright (C) 2019, 2020, 2024  MRAAGH\n");
                printf("This program comes with ABSOLUTELY NO WARRANTY\n");
                printf("This is free software, and you are welcome to redistribute it\n");
                printf("under certain conditions; for details see:\n");
                printf("https://www.gnu.org/licenses/agpl-3.0-standalone.html\n");
                printf("The source code for this program can be found at:\n");
                printf("https://gitlab.com/MRAAGH/aagrinder-terrain\n\n\n");
                printf("Usage: %s [--version] [--help] [--pretty] [--serialize] [--info] TREETYPE TREESEED\n", argv[0]);

                printf("\n");
                printf("-v, --version    print version and exit\n\n");
                printf("-h, --help       print this help and exit\n\n");
                printf("-p, --pretty     show blocks of chunk\n\n");
                printf("-s, --serialize  output serialized chunk data\n\n");
                printf("-i, --info       output statistical information\n\n");
                printf("TYPE             tree type. One of: apple, pine, apricot,\n");
                printf("almond, avocado, dead, tentacle, avocadosuper, palm,\n");
                printf("oasistree, bush, cranberry, blueberry\n\n");
                printf("TREESEED         tree seed\n\n");
                return EXIT_SUCCESS;
                break;

            case 'i':
                OPTION_INFO = true;
                break;

            case 'p':
                OPTION_PRETTY = true;
                break;

            case 's':
                OPTION_SERIALIZE = true;
                break;

            case '?':
                /* getopt_long will have already printed an error */
                break;

            default:
                /* Not sure how to get here... */
                return EXIT_FAILURE;
        }
    }

    /* Deal with non-option arguments here */
    int which = 0;
    while ( optind < argc )
    {
        if (which == 0) {
            if (strcmp(argv[optind], "apple") == 0)
                TREE_TYPE = TreeType::apple;
            else if (strcmp(argv[optind], "pine") == 0)
                TREE_TYPE = TreeType::pine;
            else if (strcmp(argv[optind], "apricot") == 0)
                TREE_TYPE = TreeType::apricot;
            else if (strcmp(argv[optind], "almond") == 0)
                TREE_TYPE = TreeType::almond;
            else if (strcmp(argv[optind], "avocado") == 0)
                TREE_TYPE = TreeType::avocado;
            else if (strcmp(argv[optind], "dead") == 0)
                TREE_TYPE = TreeType::dead;
            else if (strcmp(argv[optind], "oasistree") == 0)
                TREE_TYPE = TreeType::oasistree;
            else if (strcmp(argv[optind], "tentacle") == 0)
                TREE_TYPE = TreeType::tentacle;
            else if (strcmp(argv[optind], "avocadosuper") == 0)
                TREE_TYPE = TreeType::avocadosuper;
            else if (strcmp(argv[optind], "palm") == 0)
                TREE_TYPE = TreeType::palm;
            else if (strcmp(argv[optind], "bush") == 0)
                TREE_TYPE = TreeType::bush;
            else if (strcmp(argv[optind], "cranberry") == 0)
                TREE_TYPE = TreeType::cranberry;
            else if (strcmp(argv[optind], "blueberry") == 0)
                TREE_TYPE = TreeType::blueberry;
            else {
                fprintf(stderr, "Unknown tree type! Try -h for help\n");
                return EXIT_FAILURE;
            }
        }
        if (which == 1) TREE_SEED = atoi(argv[optind]);
        optind++;
        which++;
    }
    if(which<2){
        fprintf(stderr, "Usage: %s [--version] [--help] [--pretty] [--serialize] [--info] TREETYPE TREESEED\n", argv[0]);
        return EXIT_FAILURE;
    }

    /* double ra = dis(gen); */
    /* gen.seed(124); */
    /* ra = dis(gen); */
    /* std::cout << ra << std::endl; */


    Treegenerator treegenerator;

    uint64_t tree_seed = TREE_SEED;

    std::vector<std::vector<Block>> *blocks =
        treegenerator.generate(TREE_TYPE, tree_seed);

    // Replace VOID with air, for a compatible output format
    // TODO: in the future, aagrinder server can be compatible with null block and it will respect the air block in the tree like a mandatory free space.
    for (size_t i = 0; i < blocks->size(); i++) {
        for (size_t j = 0; j < blocks->at(0).size(); ++j) {
            if ((*blocks)[i][j] == Block::VOID) {
                (*blocks)[i][j] = Block::air;
            }
        }
    }

    // OUTPUT

    if(OPTION_PRETTY) {
      for (int i = blocks->size() - 1; i >= 0; --i) {
        for (size_t j = 0; j < blocks->at(0).size(); ++j) {
          printf("%s", block_codes.at((*blocks)[i][j]).second.c_str());
        }
        printf("\n");
      }
    }

    if(OPTION_SERIALIZE){
      for (size_t i = 0; i < blocks->size(); ++i) {
        for (size_t j = 0; j < blocks->at(0).size(); ++j) {
          printf("%s", block_codes.at((*blocks)[i][j]).first.c_str());
        }
      }
    }

    if(OPTION_INFO){
        printf("generated a tree I guess\n");
    }

    return EXIT_SUCCESS;
}
