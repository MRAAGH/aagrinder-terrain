// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef THREADJOBRUNNER_H
#define THREADJOBRUNNER_H

#include <vector>
#include "threadjob.cpp"

void processChunkJobs(
        std::vector<ChunkJob*> &chunk_jobs,
        std::vector<Stage1Job*> &stage1_jobs);

#endif /* THREADJOBRUNNER_H */
