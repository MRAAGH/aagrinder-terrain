// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CELLULAR2_H
#define CELLULAR2_H

#include <random>
#include <vector>
#include <array>
#include "noise/module/perlin.h"
#include "perlin2.h"

class Cellular2
{
    private:
        std::array<std::array<int,512>,512> *map, *map2;
        int WORLD_SEED;
        std::uint64_t basex, basey, z;
        static const int RANDOM[];
    public:

        Cellular2(
                std::array<std::array<int,512>,512> *map,
                int WORLD_SEED,
                const std::uint64_t basex,
                const std::uint64_t basey,
                const std::uint64_t z
                );

        virtual ~Cellular2();

        std::array<std::array<int,512>,512>* iteration(
                const std::vector<int> &probstrue,
                const std::vector<int> &probsfalse,
                int truthyvalue,
                int falsyvalue
                );

};

#endif /* CELLULAR2_H */

