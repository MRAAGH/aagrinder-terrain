// Copyright (C) 2019, 2020, 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BIOME_H
#define BIOME_H

#include <map>
#include <string>
#include <vector>
#include "block.h"
#include "tree.h"

enum class Biome {
    sky = 0,
    apple,
    appleforest,
    applejungle,
    apricot,
    apricotforest,
    jungle,
    taiga,
    snowtaiga,
    oasis,
    desert,
    dunes,
    rock,
    greenmountain,
    ice,
    glacier,
    mixedforest,
    tentacle,
    yay,
    flower,
    underground,
    cave,
    icecave,
    ocean,
    rainbow,
    warp,
};

struct BiomeProperties {
    std::string serial_code;
    std::string display_character;
    double pond_chance = 0.05;
    double quicksand_chance = -1;
    bool grass = false;
    double grass_chance = 0.5;
    bool flowing_grass = false;
    int vine_length = 3;
    bool ceiling_vines = false;
    int ceiling_vine_chance = 0;
    double bush_chance = 0;
    double ceiling_bush_chance = 0;
    double bush_berry_chance = 0;
    Block bush_wood = Block::Hb;
    Block bush_leaf = Block::Ab;
    Block bush_berry = Block::Fa;
    std::vector<int> tree_bounds; // see tree.h
    std::vector<int> flower_bounds; // see block.h
    int dirt_depth = 3;
    bool sand = false;
    bool sand_dunes = false;
    bool has_waterfall = false;
    bool snow = false;
    bool snowice = false;
    bool ice = false;
    bool icicle = false;
    double boulder_chance = 0;
    double pebble_chance = 0;
    double seagrass_length = 5;
};

const std::map<Biome, BiomeProperties> biome_props = {

    { Biome::apple, BiomeProperties
        {
            .serial_code = "a",
            .display_character = "a",
            .pond_chance = 0.01,
            .grass = true,
            .grass_chance = 2,
            .flowing_grass = false,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {200, 400, 0, 0, 0, 0, 0},
            .dirt_depth = 3,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.01,
            .pebble_chance = 0.07,
        }},

    { Biome::appleforest, BiomeProperties
        {
            .serial_code = "A",
            .display_character = "A",
            .pond_chance = 0.03,
            .grass = true,
            .flowing_grass = true,
            .ceiling_vines = false,
            .bush_chance = 0.1,
            .ceiling_bush_chance = 0.03,
            .bush_berry_chance = 0.2,
            .bush_wood = Block::Hu,
            .bush_leaf = Block::au,
            .bush_berry = Block::Fu,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {900, 0, 0, 1000, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {100, 400, 0, 0, 0, 0, 0},
            .dirt_depth = 4,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.01,
            .pebble_chance = 0.05,
        }},

    { Biome::applejungle, BiomeProperties
        {
            .serial_code = "j",
            .display_character = "j",
            .pond_chance = 0.05,
            .quicksand_chance = 0.5,
            .grass = true,
            .flowing_grass = true,
            .ceiling_vines = true,
            .bush_chance = 0.3,
            .ceiling_bush_chance = 0.1,
            .bush_wood = Block::Hb,
            .bush_leaf = Block::Ab,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {1000, 0, 0, 0, 0, 4000, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {100, 400, 0, 0, 0, 0, 600},
            .dirt_depth = 5,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.00,
            .pebble_chance = 0.04,
        }},

    { Biome::apricot, BiomeProperties
        {
            .serial_code = "p",
            .display_character = "p",
            .pond_chance = 0.04,
            .grass = true,
            .grass_chance = 2.4,
            .flowing_grass = false,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 200, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {200, 300, 400, 0, 0, 0, 0},
            .dirt_depth = 3,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.006,
            .pebble_chance = 0.1,
        }},

    { Biome::apricotforest, BiomeProperties
        {
            .serial_code = "P",
            .display_character = "P",
            .pond_chance = 0.07,
            .grass = true,
            .flowing_grass = true,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 1000, 0, 0, 0, 0, 0, 0, 0},
            .flower_bounds = {100, 300, 400, 0, 0, 0, 0},
            .dirt_depth = 4,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.006,
            .pebble_chance = 0.07,
        }},

    { Biome::jungle, BiomeProperties
        {
            .serial_code = "J",
            .display_character = "J",
            .pond_chance = 0.15,
            .quicksand_chance = 0.5,
            .grass = true,
            .flowing_grass = true,
            .ceiling_vines = true,
            .bush_chance = 0.35,
            .ceiling_bush_chance = 0.1,
            .bush_wood = Block::Hb,
            .bush_leaf = Block::Ab,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {100, 0, 0, 300, 0, 4000, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {100, 400, 0, 0, 0, 0, 600},
            .dirt_depth = 5,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.00,
            .pebble_chance = 0.07,
        }},

    { Biome::taiga, BiomeProperties
        {
            .serial_code = "t",
            .display_character = "t",
            .pond_chance = 0.01,
            .grass = true,
            .grass_chance = 1,
            .flowing_grass = false,
            .ceiling_vines = false,
            .bush_chance = 0.01,
            .bush_berry_chance = 0.5,
            .bush_wood = Block::Hr,
            .bush_leaf = Block::ar,
            .bush_berry = Block::Fr,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {200, 400, 0, 0, 0, 0, 0},
            .dirt_depth = 3,
            .sand = false,
            .snow = false,
            .ice = true,
            .boulder_chance = 0.03,
            .pebble_chance = 0.1,
        }},

    { Biome::snowtaiga, BiomeProperties
        {
            .serial_code = "T",
            .display_character = "T",
            .pond_chance = 0.01,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {200, 400, 0, 0, 0, 0, 0},
            .dirt_depth = 3,
            .sand = false,
            .snow = true,
            .ice = true,
            .boulder_chance = 0.007,
            .pebble_chance = 0.1,
        }},

    { Biome::desert, BiomeProperties
        {
            .serial_code = "d",
            .display_character = "d",
            .pond_chance = -100,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 4,
            .sand = true,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.01,
            .pebble_chance = 0.1,
        }},

    { Biome::oasis, BiomeProperties
        {
            .serial_code = "O",
            .display_character = "O",
            .pond_chance = 0.04,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0.005,
            .bush_wood = Block::Hb,
            .bush_leaf = Block::Ab,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 50, 0, 0, 550, 1000},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 4,
            .sand = true,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.01,
            .pebble_chance = 0.25,
        }},

    { Biome::dunes, BiomeProperties
        {
            .serial_code = "D",
            .display_character = "D",
            .pond_chance = -100,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 4,
            .sand = true,
            .sand_dunes = true,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.00,
            .pebble_chance = 0.15,
        }},

    { Biome::rock, BiomeProperties
        {
            .serial_code = "r",
            .display_character = "r",
            .pond_chance = -0.05,
            .grass = true,
            .grass_chance = 0.1,
            .ceiling_vines = false,
            .bush_chance = 0.01,
            .ceiling_bush_chance = 0.005,
            .bush_wood = Block::Hb,
            .bush_leaf = Block::Ab,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 400, 800, 0},
            .dirt_depth = 0,
            .sand = false,
            .has_waterfall = true,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.02,
            .pebble_chance = 0.1,
        }},

    { Biome::greenmountain, BiomeProperties
        {
            .serial_code = "z",
            .display_character = "z",
            .pond_chance = -0.05,
            .grass = true,
            .grass_chance = 1.5,
            .ceiling_vines = true,
            .bush_chance = 0.2,
            .ceiling_bush_chance = 0.05,
            .bush_wood = Block::Hb,
            .bush_leaf = Block::Ab,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 50, 0, 0, 0, 0},
            .dirt_depth = 1,
            .sand = false,
            .has_waterfall = true,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.02,
            .pebble_chance = 0.06,
        }},

    { Biome::ice, BiomeProperties
        {
            .serial_code = "i",
            .display_character = "i",
            .pond_chance = -100,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0.01,
            .bush_berry_chance = 0.5,
            .bush_wood = Block::Hr,
            .bush_leaf = Block::ar,
            .bush_berry = Block::Fr,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {200, 400, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
            .sand = false,
            .snow = true,
            .ice = true,
            .boulder_chance = 0.007,
            .pebble_chance = 0.07,
        }},

    { Biome::glacier, BiomeProperties
        {
            .serial_code = "g",
            .display_character = "g",
            .pond_chance = -100,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0.003,
            .bush_berry_chance = 0.5,
            .bush_wood = Block::Hr,
            .bush_leaf = Block::ar,
            .bush_berry = Block::Fr,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {200, 400, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
            .sand = false,
            .snow = true,
            .snowice = true,
            .ice = true,
            .icicle = true,
            .boulder_chance = 0.02,
            .pebble_chance = 0.00,
        }},

    { Biome::mixedforest, BiomeProperties
        {
            .serial_code = "m",
            .display_character = "m",
            .pond_chance = 0.03,
            .grass = true,
            .grass_chance = 1.8,
            .flowing_grass = false,
            .ceiling_vines = false,
            .bush_chance = 0.08,
            .bush_berry_chance = 0.2,
            .bush_wood = Block::Hu,
            .bush_leaf = Block::au,
            .bush_berry = Block::Fu,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {500, 1100, 0, 940, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {100, 400, 0, 0, 0, 0, 0},
            .dirt_depth = 3,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.018,
            .pebble_chance = 0.08,
        }},

    { Biome::tentacle, BiomeProperties
        {
            .serial_code = "C",
            .display_character = "C",
            .pond_chance = -100,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 800, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 3,
            .sand = true,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.007,
            .pebble_chance = 0.02,
        }},

    { Biome::yay, BiomeProperties
        {
            .serial_code = "y",
            .display_character = "y",
            .pond_chance = 0.04,
            .grass = true,
            .flowing_grass = true,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 500, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 400, 600, 700, 0},
            .dirt_depth = 3,
            .sand = false,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.000,
            .pebble_chance = 0.00,
        }},

    { Biome::flower, BiomeProperties
        {
            .serial_code = "f",
            .display_character = "f",
            .pond_chance = 0.04,
            .grass = true,
            .grass_chance = 2.5,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {200, 0, 0, 0, 300, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {200, 400, 600, 800, 0, 1000, 0},
            .dirt_depth = 3,
            .sand = false,
            .has_waterfall = true,
            .snow = false,
            .ice = false,
            .boulder_chance = 0.009,
            .pebble_chance = 0.1,
        }},

    { Biome::underground, BiomeProperties
        {
            .serial_code = "u",
            .display_character = "u",
            .pond_chance = 0.2,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
            .has_waterfall = true,
            .pebble_chance = 0.1,
        }},

    { Biome::icecave, BiomeProperties
        {
            .serial_code = "I",
            .display_character = "I",
            .pond_chance = 0.3,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
            .snow = true,
            .ice = true,
            .icicle = true,
            .pebble_chance = 0.1,
        }},

    { Biome::cave, BiomeProperties
        {
            .serial_code = "c",
            .display_character = "c",
            .pond_chance = 0.2,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
            .has_waterfall = true,
            .snow = false,
            .ice = false,
            .pebble_chance = 0.1,
        }},

    { Biome::ocean, BiomeProperties
        {
            .serial_code = "o",
            .display_character = "o",
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
        }},

    { Biome::sky, BiomeProperties
        {
            .serial_code = "s",
            .display_character = "s",
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
        }},

    { Biome::rainbow, BiomeProperties
        {
            .serial_code = "R",
            .display_character = "R",
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 0,
        }},

    { Biome::warp, BiomeProperties
        {
            .serial_code = "W",
            .display_character = "W",
            .pond_chance = -100,
            .grass = false,
            .ceiling_vines = false,
            .bush_chance = 0,
            // app pin mtpin apr alm avo ded ten avosup palm oastr
            .tree_bounds = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            //               *ye *bl "cy ”ma *pu ^wh `go
            .flower_bounds = {0, 0, 0, 0, 0, 0, 0},
            .dirt_depth = 3,
            .sand = true,
            .snow = false,
            .ice = false,
            .pebble_chance = 0.02,
        }},

};

#endif /* BIOME_H */
