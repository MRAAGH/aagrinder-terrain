// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TERRAIN_H
#define TERRAIN_H

#include "threadjob.cpp"
#include "perlin2.h"
#include <vector>

void generate_stage1(Perlin2 *perlin2, Stage1Job *&stage1_job);

void generate_chunk(Perlin2 *perlin2, std::vector<Stage1Job*> &stage1_jobs, ChunkJob *&chunk_job);

#endif /* TERRAIN_H */
