/* This file is part of AAGRINDER-terrain.
 * 
 * You can freely redistribute and/or modify this file. There are no
 * additional terms or conditions regarding this file or its derivatives.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARAMETERS_CPP
#define PARAMETERS_CPP

// terrain generation parameters for AAGRINDER
#include <vector>
#include <string>

const std::string AAGRINDER_TERRAIN_VERSION = "0.1.0";

const double BIOME_TEMPERATURE_FREQUENCY = 0.0002;  // increase to make biomes smaller
const double BIOME_HUMIDITY_FREQUENCY = 0.0005;  // increase to make biomes smaller
const double TERRAIN_DENSITY_FREQUENCY_X = 0.0010;  // increase to make terrain steeper horizontally
const double TERRAIN_DENSITY_FREQUENCY_Y = 0.0020;  // increase to make terrain steeper vertically
const double TERRAIN_MINOR_NOISE_FREQUENCY_X = 0.010;  // increase to make terrain steeper horizontally
const double TERRAIN_MINOR_NOISE_FREQUENCY_Y = 0.031;  // increase to make terrain steeper vertically
const double TERRAIN_SOLID_THRESHOLD = 0.275;  // increase to make mountains smaller
const double TERRAIN_MINOR_REGION_THRESHOLD = -0.37;  // increase to make sky bigger
const double TERRAIN_ISLAND_REGION_THRESHOLD = -1.62;  // increase to make islands bigger
const double TERRAIN_MINOR_ISLAND_REGION_THRESHOLD = -1.32;  // increase to make islands closer together
const double TERRAIN_TENTACLE_ISLAND_HOLE_THRESHOLD = -1.47;  // increase to make warp biome bigger
const double TERRAIN_ISLAND_THRESHOLD = 0.47;
const double TERRAIN_ISLAND_EDGE_THRESHOLD = 1.515;

// this is about the gap between different types of floating island
const double ISLAND_BIOME_GAP_FREQUENCY_X = 0.0004;
const double ISLAND_BIOME_GAP_FREQUENCY_Y = 0.0006;
const double ISLAND_BIOME_GAP_1 = -0.8;
const double ISLAND_BIOME_GAP_2 = 0.8;
const double ISLAND_BIOME_INNER_GAP_SIZE = 0.05;
const double ISLAND_BIOME_OUTER_GAP_SIZE = 0.09;

// spires
const double SPIRE_MAX_DENSITY = 0.3;  // increase to make spires thicker


const int STONE_DOWNWARDS_EXPANSION_CHANCE = 5000;  // [0-10007]

// cellular automata parameters:
const int TERRAIN_CELLULAR_ITERATIONS_1 = 0;
const std::vector<int> TERRAIN_CELLULAR_PROBS_TRUE_1
{0,0,10,20,30,30,50,100,100};
const std::vector<int> TERRAIN_CELLULAR_PROBS_FALSE_1
{0,0,40,50,70,80,90,100,100};

const int TERRAIN_CELLULAR_ITERATIONS_2 = 2;
const std::vector<int> TERRAIN_CELLULAR_PROBS_TRUE_2
{0,0,0,0,100,100,100,100,100};
const std::vector<int> TERRAIN_CELLULAR_PROBS_FALSE_2
{0,0,0,0,0,0,100,100,100};

const int CAVE_CELLULAR_ITERATIONS_1 = 3;
const std::vector<int> CAVE_CELLULAR_PROBS_TRUE_1
{0,0,10,10,10,30,40,100,100};
const std::vector<int> CAVE_CELLULAR_PROBS_FALSE_1
{0,0,40,50,70,90,90,100,100};

const int CAVE_CELLULAR_ITERATIONS_2 = 3;
const std::vector<int> CAVE_CELLULAR_PROBS_TRUE_2
{0,0,0,0,100,100,100,100,100};
const std::vector<int> CAVE_CELLULAR_PROBS_FALSE_2
{0,0,0,0,0,0,100,100,100};

const int DIRT_CELLULAR_ITERATIONS = 2;
const std::vector<int> DIRT_CELLULAR_PROBS_TRUE
{0,0,0,0,100,100,100,100,100};
const std::vector<int> DIRT_CELLULAR_PROBS_FALSE
{0,0,0,0,0,100,100,100,100};


const double CHANNEL_BASE_THRESHOLD = 0.02;
const double CHANNEL_NOISE_FREQUENCY_X = 0.002;  // increase to make caves more complicated horizontally
const double CHANNEL_NOISE_FREQUENCY_Y = 0.002;  // increase to make caves more complicated vertically
const double CHANNEL_EXPANSION_POINT1 = 0.3;  // increase to make cave open up later
const double CHANNEL_EXPANSION_POINT2 = 0.5;  // increase to make cave narrow down later
const double CHANNEL_EXPANSION_POINT3 = 0.9;  // increase to make ocean open up later
const double CHANNEL_EXPANSION_RATE = 20;  // increase to make cave and ocean open up more suddenly
const double CHANNEL_EXPANSION_MAX = 0.30;  // increase to make cave and ocean wider
const double CHANNEL_MINOR_MULTIPLIER = 0.5;  // increase to shrink reserved space for corals
const double CHANNEL_MINOR_NOISE_FREQUENCY_X = 0.016;
const double CHANNEL_MINOR_NOISE_FREQUENCY_Y = 0.02;
const double CHANNEL_END = -0.2;  // increase to prevent cave reaching beyond mountain interior

const double CAVE_DENSITY_THRESHOLD = 0;  // increase to make cave biome start deeper inside the cave
const double WATER_DENSITY_THRESHOLD = 0.7;  // increase to make water begin deeper inside the cave
const double WATERDECORATION_DENSITY_THRESHOLD = 0.805;  // increase to make seagrass start deeper
const double WATER_SAND_DEPTH_MULTIPLIER = 15.0;  // increase to make sand thicker in ocean
const int WATER_SAND_MAX_DEPTH = 5;  // increase to make sand thicker in ocean

const double CORAL_BIOME_FREQUENCY = 0.0005;  // increase to make rare corals more findable
const double CORAL_NOISE_FREQUENCY_X = 0.4;  // increase to make coral more complicated shape
const double CORAL_NOISE_FREQUENCY_Y = 0.3;  // increase to make coral more complicated shape
const std::vector<double> CORAL_RARITY {-0.9,-0.5,-0.5,-0.5,0.9,0.9,0.9,1.3,1.6};  // rarity of each coral type
const double CORAL_BIAS = -0.2;
const double CORAL_BIOME_BIAS = 0.2;

const double SEAGRASS_VARIANCE = 3;  // increase to make seagrass longer

const double STALACTITE_COST = 0.3;  // increase to make stalactites shorter
const int STALACTITE_MAX = 4;  // increase to make stalactites longer

const double STALAGMITE_COST = 0.48;  // increase to make stalagmites shorter
const int STALAGMITE_MAX = 2;  // increase to make stalagmites longer

const double ICICLE_COST = 0.2;  // increase to make icicles shorter
const int ICICLE_MAX = 4;  // increase to make icicles longer

const double GLACIER_PARKOUR_AMPLITUDE = 10;  // increase to make glacier parkour taller
const double GLACIER_ICEBLOCK_CHANCE = 0.05;  // increase to make glacier have more ice

const int WATER_FLATTEN_WIDTH = 30;  // modification not recommended
const int WATER_DRAIN_VOLUME = 300;  // oceans smaller than this volume will be removed

const int POND_MAX_DEPTH = 3;
const int POND_MAX_RADIUS = 20;
const double POND_THRESHOLD_DENSITY_BIAS = 0.2;  // increase to make pond more common near ocean

const double GRASS_LENGTH_BASE = 5;  // increase to make hanging vines longer
const double GRASS_LENGTH_MINOR_VARIANCE = 3;  // increase to make hanging vines more diverse

const double STONE_DENSITY_LIMIT = -0.1;  // increase to allow large dirt areas appearing inside mountain
const double BOULDER_DENSITY_LIMIT = 1.7;  // increase to make giant boulders smaller
const double DIAMOND_DENSITY_LIMIT = 1.74;  // increase to decrease diamonds inside giant boulder
const double BOULDER_DIAMOND_PERCENTAGE = 0.0012;  // increase to increase diamonds inside giant boulder
const double BOULDER_CHANCE_CAVE = 0.01;  // increase for more boulders in cave
const double BOULDER_CHANCE_OCEAN = 0.02;  // increase for more boulders in ocean

const double PEBBLE_CHANCE_CAVE = 0.1;  // increase for more pebbles in cave

// sum of all base chances must be <1
const double BOULDER_CHANCE_BASE = 0.02;  // increase for more embedded boulders
const double BOULDER_CHANCE_MINOR_BONUS = -0.4;  // increase for more embedded boulders on mountain edge
const double BOULDER_CHANCE_ISLAND_BONUS = -10;  // increase for more embedded boulders on islands
const double DIAMOND_CHANCE_BASE = 0.002;  // increase for more diamonds
const double DIAMOND_CHANCE_MINOR_BONUS = -0.0016;  // increase for more diamonds on mountain edge
const double DIAMOND_CHANCE_ISLAND_BONUS = -10;  // increase for more diamonds on islands
const double DIAMOND_CHANCE_FLOWER_ISLAND_BONUS = +10.008;  // increase for more diamonds on flower islands
const double MAGNETITE_CHANCE_BASE = 0.006;  // increase for more magnetite
const double MAGNETITE_CHANCE_MINOR_BONUS = 0;  // increase for more magnetite on mountain edge
const double MAGNETITE_CHANCE_ISLAND_BONUS = -10;  // increase for more magnetite on islands
const double GRAPHITE_CHANCE_BASE = 0.008;  // increase for more chalk
const double GRAPHITE_CHANCE_MINOR_BONUS = 0;  // increase for more chalk on mountain edge
const double GRAPHITE_CHANCE_ISLAND_BONUS = 0.004;  // increase for more chalk on islands

const double DIRT_POCKET_THRESHOLD = 0.3;  // increase for more dirt pockets

const int TREE_ATTEMPTS = 2;  // increase for more trees. 16 is max

const double ROCK_TENTACLE_CHANCE = 0.03;  // increase for more rock tentacles

// DO NOT EDIT NEXT 4 SETTINGS IF YOUR WORLD IS ALREADY PARTIALLY GENERATED!!
const double WARP_DENSITY_MIN = -0.3;  // increase to disallow warp shrine spawning outside of mountain
const double WARP_DENSITY_MAX = 0.3;  // increase to allow warp shrine spawning closer to mountain core

const double FORT_DENSITY_MIN = 0.2;  // increase to disallow four fort spawning near edge of mountain
const double FORT_DENSITY_MAX = 0.5;  // increase to allow four fort spawning closer to mountain core

const double CLOUD_NOISE_FREQUENCY_X = 0.003;  // increase to make cloud more complicated
const double CLOUD_NOISE_FREQUENCY_Y = 0.04;  // increase to make cloud more complicated

const double RAINBOW_CHANCE = 0.1;  // increase for more rainbows in rainbow biome

const int DESERT_PILLAR_ATTEMPTS = 500;  // increase for more desert pillars

const int SHIP_ATTEMPTS = 250;  // increase for more ships

const double TENTACLE_STRUCTURE_DENSITY = 0.6;
const double WARP_BIOME_WARP_CHANCE = 0.3;  // increase for more warps in warp biome (and fewer rubies)

#endif /* PARAMETERS_CPP */
