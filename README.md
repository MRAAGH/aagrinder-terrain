# AAGRINDER-terrain for >>>https://aagrinder.xyz<<<

![AAGRINDER terrain](https://files.mazie.rocks/image_x0_y134_w0_crop.png)

C++ component for terrain generation, intended for https://gitlab.com/MRAAGH/aagrinder.

[libnoise](http://libnoise.sourceforge.net/),
which is licensed under [GNU LGPL](https://www.gnu.org/licenses/lgpl-3.0.en.html),
is partially used and included in this project.

# Compile (on Linux/\*nix)

Open a terminal in this directory and run the `make` command.
You need `g++` installed to do this.

# Run

`./aagrinderterrain -p -- 1 2 3` for generating chunk at x=1, y=2 with world seed 3.

Make sure your terminal window is at least 256 characters wide.

The `--` allows you to use negative coordinates and should be considered mandatory.

List other options: `./aagrinderterrain --help`

Generate a tree: `./aagrindertree -p apple 3`

# Customize (optional)

You can play with terrain generation parameters in `parameters.cpp`,
then recompile by running the `make` command.

# Daemonize (optional)

The daemon caches calculation results, decreasing the number of thread jobs
executed per request. 10%-20% speed optimization on systems with <6 cpu cores.

To compile the daemon, run `make daemon` (this is not included in `make all`)

To start daemon on TCP port 8089, run `./aagrinderterraindaemon -P 8089`

Then call generator with `-P` flag: `./aagrinderterrain -P 8089 -s -- 1 2 3`
