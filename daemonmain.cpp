// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>  // close()
#include <netinet/in.h>
#include <sys/socket.h>
#include "threadjobrunner.h"
#include "parameters.cpp"
#include "outputformat.h"
#include "tcp.h"

const int MAX_QUERY = 1024;

const unsigned int CACHE_LIMIT = 64;  // ~2MB RAM usage per cached section

static volatile int sockfd = 0;
static volatile int keeprunning = 1;

void sig_handler(int signo)
{
    if (signo == SIGINT) {
        keeprunning = 0;
        close(sockfd);
    }
}

int main(int argc, char **argv) {

    signal(SIGINT, sig_handler);

    int PORT = 8089;

    int choice;
    while (1)
    {
        static struct option long_options[] =
        {
            /* Use flags like so:
               {"verbose", no_argument, &verbose_flag, 'V'}*/
            /* Argument styles: no_argument, required_argument, optional_argument */
            {"version", no_argument, 0, 'v'},
            {"help", no_argument, 0, 'h'},
            {"port", required_argument, 0, 'P'},

            {0,0,0,0}
        };

        int option_index = 0;

        /* Argument parameters:
no_argument: " "
required_argument: ":"
optional_argument: "::" */

        choice = getopt_long( argc, argv, "vhP:",
                long_options, &option_index);

        if (choice == -1)
            break;

        switch( choice )
        {
            case 'v':
                printf("aagrinder-terrain version %s\n", AAGRINDER_TERRAIN_VERSION.c_str());
                return EXIT_SUCCESS;
                break;

            case 'h':

                printf("AAGRINDER-terrain  Copyright (C) 2019, 2020, 2024  MRAAGH\n");
                printf("This program comes with ABSOLUTELY NO WARRANTY\n");
                printf("This is free software, and you are welcome to redistribute it\n");
                printf("under certain conditions; for details see:\n");
                printf("https://www.gnu.org/licenses/agpl-3.0-standalone.html\n");
                printf("The source code for this program can be found at:\n");
                printf("https://gitlab.com/MRAAGH/aagrinder-terrain\n");
                printf("libnoise, which can be found at http://libnoise.sourceforge.net/\n");
                printf("and is licensed under GNU Lesser General Public License\n");
                printf("is used and included in this project.\n\n\n");

                printf("Usage: %s [--version] [--help] [--port <port>] \n", argv[0]);
                printf("\n");
                printf("-v, --version      print version and exit\n\n");
                printf("-P, --port <port>  port number to listen on\n\n");
                return EXIT_SUCCESS;
                break;

            case 'P':
                PORT = atoi(optarg);
                break;

            case '?':
                /* getopt_long will have already printed an error */
                break;

            default:
                /* Not sure how to get here... */
                return EXIT_FAILURE;
        }
    }

    std::vector<Stage1Job*> stage1_jobs;

    int connfd;
    unsigned int len;
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1) {
        std::cerr << "failed to create socket" << std::endl;
        exit(0);
    }
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);

    int enable = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable));

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (sockaddr*)&servaddr, sizeof(servaddr))) != 0) {
        std::cerr << "failed to bind socket to port " << PORT << std::endl;
        exit(0);
    }

    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0) {
        std::cerr << "failed to listen on socket" << std::endl;
        exit(0);
    } else {
        std::cout << "AAGRINDER-terrain daemon listening on port " << PORT << std::endl;
    }
    len = sizeof(cli);

    Tcp tcp(MAX_QUERY);

    while (keeprunning) {
        // Accept the data packet from client and verification
        connfd = accept(sockfd, (sockaddr*)&cli, &len);
        if (connfd < 0) {
            std::cerr << "failed to accept client" << std::endl;
            break;
        }

        std::string recv_string(tcp.receive(connfd));
        std::stringstream iss(recv_string);

        // parse action (first word)
        std::string action;
        if (!(iss >> action)) continue;

        if (action == "c") {  // generate chunks
            int WORLD_SEED, X, Y;

            // parse seed (second word)
            if (!(iss >> WORLD_SEED)) continue;

            // every pair of words after this, is chunk coordinates
            std::vector<ChunkJob*> chunk_jobs;
            while (iss >> X >> Y) {
                ChunkJob *chunk_job = new ChunkJob;
                chunk_job->X = X;
                chunk_job->Y = Y;
                std::cout << "creating job " << WORLD_SEED << " " << X << " " << Y << '\n';
                chunk_job->WORLD_SEED = WORLD_SEED;
                chunk_jobs.push_back(chunk_job);
            }
            std::cout << std::flush;

            processChunkJobs(chunk_jobs, stage1_jobs);

            // Send each chunk in a separate packet
            for (auto &cj : chunk_jobs) {
                std::stringstream oss;
                serializeChunk(&oss, cj);
                std::string returnbuff = oss.str();
                // and send that buffer to client
                tcp.send_string(connfd, returnbuff);
            }

            for (auto &cj : chunk_jobs) delete cj;

        }

        // clean up cache down to the limit
        if (stage1_jobs.size() > CACHE_LIMIT) {
            for (auto it = stage1_jobs.begin(); it != stage1_jobs.end()-CACHE_LIMIT; it++) delete *it;
            stage1_jobs.erase(stage1_jobs.begin(), stage1_jobs.end()-CACHE_LIMIT);
        }

        std::cout << "cached sections: " << stage1_jobs.size() << std::endl;
    }

    for (auto &j : stage1_jobs) delete j;

    std::cout << "closing socket and quitting" << std::endl;
    close(sockfd);

    return EXIT_SUCCESS;
}
