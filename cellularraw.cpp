// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cellularraw.h"
#include <iostream>


CellularRaw::CellularRaw(
        std::vector<std::vector<bool>> *map,
        std::uint64_t seed
        ) {
    this->map = map;
    this->map2 = new std::vector<std::vector<bool>>(
            map->size(), std::vector<bool>((*map)[0].size(), false));
    this->seed = seed;

    this->gen = std::mt19937_64();
    this->gen.seed(seed);
    this->dis = std::uniform_real_distribution<double>(0.0, 1.0);
}

CellularRaw::~CellularRaw(){
    delete this->map2;
}

std::vector<std::vector<bool>>* CellularRaw::iteration(
        const std::vector<double> &probstrue,
        const std::vector<double> &probsfalse
        ){
    for (int i = 1; i < (*map).size()-1; ++i) {
        for (int j = 1; j < (*map)[0].size()-1; ++j) {
            int num = 0;

            num += (*map)[i+1][j+0];
            num += (*map)[i+1][j+1];
            num += (*map)[i+0][j+1];
            num += (*map)[i-1][j+1];
            num += (*map)[i-1][j+0];
            num += (*map)[i-1][j-1];
            num += (*map)[i+0][j-1];
            num += (*map)[i+1][j-1];

            double prob = (*map)[i][j]
                ? probstrue[num] : probsfalse[num];

            double generated = 0.5;
            if(prob > 0 && prob < 1) generated = dis(gen);
            /* double generated = abs(perlin2->GetValue(x,y,z)); */

            /* std::cout << generated << " < " << prob << std::endl; */
            /* gen.seed(13); */
            (*map2)[i][j] = generated < prob;
            /* std::cout << (value>0.03?'*':' '); */
            /* std::cout << value << std::endl; */
        }
        /* std::cout << std::endl; */
    }
    std::swap(map, map2);
    return map;
}

std::vector<std::vector<bool>>* CellularRaw::iteration_weighted(
        const std::vector<double> &probstrue,
        const std::vector<double> &probsfalse,
        const std::vector<double> &weights
        ){
    for (int i = 1; i < (*map).size()-1; ++i) {
        for (int j = 1; j < (*map)[0].size()-1; ++j) {
            double num = 0;

            num += (*map)[i+1][j+0] * weights[0];
            num += (*map)[i+1][j+1] * weights[1];
            num += (*map)[i+0][j+1] * weights[2];
            num += (*map)[i-1][j+1] * weights[3];
            num += (*map)[i-1][j+0] * weights[4];
            num += (*map)[i-1][j-1] * weights[5];
            num += (*map)[i+0][j-1] * weights[6];
            num += (*map)[i+1][j-1] * weights[7];

            double prob = (*map)[i][j]
                ? probstrue[num] : probsfalse[num];

            double generated = 0.5;
            if(prob > 0 && prob < 1) generated = dis(gen);
            /* double generated = abs(perlin2->GetValue(x,y,z)); */

            /* std::cout << generated << " < " << prob << std::endl; */
            /* gen.seed(13); */
            (*map2)[i][j] = generated < prob;
            /* std::cout << (value>0.03?'*':' '); */
            /* std::cout << generated << std::endl; */
        }
        /* std::cout << std::endl; */
    }
    std::swap(map, map2);
    return map;
}

