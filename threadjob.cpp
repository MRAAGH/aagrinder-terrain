// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef THREADJOB_CPP
#define THREADJOB_CPP

#include <array>
#include <vector>
#include "block.h"
#include "biome.h"
#include "tiles.cpp"

struct WaterUpdate{
    int x, y; // world coordinates
};

struct Claim {
    int x, y, x2, y2; // world coordinates
};

struct Info {
    std::string name;
    int x = 0;
    int y = 0;
    Info(std::string n, int xx, int yy)
        : name(n), x(xx), y(yy) {}
};

struct ChunkJob {
    int X, Y, WORLD_SEED;
    // all data we generate about a chunk
    std::array<std::array<Block,512>,512> *blocks;
    std::array<std::array<Biome,128>,64> *biome_map;
    std::vector<TileEntity*> tileEntities;
    std::vector<WaterUpdate*> waterUpdates;
    std::vector<Claim*> claims;
    // including some stats:
    std::vector<Info> infos;

    ~ChunkJob() {
        delete blocks;
        delete biome_map;
        for (auto e : tileEntities) delete e;
        for (auto w : waterUpdates) delete w;
        for (auto c : claims) delete c;
    }
};

struct Stage1Job {
    int X, Y, WORLD_SEED;
    // all data needed to efficiently generate a chunk
    std::array<std::array<double,256>,256> *local_density;
    std::array<std::array<double,256>,256> *local_gap;
    std::array<std::array<double,256>,256> *local_channel;
    std::array<std::array<Biome,64>,32> *biome_map;
    std::array<std::array<Biome,64>,32> *original_biome_map;
    std::array<std::array<Block,256>,256> *blocks;

    ~Stage1Job() {
        delete local_density;
        delete local_gap;
        delete local_channel;
        delete biome_map;
        delete original_biome_map;
        delete blocks;
    }
};

#endif /* THREADJOB_CPP */
