CFLAGS = -march=native --std=c++14 -lpthread -Wall
DEBUGCFLAGS = -g -fsanitize=address --std=c++14 -Wall -Wextra
structfiles = $(shell find structures noise -name "*.cpp")
srcfiles = $(structfiles) cellular2.cpp parameters.cpp perlin2.cpp treegenerator.cpp tiles.cpp threadjob.cpp threadjobrunner.cpp terrain.cpp outputformat.cpp tcp.cpp
structobjects = $(patsubst %.cpp, %.o, $(structfiles))
objects = $(patsubst %.cpp, %.o, $(srcfiles))
treerelatedobjects = $(structobjects) parameters.o treegenerator.o


# these are the default targets when executing "make":
all: terrain tree;


# specifying which make targets are not real files:
.PHONY: all terrain tree daemon noop debug debugtree clean


# specifying dependencies of each file, so make knows when to recompile it:
terrain.o: terrain.cpp biome.h block.h noise4d.cpp structures/structures.h
treegenerator.o: treegenerator.cpp block.h tree.h structures/structures.h
threadjob.o: threadjob.cpp block.h tree.h
outputformat.o: outputformat.cpp block.h


# specifying aliases (but ofc. you can also "make aagrinderterraindaemon"):
terrain: aagrinderterrain
tree: aagrindertree
daemon: aagrinderterraindaemon


# The rules to compile the final executables.
aagrinderterrain: main.cpp $(objects)
	g++ main.cpp $(CFLAGS) -O3 -o aagrinderterrain $(objects)
aagrindertree: treemain.cpp $(treerelatedobjects)
	g++ treemain.cpp $(CFLAGS) -O3 -o aagrindertree $(treerelatedobjects)
aagrinderterraindaemon: daemonmain.cpp $(objects)
	g++ daemonmain.cpp $(CFLAGS) -O3 -o aagrinderterraindaemon $(objects)


# clean removes all compiled files
clean:
	rm -f aagrinderterrain aagrindertree aagrinderterraindaemon $(objects)



# Some unimportant rules for debugging. You can ignore this
noop: $(objects)
	g++ main.cpp $(CFLAGS) -o aagrinderterrain $(objects)
debug:
	g++ main.cpp $(DEBUGCFLAGS) -o aagrinderterrain $(srcfiles)
debugtree:
	g++ treemain.cpp $(DEBUGCFLAGS) -o aagrindertree $(srcfiles)
