// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cellular2.h"
#include <iostream>
#include "noise4d.cpp"


Cellular2::Cellular2(
        std::array<std::array<int,512>,512> *map,
        int WORLD_SEED,
        const std::uint64_t basex,
        const std::uint64_t basey,
        const std::uint64_t z
        ) {
    this->map = map;
    this->map2 = new std::array<std::array<int,512>,512>(*map);
    this->WORLD_SEED = WORLD_SEED;
    this->basex = basex;
    this->basey = basey;
    this->z = z;
}

Cellular2::~Cellular2(){
    delete this->map2;
}

std::array<std::array<int,512>,512>* Cellular2::iteration(
        const std::vector<int> &probstrue,
        const std::vector<int> &probsfalse,
        int truthyvalue,
        int falsyvalue
        ){
    for (int i = 4; i < (*map).size()-4; ++i) {
        for (int j = 4; j < (*map)[0].size()-4; ++j) {
            int x = basex+j;
            int y = basey+i;
            int num = 0;

            // skip the loop if there are any strange neighbors
            if ((*map)[i+1][j+0] != truthyvalue
                    && (*map)[i+1][j+0] != falsyvalue) continue;
            if ((*map)[i+1][j+1] != truthyvalue
                    && (*map)[i+1][j+1] != falsyvalue) continue;
            if ((*map)[i+0][j+1] != truthyvalue
                    && (*map)[i+0][j+1] != falsyvalue) continue;
            if ((*map)[i-1][j+1] != truthyvalue
                    && (*map)[i-1][j+1] != falsyvalue) continue;
            if ((*map)[i-1][j+0] != truthyvalue
                    && (*map)[i-1][j+0] != falsyvalue) continue;
            if ((*map)[i-1][j-1] != truthyvalue
                    && (*map)[i-1][j-1] != falsyvalue) continue;
            if ((*map)[i+0][j-1] != truthyvalue
                    && (*map)[i+0][j-1] != falsyvalue) continue;
            if ((*map)[i+1][j-1] != truthyvalue
                    && (*map)[i+1][j-1] != falsyvalue) continue;

            num += (*map)[i+1][j+0] == truthyvalue;
            num += (*map)[i+1][j+1] == truthyvalue;
            num += (*map)[i+0][j+1] == truthyvalue;
            num += (*map)[i-1][j+1] == truthyvalue;
            num += (*map)[i-1][j+0] == truthyvalue;
            num += (*map)[i-1][j-1] == truthyvalue;
            num += (*map)[i+0][j-1] == truthyvalue;
            num += (*map)[i+1][j-1] == truthyvalue;

            int prob;
            if ((*map)[i][j] == truthyvalue) {
                prob = probstrue[num];
            }
            else if ((*map)[i][j] == falsyvalue) {
                prob = probsfalse[num];
            }
            else {
                // ignore innocent bystanders
                continue;
            }

            unsigned int generated = 5000;
            if(prob > 0 && prob < 100) {
                generated = noise4d(x, y, z, WORLD_SEED);
            }

            (*map2)[i][j] = generated < prob*100 ? truthyvalue : falsyvalue;
        }
    }
    std::swap(map, map2);
    z += 2;
    return map;
}

