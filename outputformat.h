// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OUTPUTFORMAT_H
#define OUTPUTFORMAT_H

#include "threadjob.cpp"
#include <ostream>

// prints a single chunk serialized
void serializeChunk(std::ostream *out, ChunkJob* cj);

// prints multiple generated chunks in correct format
void print (
        std::ostream *out,
        std::vector<ChunkJob*> &chunk_jobs,
        bool OPTION_FULL,
        bool OPTION_PRETTY,
        bool OPTION_SERIALIZE,
        bool OPTION_INFO
        );

#endif /* OUTPUTFORMAT_H */
