// Copyright (C) 2019, 2020, 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "terrain.h"
#include <random>
#include <deque>
#include <algorithm>
#include <math.h>
#include <array>
#include "structures/structures.h"
#include "block.h"
#include "biome.h"
#include "tiles.cpp"
#include "cellular2.h"
#include "noise4d.cpp"
#include "parameters.cpp"
#include "treegenerator.h"

#define biome_here (biome_props.at((*biome_map)[i/8][j/4]))

struct Tree{
    int i, j; // position within chunk
    int priority;
    TreeType type;
};

struct BiomeCenter {
    int x, y;
    Biome type;
};

struct Eye {
    int i, j;
};

struct FortNode {
    int x, y;
    int orientation;
    int depth;
};

struct MirrorableFortSection {
    unsigned int index;
    bool mirrored;
};

inline double density_at(int x, int y, Perlin2 *perlin2) {
    double interpolation = (y-2000)/2000.0;
    if (interpolation < 0) interpolation = 0;
    if (interpolation > 1) interpolation = 1;
    double density_main = 0;
    double density_spire = 0;
    if (interpolation < 1) {
        density_main = perlin2->GetValue(
                x*TERRAIN_DENSITY_FREQUENCY_X,
                y*TERRAIN_DENSITY_FREQUENCY_Y,
                0);
    }
    if (interpolation > 0) {
        double spire_falloff = log2(y-1500)*2-16;
        /* double spire_falloff = 30; */
        double spire_island_threshold = 1.2*(TERRAIN_MINOR_ISLAND_REGION_THRESHOLD*spire_falloff+SPIRE_MAX_DENSITY);
        double density_spire_unbounded = -abs(perlin2->GetValue(
                x*TERRAIN_DENSITY_FREQUENCY_X/4,
                y*TERRAIN_DENSITY_FREQUENCY_Y/4,
                0))*spire_falloff+SPIRE_MAX_DENSITY;
        if (density_spire_unbounded > -1) {
            density_spire = density_spire_unbounded;
        } else if (density_spire_unbounded > spire_island_threshold) {
            density_spire = -1;
        } else {
            density_spire = density_spire_unbounded - 1 - spire_island_threshold;
        }
    }
    if (density_spire > density_main) {
        double faster_interpolation = interpolation * 2;
        if (faster_interpolation > 1) faster_interpolation = 1;
        return faster_interpolation*density_spire + (1-interpolation)*density_main;
    }
    return interpolation*density_spire + (1-interpolation)*density_main;
}

inline void generate_vine(std::array<std::array<Block,512>,512> *blocks,
        int x, int y, int gx, int gy, double biome_bonus, Perlin2 *perlin2, int WORLD_SEED) {
    if((*blocks)[y][x] == Block::air
            && (*blocks)[y-1][x] != Block::B
            && !isWater((*blocks)[y-1][x])){
        int numgrass = GRASS_LENGTH_BASE;
        // divided by 2 because perlin has bigger variance
        numgrass += perlin2->GetValue( gx*0.2, gy*0.2,
                200.2)*GRASS_LENGTH_MINOR_VARIANCE/2;
        numgrass += noise4df(gx, gy, 204, WORLD_SEED)
            *GRASS_LENGTH_MINOR_VARIANCE;
        numgrass += biome_bonus;
        if(numgrass < 2) numgrass = 2; // avoid ugly stubs with no vine
        if(numgrass > y) numgrass = y; // can't go beyond generated area
        for (int g = 0; g < numgrass; ++g) {
            if((*blocks)[y-g][x] == Block::B) break; // vine hit stone
            if(isWater((*blocks)[y-g][x])) break; // vine hit water
            (*blocks)[y-g][x] = Block::grass5;
        }
    }
}

inline std::pair<int,int> find_warp_position(
        int xregion, int yregion, Perlin2 *perlin2, int WORLD_SEED) {
    // seed for region?
    int seed = noise4d(xregion, yregion, 2000, WORLD_SEED)
        + 1000*noise4d(xregion, yregion, 2002, WORLD_SEED);
    std::mt19937_64 gen;
    gen.seed(seed);
    // try to find position for warp 100 times
    int global_warp_x, global_warp_y;
    for (int i = 0; i < 100; i++) {
        // 8 * 256 - 2 * 128 = 1792
        int dx = 128 + gen() % 1792;
        int dy = 128 + gen() % 1792;
        // calculate global warp coordinates:
        global_warp_x = xregion*2048 + dx;
        global_warp_y = yregion*2048 + dy;
        // calculate local density at that position
        double density = density_at(global_warp_x, global_warp_y, perlin2);
        if (density > WARP_DENSITY_MIN
                && density < WARP_DENSITY_MAX) {
            // found a nice spot for warp!
            break;
        }
    }
    return std::make_pair(global_warp_x, global_warp_y);
}

inline std::pair<int,int> find_forg_position(
        int xregion, int yregion, Perlin2 *perlin2, int WORLD_SEED) {
    // seed for region?
    int seed = noise4d(xregion, yregion, 4444, WORLD_SEED)
        + 1000*noise4d(xregion, yregion, 4446, WORLD_SEED);
    std::mt19937_64 gen;
    gen.seed(seed);
    // try to find position for forg 500 times
    int global_forg_x, global_forg_y;
    for (int i = 0; i < 500; i++) {
        // forg coordinates must be divisible by 32
        // ((12-3) * 256) / 32 = 72
        int dx = 384 + 32 * (gen() % 72);
        int dy = 384 + 32 * (gen() % 72);
        // calculate global forg coordinates:
        global_forg_x = xregion*3072 + dx;
        global_forg_y = yregion*3072 + dy;
        // check local density in 40x40 surroundings
        bool good_density = true;
        for (int ddx = -20; ddx <= 20; ddx += 20) {
            for (int ddy = -20; ddy <= 20; ddy += 20) {
                double density = density_at(global_forg_x+ddx, global_forg_y+ddy, perlin2);
                if (density < FORT_DENSITY_MIN
                        || density > FORT_DENSITY_MAX) {
                    good_density = false;
                    break;
                }
            }
        }
        if (!good_density) continue;
        // found a nice spot for fort!
        return std::make_pair(global_forg_x, global_forg_y);
    }
    // failed to find a spot for fort.
    // return coordinates that are guaranteed to be outside this chunk.
    // And odd coordinates will let the info parser know this failed.
    return std::make_pair(global_forg_x+8193, global_forg_y+8193);
}

Biome pick_biome(double temperature, double humidity) {

    /* // for testing individual biomes: */
    /* return Biome::apple; */
    /* return Biome::applejungle; */
    /* return Biome::appleforest; */
    /* return Biome::apricot; */
    /* return Biome::apricotforest; */
    /* return Biome::jungle; */
    /* return Biome::taiga; */
    /* return Biome::snowtaiga; */
    /* return Biome::desert; */
    /* return Biome::oasis; */
    /* return Biome::dunes; */
    /* return Biome::rock; */
    /* return Biome::greenmountain; */
    /* return Biome::ice; */
    /* return Biome::glacier; */
    /* return Biome::mixedforest; */
    /* return Biome::tentacle; */
    /* return Biome::yay; */
    /* return Biome::underground; */
    /* return Biome::ocean; */

    // for realism, this should be humidity*temperature
    // but then desert is super rare and ice desert very common
    // and I don't like it that way.
    double precipitation = humidity;

    if (temperature < -1.2) {
        // automatically glacier
        return Biome::glacier;
    }
    if (temperature < -0.7) {
        // automatically ice desert
        return Biome::ice;
    }
    if (precipitation < -0.7) {
        // one of the plant-less biomes
        if (temperature < -0.5) {
            return Biome::ice;
        }
        else if (temperature < 0.25) {
            if (precipitation < -0.85) {
                return Biome::rock;
            }
            else {
                return Biome::greenmountain;
            }
        }
        else if (precipitation < -1) {
            return Biome::dunes;
        }
        else if (precipitation < -0.85 && temperature > 0.5) {
            return Biome::oasis;
        }
        else {
            return Biome::desert;
        }
    }
    else {
        // one of the planty biomes
        if (temperature < -0.5) {
            // super cold planty
            return Biome::snowtaiga;
        }
        else if (temperature < -0.4) {
            // cold planty
            return Biome::taiga;
        }
        else if (temperature < -0.3) {
            // inbetween planty
            return Biome::mixedforest;
        }
        else if (temperature < 0.35) {
            // warm planty
            if (precipitation < 0.0) {
                return Biome::apple;
            }
            else if (precipitation < 0.8) {
                return Biome::appleforest;
            }
            else {
                return Biome::applejungle;
            }
        }
        else {
            // hot planty
            if (precipitation < 0.2) {
                return Biome::apricot;
            }
            else if (precipitation < 1.0) {
                return Biome::apricotforest;
            }
            else {
                return Biome::jungle;
            }
        }
    }

}


void generate_stage1(Perlin2 *perlin2, Stage1Job *&stage1_job) {

    int X = stage1_job->X;
    int Y = stage1_job->Y;
    int WORLD_SEED = stage1_job->WORLD_SEED;

    std::array<std::array<double,256>,256> *local_density =
        new std::array<std::array<double,256>,256>();

    std::array<std::array<double,256>,256> *local_gap =
        new std::array<std::array<double,256>,256>();

    std::array<std::array<double,256>,256> *local_channel =
        new std::array<std::array<double,256>,256>();

    std::array<std::array<Biome,64>,32> *biome_map =
        new std::array<std::array<Biome,64>,32>();

    std::array<std::array<Biome,64>,32> *original_biome_map =
        new std::array<std::array<Biome,64>,32>();

    std::array<std::array<Block,256>,256> *blocks =
        new std::array<std::array<Block,256>,256>();

    // calculate local density for all positions
    for (int i = 0; i < 256; ++i) {
        for (int j = 0; j < 256; ++j) {
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            (*local_density)[i][j] = density_at(x, y, perlin2);
        }
    }

    // calculate gap for all positions
    // and modify local_density correspondingly
    for (int i = 0; i < 256; ++i) {
        for (int j = 0; j < 256; ++j) {
            (*local_gap)[i][j] = -101;
            // to save cpu cycles,
            // skip this where density is outside island region
            if ((*local_density)[i][j]
                    > TERRAIN_MINOR_ISLAND_REGION_THRESHOLD+0.2) continue;
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            double gap_value = perlin2->GetValue(
                    x*ISLAND_BIOME_GAP_FREQUENCY_X,
                    y*ISLAND_BIOME_GAP_FREQUENCY_Y,
                    -2);
            (*local_gap)[i][j] = gap_value;
            // now modify local_density to create gaps
            // find distance to nearest gap
            double dist_to_gap_1 = abs(gap_value-ISLAND_BIOME_GAP_1);
            double dist_to_gap_2 = abs(gap_value-ISLAND_BIOME_GAP_2);
            double dist_to_gap = std::min(dist_to_gap_1, dist_to_gap_2);
            if (dist_to_gap < ISLAND_BIOME_INNER_GAP_SIZE) {
                // we're inside the gap.
                // it's definitely not an island here
                (*local_density)[i][j] = TERRAIN_MINOR_ISLAND_REGION_THRESHOLD;
            }
            else if (dist_to_gap < ISLAND_BIOME_OUTER_GAP_SIZE) {
                // we're somewhere near the gap
                // it should be something between
                // island region threshold and its previous density
                // determine this with a linear equation
                double k = ((TERRAIN_MINOR_ISLAND_REGION_THRESHOLD
                            - (*local_density)[i][j])
                        / (ISLAND_BIOME_INNER_GAP_SIZE
                            -ISLAND_BIOME_OUTER_GAP_SIZE));
                double n = (*local_density)[i][j] - k * ISLAND_BIOME_OUTER_GAP_SIZE;
                (*local_density)[i][j] = k * dist_to_gap + n;
            }
        }
    }

    double base = WORLD_SEED / 10.0;

    // determine bias
    if (base >= 7000*1910 && base < 6000*2230) {
        int xmid = perlin2->GetValue(0.2,0.2,-200.2)*200*0+100;
        double horizontal_decay = (4 + 4
                * (base - 7000*1910) / 10000.0);
        for (int j = 0; j < 256; j++) {
            int x = (X*256-128)+j;
            double column_bias = log(abs(x-xmid)) / horizontal_decay;
            if (abs(x-xmid)<4) {
                column_bias = -10;
            }
            double limit = TERRAIN_MINOR_ISLAND_REGION_THRESHOLD+0.2;
            for (int i = 0; i < 256; i++) {
                (*local_density)[i][j] -= column_bias*2;
                if ((*local_density)[i][j] < limit) {
                    (*local_density)[i][j] = limit;
                }
            }
        }
    }

    // calculate local channel for all positions
    for (int i = 0; i < 256; ++i) {
        for (int j = 0; j < 256; ++j) {
            // to save cpu cycles,
            // skip this where density is very low
            if ((*local_density)[i][j]
                    < TERRAIN_MINOR_REGION_THRESHOLD) continue;
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            (*local_channel)[i][j] = perlin2->GetValue(
                    x*CHANNEL_NOISE_FREQUENCY_X,
                    y*CHANNEL_NOISE_FREQUENCY_Y,
                    8);
        }
    }

    // calculate the biomes
    {
        std::vector<BiomeCenter> biome_centers;
        for (int centerdy = -2; centerdy <= +2; centerdy++) {
            for (int centerdx = -2; centerdx <= +2; centerdx++) {
                int centerx = X + centerdx;
                int centery = Y + centerdy;
                if ((centery + centerx) % 2 == 0) {
                    // spawn biome center
                    BiomeCenter c;
                    c.y = centery * 256 + noise4d(
                            centerx, centery, 3000, WORLD_SEED)%256;
                    c.x = centerx * 256 + noise4d(
                            centerx, centery, 3001, WORLD_SEED)%256;
                    double temperature = perlin2->GetValue(
                            c.x*BIOME_TEMPERATURE_FREQUENCY,
                            c.y*BIOME_TEMPERATURE_FREQUENCY, 3003);
                    double humidity = perlin2->GetValue(
                            c.x*BIOME_HUMIDITY_FREQUENCY,
                            c.y*BIOME_HUMIDITY_FREQUENCY, 3005);
                    c.type = pick_biome(temperature, humidity);
                    // this is how you debug biome labeling:
                    // print the coordinates, then use them to set one to ice
                    // and make sure to run with flag -i
                    /* std::cout << "x " << c.x << ", y " << c.y << " " << (int)(c.type) << std::endl; */
                    /* c.type = Biome::apple; */
                    /* if (c.x == -76910) c.type = Biome::ice; */
                    biome_centers.push_back(c);
                }
            }
        }
        for (int i = 0; i < 32; ++i) {
            for (int j = 0; j < 64; ++j) {

                // density at this discrete biome position
                double density = (*local_density)[i*8][j*4];

                // global coordinates (for deterministicness)
                int x = (X*256-128)+j*4;
                int y = (Y*256-128)+i*8;
                double dy = perlin2->GetValue(
                        0.005*y, 0.005*x, 3007) * 30;
                double dx = perlin2->GetValue(
                        0.005*y, 0.005*x, 3009) * 30;
                // find nearest
                int minind = -1;
                int mindist = 99999999;
                for (size_t k = 0; k < biome_centers.size(); k++) {
                    int dist = pow(biome_centers[k].x-x-dx, 2)
                        + pow(biome_centers[k].y-y-dy, 2);
                    if (dist < mindist) {
                        mindist = dist;
                        minind = k;
                    }
                }
                // ok this is the nearest biome
                Biome chosen_biome = biome_centers[minind].type;

                // for floating islands, we use a different set of biomes
                // and they are determined by local_gap
                if (density <= TERRAIN_MINOR_ISLAND_REGION_THRESHOLD+0.2) {
                    double gap = (*local_gap)[i*8][j*4];
                    if (gap < 100) {
                        chosen_biome = Biome::sky;
                    }
                    if (gap < ISLAND_BIOME_GAP_1) {
                        chosen_biome = Biome::yay;
                    }
                    else if (gap < ISLAND_BIOME_GAP_2) {
                        if (chosen_biome == Biome::apple
                                || chosen_biome == Biome::apricot
                                || chosen_biome == Biome::taiga
                                || chosen_biome == Biome::mixedforest) {
                            // leave biome as it is,
                            // because these biomes are okay to touch
                            // the flower island biome.
                        }
                        else {
                            chosen_biome = Biome::flower;
                        }
                    }
                    else {
                        chosen_biome = Biome::tentacle;
                    }
                }

                // inbetween lesser terrain and islands is sky biome
                else if (density > TERRAIN_MINOR_ISLAND_REGION_THRESHOLD
                        && density < TERRAIN_MINOR_REGION_THRESHOLD-0.2) {
                    // but sometimes it is rainbow instead
                    if (chosen_biome == Biome::apricotforest) {
                        chosen_biome = Biome::rainbow;
                    }
                    else {
                        chosen_biome = Biome::sky;
                    }
                }

                (*original_biome_map)[i][j] = chosen_biome;
                (*biome_map)[i][j] = chosen_biome;

                // within solid terrain is underground biome
                // but we keep the original biome information
                // in original_biome_map
                if (density > TERRAIN_SOLID_THRESHOLD+0.05) {
                    (*biome_map)[i][j] = Biome::underground;
                }

            }
        }

    }

    // generate the basic shape of the world
    for (int i = 0; i < 256; ++i) {
        for (int j = 0; j < 256; ++j) {
            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;

            double density = (*local_density)[i][j];

            // default to air (for sky)
            Block block = Block::air;

            // solid mountain
            if(density > TERRAIN_SOLID_THRESHOLD) block = Block::B;

            // minor between mountain and sky
            else if(density > TERRAIN_MINOR_REGION_THRESHOLD) {

                // choose threshold the same way as for grass
                double lesser_threshold =
                    (density - TERRAIN_MINOR_REGION_THRESHOLD)
                    / (TERRAIN_SOLID_THRESHOLD - TERRAIN_MINOR_REGION_THRESHOLD)
                    * -3 + 1.5;

                double value2 = perlin2->GetValue(
                        double(x)*TERRAIN_MINOR_NOISE_FREQUENCY_X,
                        double(y)*TERRAIN_MINOR_NOISE_FREQUENCY_Y,
                        4);

                block = value2 > lesser_threshold ? Block::B : Block::air;

            }

            // sky between land and island
            else if(density > TERRAIN_MINOR_ISLAND_REGION_THRESHOLD) {
                block = Block::air;
            }

            // tentacle island (special)
            else if ((*biome_map)[i/8][j/4] == Biome::tentacle
                    || (*biome_map)[i/8][j/4] == Biome::warp) {
                if (density < TERRAIN_TENTACLE_ISLAND_HOLE_THRESHOLD) {
                    // inside tentacle island becomes warp biome
                    block = Block::air;
                    (*biome_map)[i/8][j/4] = Biome::warp;
                }
                else if (density < TERRAIN_MINOR_ISLAND_REGION_THRESHOLD) {
                    block = Block::B;
                }
                else {
                    block = Block::air;
                }
            }

            // main, constant part of island (not tentacle)
            else if(density < TERRAIN_ISLAND_REGION_THRESHOLD) {
                double value2 = perlin2->GetValue(
                        double(x)*TERRAIN_MINOR_NOISE_FREQUENCY_X,
                        double(y)*TERRAIN_MINOR_NOISE_FREQUENCY_Y,
                        4);
                block = value2 > TERRAIN_ISLAND_THRESHOLD ? Block::B : Block::air;
            }

            // minor part of island, between main and sky (not tentacle)
            else if (density < TERRAIN_MINOR_ISLAND_REGION_THRESHOLD) {

                // choose threshold using this linear equation
                double k = ((TERRAIN_ISLAND_EDGE_THRESHOLD
                            - TERRAIN_ISLAND_THRESHOLD)
                        / (TERRAIN_MINOR_ISLAND_REGION_THRESHOLD
                            - TERRAIN_ISLAND_REGION_THRESHOLD));
                double n = TERRAIN_ISLAND_EDGE_THRESHOLD
                    - k * TERRAIN_MINOR_ISLAND_REGION_THRESHOLD;
                double lesser_threshold = k * density + n;

                double value2 = perlin2->GetValue(
                        double(x)*TERRAIN_MINOR_NOISE_FREQUENCY_X,
                        double(y)*TERRAIN_MINOR_NOISE_FREQUENCY_Y,
                        4);

                block = value2 > lesser_threshold ? Block::B : Block::air;
            }

            // the sky between island biomes
            else {
                block = Block::air;
            }

            (*blocks)[i][j] = block;

        }
    }

    // channelify
    {

        // dig channels
        for (int i = 0; i < 256; ++i) {
            for (int j = 0; j < 256; ++j) {
                // world coordinates (needed for deterministicness)
                int x = (X*256-128)+j;
                int y = (Y*256-128)+i;

                if (base >= 7000*1910 && base < 6000*2230) {
                    // skip due to column bias
                    continue;
                }

                if ((*blocks)[i][j] == Block::B) {

                    double density = (*local_density)[i][j];

                    if (density < CHANNEL_END) {
                        // no channels allowed in this area.
                        continue;
                    }

                    double channel = (*local_channel)[i][j];

                    // logistic functions for expanding the channel
                    double channel_expansion1 = CHANNEL_EXPANSION_MAX
                        / ( 1 + exp(-CHANNEL_EXPANSION_RATE
                                    * (density - CHANNEL_EXPANSION_POINT1)));

                    double channel_expansion2 = CHANNEL_EXPANSION_MAX
                        / ( 1 + exp(-CHANNEL_EXPANSION_RATE
                                    * -(density - CHANNEL_EXPANSION_POINT2)));

                    double channel_expansion3 = CHANNEL_EXPANSION_MAX
                        / ( 1 + exp(-CHANNEL_EXPANSION_RATE
                                    * (density - CHANNEL_EXPANSION_POINT3)));

                    double channel_expansion = channel_expansion3
                        + std::min(channel_expansion1, channel_expansion2);

                    if (abs(channel) < CHANNEL_BASE_THRESHOLD
                            + channel_expansion * CHANNEL_MINOR_MULTIPLIER) {
                        // within main area of the channel!
                        // fill with water or cave or air
                        if (density > WATER_DENSITY_THRESHOLD) {
                            (*blocks)[i][j] = Block::water;
                        }
                        else if (density > CAVE_DENSITY_THRESHOLD) {
                            (*blocks)[i][j] = Block::cave;
                        }
                        else {
                            (*blocks)[i][j] = Block::air;
                        }
                    }
                    else if (abs(channel) < CHANNEL_BASE_THRESHOLD + channel_expansion) {
                        // within lesser area

                        // how much extra noise on channel wall
                        double lesser_threshold =
                            (abs(channel) - channel_expansion * CHANNEL_MINOR_MULTIPLIER)
                            / ((1 - CHANNEL_MINOR_MULTIPLIER) * channel_expansion)
                            * 3 - 1.5;

                        // decide within this noise
                        double value2 = perlin2->GetValue(
                                double(x)*CHANNEL_MINOR_NOISE_FREQUENCY_X,
                                double(y)*CHANNEL_MINOR_NOISE_FREQUENCY_Y,
                                12);

                        if (value2 > lesser_threshold) {
                            // lesser channel (water or cave)
                            // fill with water or cave
                            if (density > WATER_DENSITY_THRESHOLD) {
                                // lesser water here.
                                // check density again.
                                if (density < WATERDECORATION_DENSITY_THRESHOLD) {
                                    // no choice.
                                    (*blocks)[i][j] = Block::water;
                                }
                                else {
                                    // yes choice
                                    // we could put water or coral
                                    double coral_biome = perlin2->GetValue(
                                            x*CORAL_BIOME_FREQUENCY,
                                            y*CORAL_BIOME_FREQUENCY, 18);
                                    // decide about coral
                                    double value3 = perlin2->GetValue(
                                            x*CORAL_NOISE_FREQUENCY_X,
                                            y*CORAL_NOISE_FREQUENCY_Y,
                                            16)
                                        - CORAL_BIAS
                                        - CORAL_BIOME_BIAS * coral_biome;

                                    if (value3 < lesser_threshold) {
                                        (*blocks)[i][j] = Block::X;
                                    }
                                    else {
                                        (*blocks)[i][j] = Block::water;
                                    }
                                }
                            }
                            else if (density > CAVE_DENSITY_THRESHOLD) {
                                (*blocks)[i][j] = Block::cave;
                            }
                            else {
                                (*blocks)[i][j] = Block::air;
                            }
                        }
                    }

                }

            }
        }
    }

    stage1_job->local_density = local_density;
    stage1_job->local_gap = local_gap;
    stage1_job->local_channel = local_channel;
    stage1_job->biome_map = biome_map;
    stage1_job->original_biome_map = original_biome_map;
    stage1_job->blocks = blocks;
}


void generate_chunk(Perlin2 *perlin2, std::vector<Stage1Job*> &stage1_jobs, ChunkJob *&chunk_job) {

    int X = chunk_job->X;
    int Y = chunk_job->Y;
    int WORLD_SEED = chunk_job->WORLD_SEED;

    std::array<std::array<double,512>,512> *local_density =
        new std::array<std::array<double,512>,512>();

    std::array<std::array<double,512>,512> *local_gap =
        new std::array<std::array<double,512>,512>();

    std::array<std::array<double,512>,512> *local_channel =
        new std::array<std::array<double,512>,512>();

    std::array<std::array<Block,512>,512> *blocks =
        new std::array<std::array<Block,512>,512>();

    std::array<std::array<Biome,128>,64> *biome_map =
        new std::array<std::array<Biome,128>,64>();

    std::array<std::array<Biome,128>,64> *original_biome_map =
        new std::array<std::array<Biome,128>,64>();

    std::array<std::array<double,512>,512> *density_dy =
        new std::array<std::array<double,512>,512>();

    // collect the 4 relevant Stage1Jobs:
    Stage1Job *j_x0_y0 = nullptr;
    Stage1Job *j_x1_y0 = nullptr;
    Stage1Job *j_x0_y1 = nullptr;
    Stage1Job *j_x1_y1 = nullptr;
    for (auto &stage1_job : stage1_jobs) {
        if (stage1_job->X == X+0 && stage1_job->Y == Y+0) j_x0_y0 = stage1_job;
        if (stage1_job->X == X+1 && stage1_job->Y == Y+0) j_x1_y0 = stage1_job;
        if (stage1_job->X == X+0 && stage1_job->Y == Y+1) j_x0_y1 = stage1_job;
        if (stage1_job->X == X+1 && stage1_job->Y == Y+1) j_x1_y1 = stage1_job;
    }

    // merge data received from 4 Stage1Jobs:
    for (int i = 0; i < 256; ++i) {
        for (int j = 0; j < 256; ++j) {

            (*local_density)[i]    [j]     = (*(j_x0_y0->local_density))[i][j];
            (*local_density)[i+256][j]     = (*(j_x0_y1->local_density))[i][j];
            (*local_density)[i]    [j+256] = (*(j_x1_y0->local_density))[i][j];
            (*local_density)[i+256][j+256] = (*(j_x1_y1->local_density))[i][j];

            (*local_gap)[i]    [j]     = (*(j_x0_y0->local_gap))[i][j];
            (*local_gap)[i+256][j]     = (*(j_x0_y1->local_gap))[i][j];
            (*local_gap)[i]    [j+256] = (*(j_x1_y0->local_gap))[i][j];
            (*local_gap)[i+256][j+256] = (*(j_x1_y1->local_gap))[i][j];

            (*local_channel)[i]    [j]     = (*(j_x0_y0->local_channel))[i][j];
            (*local_channel)[i+256][j]     = (*(j_x0_y1->local_channel))[i][j];
            (*local_channel)[i]    [j+256] = (*(j_x1_y0->local_channel))[i][j];
            (*local_channel)[i+256][j+256] = (*(j_x1_y1->local_channel))[i][j];

            (*blocks)[i]    [j]     = (*(j_x0_y0->blocks))[i][j];
            (*blocks)[i+256][j]     = (*(j_x0_y1->blocks))[i][j];
            (*blocks)[i]    [j+256] = (*(j_x1_y0->blocks))[i][j];
            (*blocks)[i+256][j+256] = (*(j_x1_y1->blocks))[i][j];

        }
    }
    for (int i = 0; i < 32; ++i) {
        for (int j = 0; j < 64; ++j) {

            (*biome_map)[i]   [j]    = (*(j_x0_y0->biome_map))[i][j];
            (*biome_map)[i+32][j]    = (*(j_x0_y1->biome_map))[i][j];
            (*biome_map)[i]   [j+64] = (*(j_x1_y0->biome_map))[i][j];
            (*biome_map)[i+32][j+64] = (*(j_x1_y1->biome_map))[i][j];

            (*original_biome_map)[i]   [j]    = (*(j_x0_y0->original_biome_map))[i][j];
            (*original_biome_map)[i+32][j]    = (*(j_x0_y1->original_biome_map))[i][j];
            (*original_biome_map)[i]   [j+64] = (*(j_x1_y0->original_biome_map))[i][j];
            (*original_biome_map)[i+32][j+64] = (*(j_x1_y1->original_biome_map))[i][j];

        }
    }

    std::vector<TileEntity*> tileEntities;
    std::vector<WaterUpdate*> waterUpdates;
    std::vector<Claim*> claims;
    std::vector<Info> infos;


    double base = WORLD_SEED / 10.0;

    // skyblock?
    if (WORLD_SEED == 100000) {
        // skyblock!

        if (X == 0 && Y == 0) {
            // place the structure
            for (size_t i = 0; i < SKYBLOCK_STRUCTURE.size(); i++) {
                for (size_t j = 0; j < SKYBLOCK_STRUCTURE[0].size(); j++) {
                    if (SKYBLOCK_STRUCTURE[i][j] == Block::VOID) continue;
                    (*blocks)[236-i][216+j] = SKYBLOCK_STRUCTURE[i][j];
                }
            }
            tileEntities.push_back(new Chest(89, 94,
                        std::vector<Slot*> {
                            new ItemSlot("X2", 1),
                            new ItemSlot("uw", 1),
                            new Slot(), new Slot(),
                            new Slot(), new Slot(),
                            new Slot(), new Slot(),
                            new Slot(), new Slot(),
                        },10));
        }

        // return data using the pass-by-reference struct
        chunk_job->blocks = blocks;
        chunk_job->biome_map = biome_map;
        chunk_job->tileEntities = tileEntities;
        chunk_job->waterUpdates = waterUpdates;
        chunk_job->claims = claims;
        chunk_job->infos = infos;
        return;
    }

    // determine extra shapes
    int extra_shapes = 0;
    if (abs(base/2-0.9*37) < 0.01) {
        extra_shapes = pow(48,5)-4363*451;
    }

    // calculate density gradient
    for (int i = 1; i < 511; ++i) {
        for (int j = 0; j < 512; ++j) {
            (*density_dy)[i][j] =
                (*local_density)[i-1][j] - (*local_density)[i+1][j];
        }
    }


    for (int k = 0; k < 28; k++) {
        if ((extra_shapes&1<<k) && X%2==0) {
            for (int i = 256; i < 262; ++i) {
                for (int j = 196; j < 204; ++j) {
                    (*blocks)[i+k/4*6][j+k%4*8+0] = Block::B;
                    (*blocks)[i+k/4*6][j+k%4*8+60] = Block::B;
                    (*blocks)[i+k/4*6][j+k%4*8+120] = Block::B;
                }
            }
        }
    }


    // expand stone downwards randomly
    for (int i = 3; i < 512; ++i) {
        for (int j = 0; j < 512; ++j) {

            if ((*blocks)[i][j] != Block::B) continue;
            if ((*blocks)[i-1][j] != Block::air) continue;

            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;

            // chance for expanding by 1 block
            int val = noise4d(x, y, 92, WORLD_SEED);
            if (val > STONE_DOWNWARDS_EXPANSION_CHANCE) continue;
            (*blocks)[i-1][j] = Block::B;

            if ((*blocks)[i-2][j] != Block::air) continue;

            // chance for expanding by another 1 block
            val = noise4d(x, y, 94, WORLD_SEED);
            if (val > STONE_DOWNWARDS_EXPANSION_CHANCE) continue;
            (*blocks)[i-2][j] = Block::B;

            if ((*blocks)[i-3][j] != Block::air) continue;

            // chance for expanding by yet another 1 block
            val = noise4d(x, y, 96, WORLD_SEED);
            if (val > STONE_DOWNWARDS_EXPANSION_CHANCE) continue;
            (*blocks)[i-3][j] = Block::B;

        }
    }

    // cellular automata between stone and air
    {
        std::array<std::array<int,512>,512>* cell_blocks =
            reinterpret_cast<std::array<std::array<int,512>,512>*>(blocks);

        Cellular2 cell_small(cell_blocks,
                WORLD_SEED, X*256-128, Y*256-128, 100);

        for (int it = 0; it < TERRAIN_CELLULAR_ITERATIONS_1; ++it) {
            cell_blocks = cell_small.iteration(
                    TERRAIN_CELLULAR_PROBS_TRUE_1,
                    TERRAIN_CELLULAR_PROBS_FALSE_1,
                    (int)Block::B,
                    (int)Block::air);
        }

        for (int it = 0; it < TERRAIN_CELLULAR_ITERATIONS_2; ++it) {
            cell_blocks = cell_small.iteration(
                    TERRAIN_CELLULAR_PROBS_TRUE_2,
                    TERRAIN_CELLULAR_PROBS_FALSE_2,
                    (int)Block::B,
                    (int)Block::air);
        }

        blocks = reinterpret_cast<std::array<std::array<Block,512>,512>*>(cell_blocks);
    }

    // cleaning up the channels
    {
        // make top flat or blocked

        // list of all water blocks that remained
        // invalid (potential boulder spawns)
        // (I really should name it something other than Tree)
        std::vector<Tree> touchlist;
        for (int i = 492; i > 20; --i) {
            int it = 0;
            for (int j = 20; j < 492; ++j) {
                if ((*blocks)[i][j] == Block::water
                    && isAir((*blocks)[i][j-1])) {
                    // found water with air on left
                    (*blocks)[i][j] = Block::cave;
                    it++;
                    if (it > WATER_FLATTEN_WIDTH
                            || (*blocks)[i+1][j] == Block::water) {
                        // we have gone too far,
                        // or there is water above!
                        // in both these cases, it is not ok
                        // to flatten this.
                        // undo
                        for (int jj = j-it; jj <= j; jj++) {
                            (*blocks)[i][jj] = Block::water;
                        }
                        // instead, the start of this row
                        // becomes a potential boulder position:
                        Tree tree;
                        tree.i = i;
                        tree.j = j-it;
                        int x = (X*256-128)+tree.j;
                        int y = (Y*256-128)+tree.i;
                        tree.priority = noise4d(x, y, 209, WORLD_SEED);
                        touchlist.push_back(tree);
                    }
                }
                else {
                    // combo breaker
                    it = 0;
                }
            }
            it = 0;
            for (int j = 491; j > 19 ; --j) {
                if ((*blocks)[i][j] == Block::water
                    && isAir((*blocks)[i][j+1])) {
                    // found water with air on right
                    (*blocks)[i][j] = Block::cave;
                    it++;
                    if (it > WATER_FLATTEN_WIDTH
                            || (*blocks)[i+1][j] == Block::water) {
                        // we have gone too far,
                        // or there is water above!
                        // in both these cases, it is not ok
                        // to flatten this.
                        // undo
                        for (int jj = j+it; jj >= j; jj--) {
                            (*blocks)[i][jj] = Block::water;
                        }
                        // instead, the start of this row
                        // becomes a potential boulder position:
                        Tree tree;
                        tree.i = i;
                        tree.j = j+it;
                        int x = (X*256-128)+tree.j;
                        int y = (Y*256-128)+tree.i;
                        tree.priority = noise4d(x, y, 209, WORLD_SEED);
                        touchlist.push_back(tree);
                    }
                }
                else {
                    // combo breaker
                    it = 0;
                }
            }
            for (int j = 20; j < 492; ++j) {
                if ((*blocks)[i][j] == Block::water
                    && isAir((*blocks)[i-1][j])) {
                    // found water with air below
                    // this is automatically invalid
                    Tree tree;
                    tree.i = i;
                    tree.j = j;
                    int x = (X*256-128)+tree.j;
                    int y = (Y*256-128)+tree.i;
                    tree.priority = noise4d(x, y, 209, WORLD_SEED);
                    touchlist.push_back(tree);
                }
            }
        }


        // remove small bodies of water
        std::array<std::array<int,512>,512> visited{};
        for (int i = 20; i < 492; ++i) {
            for (int j = 20; j < 492; ++j) {
                if ((*blocks)[i][j] == Block::water
                        && isAir((*blocks)[i-1][j])
                   ) {
                    // found water with air below
                    // check if it is a small body of water
                    bool yes_drain = false;
                    std::deque<std::pair<int,int>> stack;
                    std::vector<std::pair<int,int>> found;
                    // need unique color
                    int currentcolor = i * 512 + j + 1;
                    visited[i][j] = currentcolor;
                    stack.push_back(std::make_pair(j, i));
                    found.push_back(std::make_pair(j, i));
                    for (int k = 0; k < WATER_DRAIN_VOLUME; k++) {
                        if (stack.empty()) {
                            // ran out of stack.
                            // this means there's not a lot of water
                            // drain it immediately
                            yes_drain = true;
                            break;
                        }

                        // take element from stack
                        std::pair<int, int> pair_from_stack = stack.front();
                        stack.pop_front();
                        int x = pair_from_stack.first;
                        int y = pair_from_stack.second;

                        // check for adjacent incorrect color
                        if (((*blocks)[y-1][x] == Block::water && visited[y-1][x] != currentcolor)
                                || (visited[y+1][x] && visited[y+1][x] != currentcolor)
                                || (visited[y][x-1] && visited[y][x-1] != currentcolor)
                                || (visited[y][x+1] && visited[y][x+1] != currentcolor)
                           ) {
                            // this means we touched a previously explored area
                            // reject
                            break;
                        }

                        // continue bfs
                        if ((*blocks)[y][x-1] == Block::water
                                && !visited[y][x-1]) {
                            visited[y][x-1] = currentcolor;
                            stack.push_back(std::make_pair(x-1, y));
                            found.push_back(std::make_pair(x-1, y));
                        }
                        if ((*blocks)[y][x+1] == Block::water
                                && !visited[y][x+1]) {
                            visited[y][x+1] = currentcolor;
                            stack.push_back(std::make_pair(x+1, y));
                            found.push_back(std::make_pair(x+1, y));
                        }
                        if ((*blocks)[y+1][x] == Block::water
                                && !visited[y+1][x]) {
                            visited[y+1][x] = currentcolor;
                            stack.push_back(std::make_pair(x, y+1));
                            found.push_back(std::make_pair(x, y+1));
                        }

                    }

                    if (yes_drain) {
                        // DRAIN EVERYTHING
                        for (auto& pair : found) {
                            int x = pair.first;
                            int y = pair.second;
                            (*blocks)[y][x] = Block::cave;
                        }
                    }

                }
            }
        }

        // order boulder positions by priority score
        std::sort(touchlist.begin(), touchlist.end(),
                [](const Tree &a, const Tree &b) -> bool
                {
                return a.priority > b.priority;
                });

        // place boulders in some of those positions
        for(auto touch : touchlist){
            int x = (X*256-128)+touch.j;
            int y = (Y*256-128)+touch.i;

            if ((*blocks)[touch.i][touch.j] != Block::water) {
                // actually this spot is not relevant any more
                continue;
            }

                // choose boulder template:
                int seed = noise4d(x, y, 303, WORLD_SEED);
                int chosen_ind = seed % BLOCKADE_BOULDERS.size();
                int chosen = BLOCKADE_BOULDERS[chosen_ind];

                auto bouldermap =
                    BOULDER_STRUCTURES[chosen];

                // place the structure
                for (size_t ddy = 0; ddy < bouldermap.size(); ddy++) {
                    for (size_t ddx = 0; ddx < bouldermap[0].size(); ddx++) {
                        Block newblock = bouldermap[ddy][ddx];
                        if (newblock == Block::VOID) continue;
                        int worldx = touch.j - bouldermap[0].size()/2 + ddx;
                        int worldy = touch.i + bouldermap.size()/2 - ddy;
                        (*blocks)[worldy][worldx] = newblock;
                    }
                }

            }



    }

    // create ocean biome where water
    for (int i = 0; i < 512; ++i) {
        for (int j = 0; j < 512; ++j) {
            Block there = (*blocks)[i][j];
            if (isWater(there) || there == Block::X) {
                (*biome_map)[i/8][j/4] = Biome::ocean;
            }
        }
    }


    // cellular automata between stone and cave
    {
        std::array<std::array<int,512>,512>* cell_blocks =
            reinterpret_cast<std::array<std::array<int,512>,512>*>(blocks);

        Cellular2 cell_small(cell_blocks,
                WORLD_SEED, X*256-128, Y*256-128, 100);

        for (int it = 0; it < CAVE_CELLULAR_ITERATIONS_1; ++it) {
            cell_blocks = cell_small.iteration(
                    CAVE_CELLULAR_PROBS_TRUE_1,
                    CAVE_CELLULAR_PROBS_FALSE_1,
                    (int)Block::B,
                    (int)Block::cave);
        }

        for (int it = 0; it < CAVE_CELLULAR_ITERATIONS_2; ++it) {
            cell_blocks = cell_small.iteration(
                    CAVE_CELLULAR_PROBS_TRUE_2,
                    CAVE_CELLULAR_PROBS_FALSE_2,
                    (int)Block::B,
                    (int)Block::cave);
        }

        blocks = reinterpret_cast<std::array<std::array<Block,512>,512>*>(cell_blocks);
    }

    // create cave biome where cave block
    for (int i = 0; i < 512; ++i) {
        for (int j = 0; j < 512; ++j) {
            Block there = (*blocks)[i][j];
            if (there == Block::cave
                    && (*biome_map)[i/8][j/4] == Biome::underground) {
                if (biome_props.at((*original_biome_map)
                            [i/8][j/4]).snow) {
                    (*biome_map)[i/8][j/4] = Biome::icecave;
                }
                else {
                    (*biome_map)[i/8][j/4] = Biome::cave;
                }

            }
        }
    }

    // pondify ponify
    std::array<std::array<int,512>,512> depths{};
    for (int i = 40; i < 502; ++i) {
        for (int j = 40; j < 502; ++j) {
            Block here = (*blocks)[i][j];
            Block below = (*blocks)[i-1][j];
            // check if ok position to spawn pond
            if ((!isAir(here) && !isGrass(here))
                    || (below != Block::B
                        && below != Block::pond
                        && below != Block::quicksand
                        )) {
                // not okay
                continue;
            }
            // need global coordinates
            // for deterministicness
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            // choose block for pond
            Block pond_block = Block::pond;
            Block other_pond_block = Block::quicksand;
            if (below == Block::quicksand) {
                std::swap(pond_block, other_pond_block);
            }
            else if (below == Block::pond) {
            }
            else {
                // roll for quicksand
                double generated = noise4df(x, y, 297, WORLD_SEED);
                if (generated < biome_here.quicksand_chance) {
                    std::swap(pond_block, other_pond_block);
                }
            }
            // is ok place to spawn a pond.
            // but unless it's already pond underneath,
            // there is going to be a chance that we still cancel this.
            if (below != pond_block) {
                double pondthreshold = biome_here.pond_chance;
                pondthreshold += (*local_density)[i][j]
                    * POND_THRESHOLD_DENSITY_BIAS;
                // roll for pond
                double generated = noise4df(x, y, 300, WORLD_SEED);
                if (generated > pondthreshold) {
                    continue;
                }
            }
            // simulate what happens if we spawn water here
            int right;
            bool yes_pond = false;
            int estimated_pond_depth = 1;
            for (right = 0; right < POND_MAX_RADIUS; right++) {
                Block there = (*blocks)[i][j+right];
                Block below = (*blocks)[i-1][j+right];
                // check if obstructed
                if (!isAir(there)) {
                    // found boundary
                    yes_pond = true;
                    break;
                }
                else if (below == pond_block) {
                    // depth increases
                    int depth_there = depths[i-1][j+right];
                    if (depth_there + 1 > estimated_pond_depth) {
                        estimated_pond_depth = depth_there + 1;
                    }
                }
                else if (below == other_pond_block) {
                    // not allowing mixed types
                    // reject
                    yes_pond = false;
                    break;
                }
                else if (isAir(below)) {
                    // not allowed to flow over edge
                    // reject
                    yes_pond = false;
                    break;
                }
            }
            if (!yes_pond) continue;
            int left;
            yes_pond = false;
            for (left = 0; left < POND_MAX_RADIUS; left++) {
                Block there = (*blocks)[i][j-left];
                Block below = (*blocks)[i-1][j-left];
                // check if obstructed
                if (!isAir(there)) {
                    // found boundary
                    yes_pond = true;
                    break;
                }
                else if (below == pond_block) {
                    // depth increases
                    int depth_there = depths[i-1][j-left];
                    if (depth_there + 1 > estimated_pond_depth) {
                        estimated_pond_depth = depth_there + 1;
                    }
                }
                else if (below == other_pond_block) {
                    // not allowing mixed types
                    // reject
                    yes_pond = false;
                    break;
                }
                else if (isAir(below)) {
                    // not allowed to flow over edge
                    // reject
                    yes_pond = false;
                    break;
                }
            }
            if (!yes_pond) continue;
            if (estimated_pond_depth > POND_MAX_DEPTH) continue;

            // if we got this far, pond was approved
            for (int k = -left+1; k < right; k++) {
                depths[i][j+k] = estimated_pond_depth;
                if((*blocks)[i][j+k] == Block::cave) {
                    (*blocks)[i][j+k] = Block::cavepond;
                }
                else {
                    (*blocks)[i][j+k] = pond_block;
                }
            }
        }
    }

    // sand at bottom of ocean
    for (int i = 40; i < 502; ++i) {
        for (int j = 40; j < 502; ++j) {
            if ((*biome_map)[i/8][j/4] != Biome::ocean) continue;
            Block here = (*blocks)[i][j];
            if (here != Block::water
                    && here != Block::X) continue;
            if ((*blocks)[i-1][j] != Block::B) continue;
            int sand_depth = ((*local_density)[i][j] - WATERDECORATION_DENSITY_THRESHOLD) * WATER_SAND_DEPTH_MULTIPLIER;
            if (sand_depth > WATER_SAND_MAX_DEPTH) {
                sand_depth = WATER_SAND_MAX_DEPTH;
            }
            for (int d = 1; d <= sand_depth; d++) {
                if ((*blocks)[i-d-1][j] != Block::B) break;
                (*blocks)[i-d][j] = Block::sand;
                (*biome_map)[(i-d)/8][j/4] = Biome::ocean;
            }
        }
    }

    // remove floating corals
    // and color corals
    {
        std::array<std::array<bool,512>,512> visited{};
        for (int i = 20; i < 492; ++i) {
            for (int j = 20; j < 492; ++j) {
                // world coordinates (needed for deterministicness)
                int x = (X*256-128)+j;
                int y = (Y*256-128)+i;
                if ((*blocks)[i][j] == Block::X
                        && !visited[i][j]) {
                    // found coral
                    // and it's not been visited yet
                    // choose color
                    std::vector<Block> coral_pallete;
                    for (int k = 0; k < 9; ++k) {
                        double coral_biome = perlin2->GetValue(
                                x * CORAL_BIOME_FREQUENCY,
                                y * CORAL_BIOME_FREQUENCY, 400 + k);
                        if (coral_biome > CORAL_RARITY[k]) {
                            coral_pallete.push_back((Block)((int)Block::X0+k));
                        }
                    }
                    // fix for empty
                    if (coral_pallete.empty()) {
                        coral_pallete.push_back(Block::X0);
                    }
                    int coral_color_choice = noise4d(x, y, 420, WORLD_SEED)
                        % coral_pallete.size();
                    Block coral_block = coral_pallete[coral_color_choice];

                    // check if floating
                    bool yes_floating = true;
                    std::vector<std::pair<int,int>> stack, found;
                    visited[i][j] = true;
                    stack.push_back(std::make_pair(j, i));
                    found.push_back(std::make_pair(j, i));
                    while (!stack.empty()) {
                        // take element from stack
                        std::pair<int, int> pair_from_stack = stack.back();
                        stack.pop_back();
                        int x = pair_from_stack.first;
                        int y = pair_from_stack.second;

                        if (x == 0 || x == 511 || y == 0 || y == 511) continue;

                        // color
                        (*blocks)[y][x] = coral_block;

                        // check if attached
                        Block attachment = (*blocks)[y-1][x];
                        if (attachment == Block::B
                                || attachment == Block::sand) {
                            // ok this part is attached to stone
                            yes_floating = false;
                            // no need to break loop. Keep labeling,
                            // because we need to visit everything anyway.
                        }

                        if ((*blocks)[y][x-1] == Block::X
                                && !visited[y][x-1]) {
                            visited[y][x-1] = true;
                            stack.push_back(std::make_pair(x-1, y));
                            found.push_back(std::make_pair(x-1, y));
                        }
                        if ((*blocks)[y][x+1] == Block::X
                                && !visited[y][x+1]) {
                            visited[y][x+1] = true;
                            stack.push_back(std::make_pair(x+1, y));
                            found.push_back(std::make_pair(x+1, y));
                        }
                        if ((*blocks)[y-1][x] == Block::X
                                && !visited[y-1][x]) {
                            visited[y-1][x] = true;
                            stack.push_back(std::make_pair(x, y-1));
                            found.push_back(std::make_pair(x, y-1));
                        }
                        if ((*blocks)[y+1][x] == Block::X
                                && !visited[y+1][x]) {
                            visited[y+1][x] = true;
                            stack.push_back(std::make_pair(x, y+1));
                            found.push_back(std::make_pair(x, y+1));
                        }

                    }

                    if (yes_floating) {
                        // DELETE EVERYTHING
                        for (auto& pair : found) {
                            int x = pair.first;
                            int y = pair.second;
                            (*blocks)[y][x] = Block::water;
                        }
                    }

                }
            }
        }
    }

    // grassify
    for (int i = 510; i > 3; --i) {
        for (int j = 1; j < 511; ++j) {
            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;

            if ((*blocks)[i][j] == Block::pond
                    && (*blocks)[i-1][j] == Block::B){
                // super special case, only for ponds
                (*blocks)[i][j] = Block::fakegrasspond;
                continue;
            }
            if ((*blocks)[i][j] == Block::quicksand
                    && (*blocks)[i-1][j] == Block::B){
                // super special case, only for quicksand ponds
                (*blocks)[i][j] = Block::fakegrassquicksand;
                continue;
            }
            // is a place for grass?
            if(((*blocks)[i][j] == Block::air
                        || isGrass((*blocks)[i][j]))
                    && (*blocks)[i-1][j] == Block::B){
                if(biome_here.flowing_grass){
                    // biome full of grass!
                    (*blocks)[i][j] = Block::grass5;

                    // can make some grass on the left?
                    generate_vine(blocks, j-1, i, x, y,
                            biome_here.vine_length, perlin2, WORLD_SEED);

                    // can make some grass on the right?
                    generate_vine(blocks, j+1, i, x, y,
                            biome_here.vine_length, perlin2, WORLD_SEED);
                }
                else if (biome_here.grass) {
                    // biome with partial grass.
                    // instead of continuous grass,
                    // we have strands here.
                    double grass_strand_chance = biome_here.grass_chance;
                    double value = noise4df(x, y, 204, WORLD_SEED);
                    if (value*11 < grass_strand_chance) {
                        // SUPER TALL GRASS
                        (*blocks)[i][j] = Block::grass10;
                        if (isAir((*blocks)[i+1][j])) {
                            (*blocks)[i+1][j] = Block::grass8;
                        }
                    }
                    else if (value*7 < grass_strand_chance) {
                        // tall grass
                        (*blocks)[i][j] = Block::grass10;
                    }
                    else if (value*3 < grass_strand_chance) {
                        // boring normal grass
                        (*blocks)[i][j] = Block::grass8;
                    }
                    else {
                        // no grass at all,
                        // but still need to mark this position
                        // for dirtifying
                        (*blocks)[i][j] = Block::fakegrass;
                    }
                }
                else {
                    // no flowing grass and also no grass strands
                    // but we still need to mark this position
                    // for dirtifying
                    (*blocks)[i][j] = Block::fakegrass;
                }
            }
            else if((*blocks)[i][j] == Block::air
                    && (*blocks)[i+1][j] == Block::B
                    && isAir((*blocks)[i-1][j])
                    && isAir((*blocks)[i-2][j])
                    && isAir((*blocks)[i-3][j])
                    && biome_here.ceiling_vines
                    ) {
                // this is a place for a ceiling-hanging vine
                // chance for the vine:
                double vine_roll = noise4d(x, y, 200, WORLD_SEED);
                if (vine_roll < biome_here.ceiling_vine_chance) {
                    generate_vine(blocks, j, i, x, y,
                            biome_here.vine_length, perlin2, WORLD_SEED);
                }
            }

        }
    }


    //connect grass
    for (int i = 58; i < 430; ++i) {
        // it's important that this goes so far beyond chunk,
        // because it also modifies the ground and can
        // affect trees.
        for (int j = 58; j < 454; ++j) {
            // is grass?
            if((*blocks)[i][j] == Block::grass5){
                // what's underneath?
                switch((*blocks)[i-1][j]){
                    case Block::air:
                        // underneath is air. Choice is obviously ╵
                        (*blocks)[i][j] = Block::grass2;
                        break;
                    case Block::B:
                        // underneath is stone. Many options here

                        // special case: is there stone on left & right?
                        if((*blocks)[i][j-1] == Block::B && (*blocks)[i][j+1] == Block::B){
                            // ugh.
                            if(isGrass((*blocks)[i+1][j])){
                                // okay at least there's something above to connect to
                                // use ╵
                                (*blocks)[i][j] = Block::grass2;
                                break;
                            }
                            // you know what, maybe this wasn't such a good idea
                            // hide it, hide it so nobody can see
                            (*blocks)[i][j] = Block::B;
                            break;
                        }

                        // okay more "normal" cases with running grass:
                        if((*blocks)[i][j-1] == Block::B
                                && isGrass((*blocks)[i+1][j-1])
                                && isGrass((*blocks)[i+1][j])){
                            // nice curve with hill on left ╰
                            (*blocks)[i][j] = Block::grass3;
                            break;
                        }
                        if((*blocks)[i][j+1] == Block::B
                                && isGrass((*blocks)[i+1][j+1])
                                && isGrass((*blocks)[i+1][j])){
                            // nice curve with hill on right ╯
                            (*blocks)[i][j] = Block::grass6;
                            break;
                        }

                        // huh, there's nothing special around.
                        // Might as well leave it regular grass.
                        // As it already is (block id 15).
                        break;

                    default:
                        // this contains all cases when there's grass underneath

                        // is there also grass above?
                        if(isGrass((*blocks)[i+1][j])){
                            // none shall interrupt the long running vine │
                            (*blocks)[i][j] = Block::grass10;
                            break;
                        }

                        // connect to left and right?
                        if(isGrass((*blocks)[i][j+1])
                                && isGrass((*blocks)[i][j-1])
                                && (*blocks)[i-1][j+1] == Block::B
                                && (*blocks)[i-1][j-1] == Block::B){
                            // ┬
                            (*blocks)[i][j] = Block::grass13;
                            break;
                        }
                        // connect to left?
                        if(isGrass((*blocks)[i][j-1])
                                && (*blocks)[i-1][j-1] == Block::B){
                            // ╮
                            (*blocks)[i][j] = Block::grass12;
                            break;
                        }
                        // connect to right?
                        if(isGrass((*blocks)[i][j+1])
                                && (*blocks)[i-1][j+1] == Block::B){
                            // ╭
                            (*blocks)[i][j] = Block::grass9;
                            break;
                        }

                        // doesn't connect to left and right,
                        // and also not above.
                        // this means it's the start of a hanging vine.
                        (*blocks)[i][j] = Block::grass10;

                        break;

                }
            }
        }
    }

    // fix ugly parts of grass
    for (int i = 50; i < 462; ++i) {
        for (int j = 50; j < 462; ++j) {
            if ((*blocks)[i][j] == Block::grass13
                && (*blocks)[i-1][j] == Block::grass2) {
                // found ugly part type 1
                (*blocks)[i][j] = Block::grass5;
                (*blocks)[i-1][j] = (*blocks)[i-2][j];
            }
            if ((*blocks)[i][j] == Block::grass12
                && (*blocks)[i-1][j] == Block::grass3
                && (*blocks)[i][j+1] == Block::grass9
                && (*blocks)[i-1][j+1] == Block::grass6) {
                // found ugly part type 2
                (*blocks)[i][j] = Block::grass5;
                (*blocks)[i-1][j] = (*blocks)[i-2][j];
                (*blocks)[i][j+1] = Block::grass5;
                (*blocks)[i-1][j+1] = (*blocks)[i-2][j+1];
            }
            if ((*blocks)[i][j] == Block::grass9
                && (*blocks)[i-1][j] == Block::grass6
                && (*blocks)[i][j+1] == Block::grass5
                && (*blocks)[i][j+2] == Block::grass12
                && (*blocks)[i-1][j+2] == Block::grass3) {
                // found ugly part type 3
                (*blocks)[i][j] = Block::air;
                (*blocks)[i-1][j] = Block::grass5;
                (*blocks)[i][j+1] = Block::air;
                (*blocks)[i-1][j+1] = Block::grass5;
                (*blocks)[i][j+2] = Block::air;
                (*blocks)[i-1][j+2] = Block::grass5;
            }
            if ((*blocks)[i][j] == Block::grass5
                && (*blocks)[i+1][j] == Block::grass10
                && !isGrass((*blocks)[i][j+1])
                && !isGrass((*blocks)[i][j-1])) {
                // found ugly part type 4
                // (hanging vine touching grassless floor)
                (*blocks)[i][j] = Block::grass2;
            }
        }
    }


    // seagrass
    for (int i = 110; i < 392; ++i) {
        for (int j = 120; j < 392; ++j) {
            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            if ((*blocks)[i][j] == Block::water
                && (*blocks)[i-1][j] == Block::sand
                && (*biome_map)[i/8][j/4] == Biome::ocean) {
                // found place for seagrass
                double seagrass_length = biome_here.seagrass_length;
                seagrass_length += perlin2->GetValue( x*0.4, y*0.4,
                        432) * SEAGRASS_VARIANCE;
                if (seagrass_length > 0) {
                    int k = 0;
                    while (k < seagrass_length) {
                        if ((*blocks)[i+k][j] != Block::water) {
                            // hit something
                            break;
                        }
                        (*blocks)[i+k][j] = Block::grass10;
                        k++;
                    }
                    if (k - seagrass_length > 0.5) {
                        (*blocks)[i+k][j] = Block::grass8;
                    }
                }
            }
        }
    }


    // dirtify (dirty)
    for (int i = 50; i < 462; ++i) {
        for (int j = 50; j < 462; ++j) {
            if ((isGrass((*blocks)[i][j])
                        || (*blocks)[i][j] == Block::fakegrass
                        || (*blocks)[i][j] == Block::fakegrasspond
                        || (*blocks)[i][j] == Block::fakegrassquicksand
                        )
                    && (*blocks)[i-1][j] == Block::B) {
                // dirtless biome?
                if (biome_here.dirt_depth < 1) {
                    continue;
                }
                Block dirtblock = Block::d;
                if (biome_here.sand) {
                    dirtblock = Block::sand;
                }
                (*blocks)[i-1][j] = dirtblock;
                // determine number of extra dirt layers
                for (int d = 1; d < biome_here.dirt_depth; d++) {
                    if ((*blocks)[i-1-d][j] == Block::B) {
                        (*blocks)[i-1-d][j] = dirtblock;
                    }
                    else {
                        break;
                    }
                }
            }
        }
    }

    // MORE DIRTY
    for (int i = 50; i < 462; ++i) {
        for (int j = 50; j < 462; ++j) {
            if ((*blocks)[i][j] == Block::B) {
                if ((*local_density)[i][j] < STONE_DENSITY_LIMIT
                        && (*local_density)[i][j] > TERRAIN_MINOR_ISLAND_REGION_THRESHOLD + 0.1
                        && (*density_dy)[i][j] > 0
                        ) {
                    // also check if biome has dirt at all
                    if(biome_here.dirt_depth > 0 && !biome_here.sand){
                        (*blocks)[i][j] = Block::d;
                    }
                }
            }
        }
    }

    // EVEN MORE DIRTY
    for (int i = 50; i < 462; ++i) {
        for (int j = 50; j < 462; ++j) {
            if ((*blocks)[i][j] == Block::B) {
                double density = (*local_density)[i][j];
                double channel = (*local_channel)[i][j];
                if (
                        (density > 0.27
                        && density < 0.32
                        && channel > -0.5
                        && channel < -0.4)
                        ) {
                    (*blocks)[i][j] = Block::d;
                }
            }
        }
    }

    // cellular automata between stone and dirt
    {
        std::array<std::array<int,512>,512>* cell_blocks =
            reinterpret_cast<std::array<std::array<int,512>,512>*>(blocks);

        Cellular2 cell_small(cell_blocks,
                WORLD_SEED, X*256-128, Y*256-128, 100);

        for (int it = 0; it < DIRT_CELLULAR_ITERATIONS; ++it) {
            cell_blocks = cell_small.iteration(
                    DIRT_CELLULAR_PROBS_TRUE,
                    DIRT_CELLULAR_PROBS_FALSE,
                    (int)Block::d,
                    (int)Block::B);
        }

        blocks = reinterpret_cast<std::array<std::array<Block,512>,512>*>(cell_blocks);
    }


    for (int j = 30; j < 482; j+=16) {
        // world coordinates (needed for deterministicness)
        int x = (X*256-128)+j;
        // y coordinate is irrelevant for this.
        // I want no variation vertically.
        // 383 is an arbitrary prime.
        int dx = noise4d(x, 383, 506, WORLD_SEED) % 6;
        for (int i = 30; i < 482; ++i) {
            if (!isAirOrFakegrass((*blocks)[i+1][j+dx])) continue;
            if ((*blocks)[i][j+dx] != Block::sand) continue;
            if (!biome_here.sand_dunes) continue;
            // become hole between dunes
            (*blocks)[i][j+dx] = Block::fakegrass;
            // change above fakegrass into air
            if ((*blocks)[i+1][j+dx] == Block::fakegrass) {
                (*blocks)[i+1][j+dx] = Block::air;
            }
        }
    }


    // sand dunes cellular automata
    for (int it = 0; it < 10; ++it) {
        for (int i = 30; i < 482; ++i) {
            for (int j = 481; j >= 30; --j) {
                if (!isAirOrFakegrass((*blocks)[i][j])) continue;
                if (!biome_here.sand_dunes) continue;
                // might become sand
                if ((
                    //   .x.
                    //   BBB.
                    //      B
                       (*blocks)[i-1][j] == Block::sand
                    && (*blocks)[i-1][j+1] == Block::sand
                    && (*blocks)[i-1][j-1] == Block::sand
                    && (*blocks)[i-2][j+2] == Block::sand
                    && isAirOrFakegrass((*blocks)[i][j+1])
                    && isAirOrFakegrass((*blocks)[i][j-1])
                    && isAirOrFakegrass((*blocks)[i-1][j+2])
                ) || (
                    //   .x.
                    //  .BBB 
                    //  B    
                       (*blocks)[i-1][j] == Block::sand
                    && (*blocks)[i-1][j+1] == Block::sand
                    && (*blocks)[i-1][j-1] == Block::sand
                    && (*blocks)[i-2][j+2] == Block::sand
                    && isAirOrFakegrass((*blocks)[i][j+1])
                    && isAirOrFakegrass((*blocks)[i][j-1])
                    && isAirOrFakegrass((*blocks)[i-1][j+2])
                ) || (
                    //     .
                    //   .xB 
                    //   BB  
                       (*blocks)[i][j+1] == Block::sand
                    && (*blocks)[i-1][j] == Block::sand
                    && (*blocks)[i-1][j-1] == Block::sand
                    && isAirOrFakegrass((*blocks)[i+1][j+1])
                    && isAirOrFakegrass((*blocks)[i][j-1])
                ) || (
                    //   .  
                    //   Bx. 
                    //    BB 
                       (*blocks)[i][j-1] == Block::sand
                    && (*blocks)[i-1][j] == Block::sand
                    && (*blocks)[i-1][j+1] == Block::sand
                    && isAirOrFakegrass((*blocks)[i+1][j-1])
                    && isAirOrFakegrass((*blocks)[i][j+1])
                )) {
                    (*blocks)[i][j] = Block::sand;
                    // change above air into fakegrass
                    if ((*blocks)[i+1][j] == Block::air) {
                        (*blocks)[i+1][j] = Block::fakegrass;
                    }
                }
            }
        }
    }

    // boulderify
    for (int i = 50; i < 462; ++i) {
        for (int j = 50; j < 462; ++j) {
            if ((*blocks)[i][j] != Block::B) continue;
            if ((*local_density)[i][j] < BOULDER_DENSITY_LIMIT) continue;
            (*blocks)[i][j] = Block::b;
            if ((*local_density)[i][j] < DIAMOND_DENSITY_LIMIT) continue;
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            // roll for diamond
            double generated = noise4df(x, y, 300, WORLD_SEED);
            if (generated < BOULDER_DIAMOND_PERCENTAGE) {
                // ok diamond.
                (*blocks)[i][j] = Block::DD;
            }
        }
    }


    // dirty pockets
    for (int i = 64; i < 448; i+=64) {
        for (int j = 64; j < 448; j+=64) {
            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            // roll for dirt pocket
            double generated = noise4df(x, y, 304, WORLD_SEED);
            if (generated > DIRT_POCKET_THRESHOLD) continue;
            // calculate coordinates of structure center
            int dx = j-32;
            int dy = i-32;
            int dxymod = noise4d(x, y, 302, WORLD_SEED)%4096;
            dx += dxymod/64;
            dy += dxymod%64;
            // choose dirt template:
            int chosen = noise4d(x, y, 307, WORLD_SEED)
                % DIRT_STRUCTURES.size();
            auto dirtmap = DIRT_STRUCTURES[chosen];
            if ((*blocks)[dy][dx] != Block::B) continue;
            // decide structure orientation
            int orientation = noise4d(x, y, 308, WORLD_SEED) % 8;
            // flip horizontally
            bool flipx = (orientation & 1) > 0;
            // flip vertically
            bool flipy = ((orientation>>1) & 1) > 0;
            // flip diagonally
            bool flipd = ((orientation>>2) & 1) > 0;
            // place the structure
            int size = dirtmap.size();
            for (int ddy = 0; ddy < size; ddy++) {
                for (int ddx = 0; ddx < size; ddx++) {
                    // modify coordinates for specific orientation
                    int ddxmod = ddx;
                    int ddymod = ddy;
                    if (flipx) ddxmod = size-ddxmod-1;
                    if (flipy) ddymod = size-ddymod-1;
                    if (flipd) std::swap(ddxmod, ddymod);
                    // use modified coordinates for sampling
                    Block newblock = dirtmap[ddymod][ddxmod];
                    if (newblock == Block::VOID) continue;
                    int worldx = dx - dirtmap[0].size()/2 + ddx;
                    int worldy = dy + dirtmap.size()/2 - ddy;
                    // dirt only replaces stone and diamond
                    Block block_here = (*blocks)[worldy][worldx];
                    if (block_here != Block::B
                            && block_here != Block::B) continue;
                    (*blocks)[worldy][worldx] = newblock;
                }
            }
        }
    }


    // oreify, orify, ores
    // DIMOND (diamond, for slashers. or even diamondify)
    // also another boulder
    for (int i = 104; i < 408; i+=16) {
        for (int j = 104; j < 408; j+=16) {
            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;

            double density = (*local_density)[i][j];

            // chance for diamond and boulder
            double dprob = DIAMOND_CHANCE_BASE;
            double bprob = BOULDER_CHANCE_BASE;
            double mprob = MAGNETITE_CHANCE_BASE;
            double gprob = GRAPHITE_CHANCE_BASE;
            // bonus for island:
            if(density < TERRAIN_MINOR_ISLAND_REGION_THRESHOLD) {
                dprob += DIAMOND_CHANCE_ISLAND_BONUS;
                bprob += BOULDER_CHANCE_ISLAND_BONUS;
                mprob += MAGNETITE_CHANCE_ISLAND_BONUS;
                gprob += GRAPHITE_CHANCE_ISLAND_BONUS;
            }
            if((*biome_map)[i/8][j/4] == Biome::flower) {
                dprob += DIAMOND_CHANCE_FLOWER_ISLAND_BONUS;
            }
            // bonus for edge of mountain:
            else if(density < TERRAIN_MINOR_REGION_THRESHOLD) {
                dprob += DIAMOND_CHANCE_MINOR_BONUS;
                bprob += BOULDER_CHANCE_MINOR_BONUS;
                mprob += MAGNETITE_CHANCE_MINOR_BONUS;
                gprob += GRAPHITE_CHANCE_MINOR_BONUS;
            }

            // make cumulative
            double dthreshold = dprob;
            double bthreshold = bprob + dthreshold;
            double mthreshold = mprob + bthreshold;
            double gthreshold = gprob + mthreshold;

            // roll for all ores
            double generated = noise4df(x, y, 300, WORLD_SEED);

            // structure will go into this array
            std::array<std::array<Block,9>,9> oremap;
            int seed = noise4d(x, y, 303, WORLD_SEED);

            // calculate coordinates of structure center
            int dx = j-10;
            int dy = i-10;
            int dxymod = noise4d(x, y, 302, WORLD_SEED)%400;
            dx += dxymod/20;
            dy += dxymod%20;
            // must be stone at structure center
            if ((*blocks)[dy][dx] != Block::B) continue;

            bool force = false;
            bool allow_rotation = false;

            if (generated < dthreshold) {
                // diamond was chosen
                // make a structure of void and just 1 diamond
                std::fill(&oremap[0][0], &oremap[8][8]+1, Block::VOID);
                std::mt19937_64 gen;
                gen.seed(seed);
                for (int p = 0; p < 3; p++) {
                    int dx = -2 + gen() % 5;
                    int dy = -2 + gen() % 5;
                    oremap[4+dy][4+dx] = Block::pinkB;
                }
                oremap[4][4] = Block::DD;
            }
            else if (generated < bthreshold) {
                int chosen_ind = seed % UNDERGROUND_BOULDERS.size();
                int chosen = UNDERGROUND_BOULDERS[chosen_ind];
                oremap = BOULDER_STRUCTURES[chosen];
                force = true;
            }
            else if (generated < mthreshold) {
                int chosen = seed % MAGNETITE_STRUCTURES.size();
                oremap = MAGNETITE_STRUCTURES[chosen];
                allow_rotation = true;
            }
            else if (generated < gthreshold) {
                int chosen = seed % GRAPHITE_STRUCTURES.size();
                oremap = GRAPHITE_STRUCTURES[chosen];
                allow_rotation = true;
            }
            else if ((*biome_map)[dy/8][dx/4] == Biome::underground) {
                // simple 1-block boulder
                (*blocks)[dy][dx] = Block::b;
                continue;
            }
            else {
                // no ore here
                continue;
            }
            // decide structure orientation
            int orientation = 0;
            if (allow_rotation) {
                orientation = noise4d(x, y, 308, WORLD_SEED) % 8;
            }
            // flip horizontally
            bool flipx = (orientation & 1) > 0;
            // flip vertically
            bool flipy = ((orientation>>1) & 1) > 0;
            // flip diagonally
            bool flipd = ((orientation>>2) & 1) > 0;
            // place the structure
            for (int ddy = 0; ddy < 9; ddy++) {
                for (int ddx = 0; ddx < 9; ddx++) {
                    // modify coordinates for specific orientation
                    int ddxmod = ddx;
                    int ddymod = ddy;
                    if (flipx) ddxmod = 9-ddxmod-1;
                    if (flipy) ddymod = 9-ddymod-1;
                    if (flipd) std::swap(ddxmod, ddymod);
                    // use modified coordinates for sampling
                    Block newblock = oremap[ddymod][ddxmod];
                    if (newblock == Block::VOID) continue;
                    int worldx = dx - 4 + ddx;
                    int worldy = dy + 4 - ddy;
                    Block oldblock = (*blocks)[worldy][worldx];
                    if (!force && oldblock != Block::B) {
                        // skip because we're only replacing stone
                        continue;
                    }
                    (*blocks)[worldy][worldx] = newblock;
                }
            }

        }
    }


    // GIANT boulder pillars in desert
    {
        int seed = noise4d(X, Y, 2104, WORLD_SEED)
            + 1000*noise4d(X, Y, 2106, WORLD_SEED);
        std::mt19937_64 gen;
        gen.seed(seed);
        // pick a random position inside this chunk
        int pillarx = 128 + 20 + gen() % (256-40);
        int pillary = 128 + 20 + gen() % (256-40);
        bool foundposition = false;
        for (int i = 0; i < DESERT_PILLAR_ATTEMPTS; i++) {
            while ((*blocks)[pillary][pillarx] != Block::sand
                    && pillary > 128+5) {
                pillary--;
            }
            while (!isAirOrFakegrass((*blocks)[pillary+1][pillarx]) && pillary < 128+256-40) {
                pillary++;
            }

//        -15       -10       -5         0         5        +10       +15
//37                                     x
//36 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//35 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//34 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//33 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//32 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//31 _ _ _ _ _ _ _ _ _ _ _ _ x _ _ b b b b b b _ _ _ x _ _ _ _ _ _ _ _ _ _ _ _
//30 _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//29 _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//28 _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//27 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//26 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//25 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b x b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//24 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//23 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//22 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//21 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//20 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//19 _ _ _ _ _ _ _ _ _ _ _ _ x _ _ _ b b b _ _ _ _ _ x _ _ _ _ _ _ _ _ _ _ _ _
//18 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//17 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//16 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//15 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//14 _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//13 _ _ _ _ _ _ x _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ x _ _ _ _ _ _
//12 _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//11 _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//10 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//9  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//8  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//7  _ _ _ _ _ _ _ _ _ _ _ _ x _ _ b b b b b b _ _ _ x _ _ _ _ _ _ _ _ _ _ _ _
//6  _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//5  _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//4  _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//3  _ _ _ x _ _ _ _ _ _ _ _ b b b b b b b b b b _ _ _ _ _ _ _ _ _ _ _ x _ _ _
//2  _ _ _ _ _ _ _ _ _ _ _ b b b b b b b b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _
//1  _ _ _ _ _ _ _ _ _ _ b b b b b b b b x b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _
//0  _ _ _ _ _ _ _ _ _ _ b b b b b b b b s b b b b b _ _ _ _ _ _ _ _ _ _ _ _ _
//-1   s                 s       s               s       s                 s
//-2
//-3
//-4
//-5
//-6           o                         o                         o
//-7      -15       -10       -5         0         5        +10       +15

            if ((*biome_map)[pillary/8][pillarx/4] == Biome::desert
                    && (*blocks)[pillary][pillarx] == Block::sand
                    && isAirOrFakegrass((*blocks)[pillary+1][pillarx])
                    && isAirOrFakegrass((*blocks)[pillary+37][pillarx+0])
                    && isAirOrFakegrass((*blocks)[pillary+31][pillarx+6])
                    && isAirOrFakegrass((*blocks)[pillary+31][pillarx-6])
                    && isAirOrFakegrass((*blocks)[pillary+25][pillarx+0])
                    && isAirOrFakegrass((*blocks)[pillary+19][pillarx+6])
                    && isAirOrFakegrass((*blocks)[pillary+19][pillarx-6])
                    && isAirOrFakegrass((*blocks)[pillary+13][pillarx+12])
                    && isAirOrFakegrass((*blocks)[pillary+13][pillarx-12])
                    && isAirOrFakegrass((*blocks)[pillary+7][pillarx+6])
                    && isAirOrFakegrass((*blocks)[pillary+7][pillarx-6])
                    && isAirOrFakegrass((*blocks)[pillary+3][pillarx+15])
                    && isAirOrFakegrass((*blocks)[pillary+3][pillarx-15])
                    && (*blocks)[pillary-1][pillarx-17] == Block::sand
                    && (*blocks)[pillary-1][pillarx-8] == Block::sand
                    && (*blocks)[pillary-1][pillarx-4] == Block::sand
                    && (*blocks)[pillary-1][pillarx+4] == Block::sand
                    && (*blocks)[pillary-1][pillarx+8] == Block::sand
                    && (*blocks)[pillary-1][pillarx+17] == Block::sand
                    && !isAir((*blocks)[pillary-6][pillarx-13])
                    && !isAir((*blocks)[pillary-6][pillarx+0])
                    && !isAir((*blocks)[pillary-6][pillarx+13])
                    ) {
                foundposition = true;
                break;
            }
            pillarx = 128 + 20 + gen() % (256-40);
            pillary = 128 + 20 + gen() % (256-40);
        }
        if (foundposition) {
            // choose desert pillar structure
            int chosen = gen() % DESERT_PILLAR_STRUCTURES.size();
            auto pillarmap = DESERT_PILLAR_STRUCTURES[chosen];
            // decide structure orientation
            int orientation = gen() % 8;
            // flip horizontally
            bool flipx = (orientation & 1) > 0;
            // place the structure
            int size = pillarmap.size();
            for (int ddy = 0; ddy < size; ddy++) {
                for (int ddx = 0; ddx < size; ddx++) {
                    // modify coordinates for specific orientation
                    int ddxmod = ddx;
                    int ddymod = ddy;
                    if (flipx) ddxmod = size-ddxmod-1;
                    // use modified coordinates for sampling
                    Block newblock = pillarmap[ddymod][ddxmod];
                    if (newblock == Block::VOID) continue;
                    int localx = pillarx - pillarmap[0].size()/2 + ddx;
                    int localy = pillary + pillarmap.size() - 1 - ddy;
                    (*blocks)[localy][localx] = newblock;
                }
            }
        }
    }


    // boulders, for the fourth time. I like boulders
    for (int i = 45; i < 467; ++i) {
        for (int j = 45; j < 467; ++j) {
            // needs some terrain depth for placing boulder:
            if (isAir((*blocks)[i-2][j])
                    || isAir((*blocks)[i-3][j])) {
                // not deep enough here
                continue;
            }

            double boulder_chance;
            const std::vector<int> *boulder_collection;
            if (((*blocks)[i][j] == Block::air
                        || isGrassf((*blocks)[i][j])
                ) && ((*blocks)[i-1][j] == Block::d
                    || (*blocks)[i-1][j] == Block::sand)
               ) {
                boulder_chance = biome_here.boulder_chance;
                boulder_collection = &SURFACE_BOULDERS;
            }
            else if ((*blocks)[i][j] == Block::cave
                    && (*blocks)[i-1][j] == Block::B
                    ) {
                boulder_chance = BOULDER_CHANCE_CAVE;
                boulder_collection = &CAVE_BOULDERS;
            }
            else if (((*blocks)[i][j] == Block::air
                        || isGrassf((*blocks)[i][j]))
                    && (*blocks)[i-1][j] == Block::B
                    ) {
                boulder_chance = biome_here.boulder_chance;
                boulder_collection = &CAVE_BOULDERS;
            }
            else if (((*blocks)[i][j] == Block::water
                        || isGrass((*blocks)[i][j])
                        || isCoral((*blocks)[i][j]))
                    && (*blocks)[i-1][j] == Block::sand
                    && (*biome_map)[i/8][j/4] == Biome::ocean
                    ) {
                boulder_chance = BOULDER_CHANCE_OCEAN;
                boulder_collection = &OCEAN_BOULDERS;
            }
            else {
                // no chance
                continue;
            }

            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            // roll for boulder
            double generated = noise4df(x, y, 300, WORLD_SEED);
            if (generated > boulder_chance) continue;
            // choose boulder template:
            int seed = noise4d(x, y, 303, WORLD_SEED);
            int chosen_ind = seed % boulder_collection->size();
            int chosen = boulder_collection->at(chosen_ind);

            auto bouldermap = BOULDER_STRUCTURES[chosen];

            // prevent overlapping boulders
            bool yesboulder = true;
            for (size_t ddy = 0; ddy < bouldermap.size(); ddy++) {
                for (size_t ddx = 0; ddx < bouldermap[0].size(); ddx++) {
                    Block newblock = bouldermap[ddy][ddx];
                    if (newblock == Block::VOID) continue;
                    int worldx = j - bouldermap.size()/2 + ddx;
                    int worldy = i + bouldermap[0].size()/2 - ddy;
                    if ((*blocks)[worldy][worldx] == Block::b) {
                        yesboulder = false;
                        break;
                    }
                }
            }

            if (!yesboulder) {
                continue;
            }

            // place the structure
            for (size_t ddy = 0; ddy < bouldermap.size(); ddy++) {
                for (size_t ddx = 0; ddx < bouldermap[0].size(); ddx++) {
                    Block newblock = bouldermap[ddy][ddx];
                    if (newblock == Block::VOID) continue;
                    int worldx = j - bouldermap[0].size()/2 + ddx;
                    int worldy = i + bouldermap.size()/2 - ddy;
                    (*blocks)[worldy][worldx] = newblock;
                }
            }

        }
    }

    // parkourify
    for (int i = 45; i < 467; ++i) {
        for (int j = 45; j < 467; ++j) {
            if (!isGrassf((*blocks)[i][j])) continue;
            if ((*biome_map)[i/8][j/4] != Biome::glacier) continue;
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            double temperature = perlin2->GetValue(
                    x*BIOME_TEMPERATURE_FREQUENCY,
                    y*BIOME_TEMPERATURE_FREQUENCY, 3003);
            // temperature is between -2 and -1 in this biome
            // we want parkour the highest at -2
            // -temperature is between 1 and 2
            // (-temperaure-1)*2 is between 0 and 2
            double parkour_max_height = (-temperature-1)*GLACIER_PARKOUR_AMPLITUDE;
            int parkourheight = noise4df(x, y, 220, WORLD_SEED)*parkour_max_height;
            for (int p = 0; p < parkourheight; p++) {
                if (!isAir((*blocks)[i+p][j])
                        && !isGrassf((*blocks)[i+p][j])) break;
                if (noise4df(x, y+p, 223, WORLD_SEED)<GLACIER_ICEBLOCK_CHANCE) {
                    (*blocks)[i+p][j] = Block::iceblock;
                } else {
                    (*blocks)[i+p][j] = Block::B;
                }
            }
        }
    }


    // tentaclify. rock tentacles. stone tentacles. tenticles.
    for (int i = 45; i < 467; ++i) {
        for (int j = 45; j < 467; ++j) {
            if ((*biome_map)[i/8][j/4] != Biome::tentacle) {
                // wrong biome
                continue;
            }
            if ((*density_dy)[i][j] < 0) {
                // wrong direction
                continue;
            }
            // spots marked o must be B
            // spots marked x must be air or tentacle
            // dot (.) represents structure origin

            //   -1 0 1 2 3 4 5 6 7 8 9 10
            //  1       o         o
            //  0 o . B B B B B B B B B o
            // -1   B B B B B B B B B B
            // -2   B B B B B B B B B _
            // -3   _ B B B B B B B _ _
            // -4   _ B B B B B B B _ _
            // -5   _ B x B B B B x _ _
            // -6   _ B B B B B B _ _ _
            // -7   B B B B B B _ _ _ _
            // -8   B B B B B B _ _ _ _
            // -9   B B B B B B _ _ _ _
            //-10   B B B B B _ _ _ _ _
            //-11   _ B B B B _ _ _ _ _
            //-12 x _ B B B _ _ _ _ _ _ x
            //-13   _ B B B B _ _ _ _ _
            //-14   _ _ B B B _ _ _ _ _
            //-15   _ _ _ B B B _ _ _ _
            //-16   _ _ _ _ B B _ _ _ _
            //-17   _ _ _ _ _ B B _ _ _
            //-18
            //-19       x         x
            //   -1 0 1 2 3 4 5 6 7 8 9 10

            if ((*blocks)[i][j-1] == Block::B
                    && (*blocks)[i+1][j+1] == Block::B
                    && (*blocks)[i+2][j+7] == Block::B
                    && (*blocks)[i][j+10] == Block::B
                    && isAir((*blocks)[i-5][j+2])
                    && isAir((*blocks)[i-5][j+7])
                    && isAir((*blocks)[i-19][j+2])
                    && isAir((*blocks)[i-19][j+7])){
                // ok it is a nice spot for this structure

                int x = (X*256-128)+j;
                int y = (Y*256-128)+i;
                // roll for tentacle
                double generated = noise4df(x, y, 306, WORLD_SEED);
                if (generated > ROCK_TENTACLE_CHANCE) continue;
                // choose tentacle template:
                int seed = noise4d(x, y, 309, WORLD_SEED);
                int chosen = seed % ROCK_TENTACLE_STRUCTURES.size();
                auto tent_map = ROCK_TENTACLE_STRUCTURES[chosen];

                // place the structure
                for (size_t ddy = 0; ddy < tent_map.size(); ddy++) {
                    for (size_t ddx = 0; ddx < tent_map[0].size(); ddx++) {
                        Block newblock = tent_map[ddy][ddx];
                        if (newblock == Block::VOID) continue;
                        int worldx = j + ddx;
                        int worldy = i - ddy;
                        (*blocks)[worldy][worldx] = newblock;
                    }
                }
            }
        }
    }
    // now turn all these tentacles to actual stone
    // simple replacement of wannaberock with B
    for (int i = 44; i < 484; ++i) {
        for (int j = 44; j < 478; ++j) {
            if ((*blocks)[i][j] == Block::wannaberock){
                (*blocks)[i][j] = Block::B;
            }
        }
    }


    { // warp biome warps
        std::array<std::array<Eye,32>,32> eyes;
        for (int i = 3; i < 30; i++) {
            for (int j = 3; j < 30; j++) {
                // world coordinates (needed for deterministicness)
                int x = (X*256-128)+j*16;
                int y = (Y*256-128)+i*16;
                if ((i+j)%2 == 0) {
                    int dxy= noise4d(x, y, 302, WORLD_SEED) % 144;
                    int dx = dxy/12;
                    int dy = dxy%12;
                    eyes[i][j].i = i*16 + dy;
                    eyes[i][j].j = j*16 + dx;
                }
            }
        }
        // Make the connections
        for (int i = 4; i < 29; i++) {
            for (int j = 4; j < 30; j++) {
                if ((i+j)%2 == 0) {
                    Eye eyehere = eyes[i][j];
                    Eye eyeabove = eyes[i+1][j-1];
                    Eye eyebelow = eyes[i-1][j-1];
                    for (double delta = 0; delta < 1; delta += 1.0/32) {
                        int midi = (eyehere.i+delta*(eyeabove.i-eyehere.i));
                        int midj = (eyehere.j+delta*(eyeabove.j-eyehere.j));
                        if (!isAirOrFakegrass((*blocks)[midi][midj])){
                            continue;
                        }
                        if ((*biome_map)[midi/8][midj/4] != Biome::warp) {
                            // tentacle is only in warp biome
                            continue;
                        }
                        double gap = (*local_gap)[midi][midj];
                        if (gap < ISLAND_BIOME_GAP_2+ISLAND_BIOME_INNER_GAP_SIZE) {
                            // there is gap here
                            continue;
                        }
                        (*blocks)[midi][midj] = Block::wannabetentacle;
                    }
                    for (double delta = 0; delta < 1; delta += 1.0/32) {
                        int midi = (eyehere.i+delta*(eyebelow.i-eyehere.i));
                        int midj = (eyehere.j+delta*(eyebelow.j-eyehere.j));
                        if (!isAirOrFakegrass((*blocks)[midi][midj])){
                            continue;
                        }
                        if ((*biome_map)[midi/8][midj/4] != Biome::warp) {
                            // tentacle is only in warp biome
                            continue;
                        }
                        double gap = (*local_gap)[midi][midj];
                        if (gap < ISLAND_BIOME_GAP_2+ISLAND_BIOME_INNER_GAP_SIZE) {
                            // there is gap here
                            continue;
                        }
                        (*blocks)[midi][midj] = Block::wannabetentacle;
                    }
                }
            }
        }
        // Tentacle cellular automata
        std::array<std::array<Block,512>,512> *blocks2 =
            new std::array<std::array<Block,512>,512>();
        for (int it = 0; it < 2; it++) {
            if (it == 1) {
                for (int i = 48; i < 464; ++i) {
                    for (int j = 48; j < 464; ++j) {
                        if ((*blocks)[i][j] != Block::wannabetentacle) {
                            continue;
                        }
                        // world coordinates (needed for deterministicness)
                        int x = (X*256-128)+j;
                        int y = (Y*256-128)+i;
                        double chance = noise4df(x, y, 392, WORLD_SEED);
                        if (chance > TENTACLE_STRUCTURE_DENSITY) {
                            (*blocks)[i][j] = Block::air;
                        }
                    }
                }
            }
            for (int i = 48; i < 464; ++i) {
                for (int j = 48; j < 464; ++j) {
                    if ((*biome_map)[i/8][j/4] != Biome::warp) {
                        // tentacle is only in warp biome
                        (*blocks2)[i][j] = (*blocks)[i][j];
                        continue;
                    }
                    /* int sum = 0; */

                    // unrolled loop:
                    int sum
                        = ((*blocks)[i-1][j-1] == Block::wannabetentacle)
                        + ((*blocks)[i-1][j+0] == Block::wannabetentacle)
                        + ((*blocks)[i-1][j+1] == Block::wannabetentacle)
                        + ((*blocks)[i+0][j-1] == Block::wannabetentacle)
                        + ((*blocks)[i+0][j+1] == Block::wannabetentacle)
                        + ((*blocks)[i+1][j-1] == Block::wannabetentacle)
                        + ((*blocks)[i+1][j+0] == Block::wannabetentacle)
                        + ((*blocks)[i+1][j+1] == Block::wannabetentacle);

                    if ((*blocks)[i][j] == Block::wannabetentacle) {
                        if (sum < 1) {
                            (*blocks2)[i][j] = Block::air;
                        }
                        else {
                            (*blocks2)[i][j] = Block::wannabetentacle;
                        }
                    }
                    else if (isAirOrFakegrass((*blocks)[i][j])
                            || (*blocks)[i][j] == Block::pebble) {
                        if (sum < 2) {
                            (*blocks2)[i][j] = (*blocks)[i][j];
                        }
                        else {
                            (*blocks2)[i][j] = Block::wannabetentacle;
                        }
                    }
                    else {
                        // this covers all weird blocks that might appear
                        // such as trees, hanging tentacles ...
                        (*blocks2)[i][j] = (*blocks)[i][j];
                    }
                }
            }
            std::array<std::array<Block,512>,512> *temp = blocks;
            blocks = blocks2;
            blocks2 = temp;
        }
        delete blocks2;
        // place eye structures at intersections
        for (int i = 3; i < 30; i++) {
            for (int j = 3; j < 30; j++) {
                if ((i+j)%2 == 0) {
                    Eye eye= eyes[i][j];
                    // world coordinates (needed for deterministicness)
                    int x = (X*256-128)+eye.j;
                    int y = (Y*256-128)+eye.i;
                    if ((*biome_map)[eye.i/8][eye.j/4] != Biome::warp) {
                        continue;
                    }
                    if ((*blocks)[eye.i][eye.j] != Block::wannabetentacle) {
                        continue;
                    }
                    double gap = (*local_gap)[eye.i][eye.j];
                    if (gap < ISLAND_BIOME_GAP_2+ISLAND_BIOME_INNER_GAP_SIZE) {
                        // there is gap here
                        continue;
                    }
                    // roll for warp eye
                    double value = noise4df(x, y, 2204, WORLD_SEED);
                    if (value < WARP_BIOME_WARP_CHANCE) {
                        // warp eye
                        for (size_t dy = 0; dy < EYE_WARP.size(); dy++) {
                            for (size_t dx = 0; dx < EYE_WARP[0].size(); dx++) {
                                Block newblock = EYE_WARP[dy][dx];
                                if (newblock == Block::VOID) continue;
                                (*blocks)[eye.i+3-dy][eye.j-3+dx] = newblock;
                            }
                        }

                        // choose target
                        int xregion = X<0 ? (X-7)/8 : X/8;
                        int yregion = Y<0 ? (Y-7)/8 : Y/8;
                        int targetregiondx = 0;
                        int targetregiondy = 0;
                        int warpmaxrange = 2;
                        int seed = noise4d(eye.i, eye.j, 2004, WORLD_SEED);
                        std::mt19937_64 gen;
                        gen.seed(seed);
                        // pick a region to target
                        while (targetregiondx == 0 && targetregiondy == 0) {
                            targetregiondx = gen() % (2*warpmaxrange+1) - warpmaxrange;
                            targetregiondy = gen() % (2*warpmaxrange+1) - warpmaxrange;
                        }
                        int targetregionx = xregion + targetregiondx;
                        int targetregiony = yregion + targetregiondy;
                        // find warp in that region
                        auto target_warp = find_warp_position(
                                targetregionx, targetregiony, perlin2, WORLD_SEED);
                        // unpack coordinates
                        int warp_target_x = target_warp.first;
                        int warp_target_y = target_warp.second;
                        // store this information in tile entity
                        tileEntities.push_back(new Warp(
                                    x,
                                    y-2,
                                    warp_target_x,
                                    warp_target_y));

                    }
                    else {
                        // ruby eye
                        // choose eye template:
                        int seed = noise4d(x, y, 2206, WORLD_SEED);
                        int chosen = seed % EYES_RUBY.size();
                        auto eyemap = EYES_RUBY[chosen];
                        // place the structure
                        for (size_t dy = 0; dy < eyemap.size(); dy++) {
                            for (size_t dx = 0; dx < eyemap[0].size(); dx++) {
                                Block newblock = eyemap[dy][dx];
                                if (newblock == Block::VOID) continue;
                                (*blocks)[eye.i+1-dy][eye.j-5+dx] = newblock;
                            }
                        }
                    }

                }
            }
        }

    }
    // now turn all these wannabes to actual tentacles
    for (int i = 48; i < 464; ++i) {
        for (int j = 48; j < 464; ++j) {
            if ((*blocks)[i][j] == Block::wannabetentacle){
                (*blocks)[i][j] = Block::WA;
            }
        }
    }


    // four fort
    {
        // what is current fort region?
        int xregion = X<0 ? (X-11)/12 : X/12;
        int yregion = Y<0 ? (Y-11)/12 : Y/12;
        // find fort
        auto global_forg = find_forg_position(
                xregion, yregion, perlin2, WORLD_SEED);
        // unpack coordinates
        int forglocationx = global_forg.first;
        int forglocationy = global_forg.second;
        int fortoriginx = forglocationx - 512;
        int fortoriginy = forglocationy - 512;
        // stats
        if (forglocationy % 2 == 0) {
            // forg was not cancelled
            infos.push_back(Info("forg", forglocationx, forglocationy));
        } else {
            // forg was cancelled, but I guess there is no harm in
            // processing the rest of this block, so I'm leaving it enabled
        }

        int seed = noise4d(xregion, yregion, 2006, WORLD_SEED);
        std::mt19937_64 gen;
        gen.seed(seed);

        std::array<std::array<bool,1024>,1024> visited{};

        std::deque<FortNode> queue;
        FortNode start;
        start.x = 512;
        start.y = 512;
        start.orientation = 0;
        start.depth = 0;
        queue.push_back(start);

        std::vector<int> num_generated_sections_per_type(FORT_SECTIONS.size(), 0);
        int num_allowed_dead_ends = -2;

        while (queue.size() > 0) {
            FortNode node = queue.front();
            queue.pop_front();
            if (node.depth > 100) continue;
            // Get terrain density at this node
            const int node_x_within_world = node.x + fortoriginx;
            const int node_y_within_world = node.y + fortoriginy;
            double density = perlin2->GetValue(
                    node_x_within_world*TERRAIN_DENSITY_FREQUENCY_X,
                    node_y_within_world*TERRAIN_DENSITY_FREQUENCY_Y,
                    0);
            // Filter out mismatching sections
            std::vector<MirrorableFortSection> good_sections;
            for (unsigned int i = 0; i < FORT_SECTIONS.size(); i++) {
                auto &section = FORT_SECTIONS[i];
                if (node.depth < section.mindepth) continue;
                if (node.depth > section.maxdepth) continue;
                if (num_generated_sections_per_type[i] >= section.limit) continue;
                // do not allow dead end if no capacity for dead ends
                if (num_allowed_dead_ends < 1 && section.connections.size() < 1) continue;
                // do not allow if beyond allowed terrain density
                if (density < section.mindensity) continue;
                if (density > section.maxdensity) continue;
                // try to add unmirrored
                if (node.orientation == section.orientation) {
                    good_sections.push_back({i, false});
                }
                const int mirrored_orientaion = section.orientation % 2 == 0
                    ? (section.orientation + 2) % 4
                    : section.orientation;
                if (node.orientation == mirrored_orientaion) {
                    good_sections.push_back({i, true});
                }
            }

            int chosen_index = 0;
            while (good_sections.size() > 0) {
                chosen_index = gen() % good_sections.size();
                const MirrorableFortSection &msection = good_sections[chosen_index];
                const FortSection &section = FORT_SECTIONS[msection.index];
                const bool mirrored = msection.mirrored;
                const int section_height = section.blocks.size();
                const int section_width = section.blocks[0].size();
                const int real_offsetx = mirrored
                    ? section_width - 1 - section.offsetx
                    : section.offsetx;
                const int section_x_within_fort_area = node.x-real_offsetx;
                const int section_y_within_fort_area = node.y+section.offsety;
                bool is_overlaping = false;
                for (int i = 0; i < section_height; i++) {
                    for (int j = 0; j < section_width; j++) {
                        // check if it has a block at this location
                        if (section.blocks[i][j] == Block::VOID) continue;
                        // check if overlaping visited
                        const int real_j = mirrored ? section_width - 1 - j : j;
                        if (visited [section_y_within_fort_area-i]
                            [section_x_within_fort_area+real_j]) {
                            // overlap!
                            // reject this candidate section:
                            is_overlaping = true;
                            break;
                        }
                    }
                    if (is_overlaping) break;
                }
                if (is_overlaping) {
                    // Remove section from list of candidates.
                    good_sections.erase(good_sections.begin() + chosen_index);
                } else {
                    // found a good section with no overlap!
                    // This one will be used. No need to continue this loop.
                    break;
                }
            }

            if (good_sections.size() == 0) {
                // no available sections! Increment depth
                FortNode next;
                next.x = node.x;
                next.y = node.y;
                next.orientation = node.orientation;
                next.depth = node.depth+1;
                queue.push_back(next);
                continue;
            }

            const MirrorableFortSection &msection = good_sections[chosen_index];
            const FortSection &section = FORT_SECTIONS[msection.index];
            const bool mirrored = msection.mirrored;
            const int section_height = section.blocks.size();
            const int section_width = section.blocks[0].size();
            const int real_offsetx = mirrored
                ? section_width - 1 - section.offsetx
                : section.offsetx;

            // Get local coordinates in different coordinate systems
            const int section_x_within_fort_area = node.x - real_offsetx;
            const int section_y_within_fort_area = node.y + section.offsety;
            const int section_x_within_world = section_x_within_fort_area + fortoriginx;
            const int section_y_within_world = section_y_within_fort_area + fortoriginy;
            const int section_x_within_chunk = section_x_within_world - X*256;
            const int section_y_within_chunk = section_y_within_world - Y*256;
            const int section_x_within_generation_area = section_x_within_chunk + 128;
            const int section_y_within_generation_area = section_y_within_chunk + 128;

            // place the section structure
            for (int i = 0; i < section_height; i++) {
                for (int j = 0; j < section_width; j++) {
                    // possibly mirror x coordinate:
                    const int real_j = mirrored ? section_width - 1 - j : j;
                    // skip if void
                    if (section.blocks[i][j] == Block::VOID) {
                        // check if it happens to be within the chunk:
                        if (section_x_within_chunk+real_j < 0
                                || section_x_within_chunk+real_j >= 256
                                || section_y_within_chunk-i < 0
                                || section_y_within_chunk-i >= 256) {
                            // no checks needed outside of the chunk
                            continue;
                        }
                        // trigger a water update if there is naturally water here
                        if (isWater((*blocks)
                                    [section_y_within_generation_area-i]
                                    [section_x_within_generation_area+real_j])) {
                            WaterUpdate *w = new WaterUpdate();
                            w->x = section_x_within_world+real_j;
                            w->y = section_y_within_world-i;
                            waterUpdates.push_back(w);
                        }
                        continue;
                    }
                    // mark as visited
                    visited [section_y_within_fort_area-i]
                        [section_x_within_fort_area+real_j] = true;
                    // check if within generated area
                    if (section_x_within_generation_area+real_j > 64
                            && section_x_within_generation_area+real_j < 448
                            && section_y_within_generation_area-i > 64
                            && section_y_within_generation_area-i < 448) {
                        // place the structure
                        (*blocks)
                            [section_y_within_generation_area-i]
                            [section_x_within_generation_area+real_j]
                                = section.blocks[i][j];
                        // check if block within chunk:
                        if (section_x_within_chunk+real_j < 0
                                || section_x_within_chunk+real_j >= 256
                                || section_y_within_chunk-i < 0
                                || section_y_within_chunk-i >= 256) {
                            // do not create entities outside of this chunk!
                            continue;
                        }
                        // create entities if needed:
                        if (section.blocks[i][j] == Block::chest) {
                            int seed = noise4d(section_x_within_world, section_y_within_world, 4447, WORLD_SEED)
                                + 1000*noise4d(xregion, yregion, 4449, WORLD_SEED);
                            tileEntities.push_back(new FortChest(
                                        section_x_within_world+real_j,
                                        section_y_within_world-i,
                                        seed));
                        }
                        if (section.blocks[i][j] == Block::forg) {
                            tileEntities.push_back(new Forg(
                                        section_x_within_world+real_j,
                                        section_y_within_world-i));
                        }
                        if (section.blocks[i][j] == Block::water) {
                            WaterUpdate *w = new WaterUpdate();
                            w->x = section_x_within_world+real_j;
                            w->y = section_y_within_world-i;
                            waterUpdates.push_back(w);
                        }
                    }
                }
            }

            // claim this section
            Claim *c = new Claim();
            c->x = section_x_within_world;
            c->y = section_y_within_world - section_height + 1;
            c->x2 = section_x_within_world + section_width - 1;
            c->y2 = section_y_within_world;
            claims.push_back(c);

            // increment connection counter
            num_generated_sections_per_type[msection.index]++;

            // prepare connections to future section
            for (auto connection : section.connections) {
                FortNode next;
                next.y = section_y_within_fort_area - connection.offsety;
                if (mirrored) {
                    next.orientation = connection.orientation % 2 == 0
                        ? (connection.orientation + 2) % 4
                        : connection.orientation;
                    next.x = section_x_within_fort_area + section_width - 1 - connection.offsetx;
                } else {
                    next.orientation = connection.orientation;
                    next.x = section_x_within_fort_area + connection.offsetx;
                }
                next.depth = node.depth+1;
                queue.push_back(next);
            }

            // keep track of how many paths are being generated
            num_allowed_dead_ends += section.connections.size() - 1;
        }
    }


    { // random warps
        // what is current warp region?
        int xregion = X<0 ? (X-7)/8 : X/8;
        int yregion = Y<0 ? (Y-7)/8 : Y/8;
        // find warp
        auto global_warp = find_warp_position(
                xregion, yregion, perlin2, WORLD_SEED);
        // unpack coordinates
        int global_warp_x = global_warp.first;
        int global_warp_y = global_warp.second;
        // coordinates relative to current generated area:
        int dxlocal = global_warp_x - X*256+128;
        int dylocal = global_warp_y - Y*256+128;
        // stats
        infos.push_back(Info("warp", global_warp_x, global_warp_y));

        // is it within the area we are generating?
        if (dxlocal > 64 && dxlocal < 448
                && dylocal > 64 && dylocal < 448) {
            // place the structure
            for (size_t i = 0; i < WARP_STRUCTURE.size(); i++) {
                for (size_t j = 0; j < WARP_STRUCTURE[0].size(); j++) {
                    Block newblock = WARP_STRUCTURE[i][j];
                    if (newblock == Block::VOID) continue;
                    (*blocks)
                        [dylocal+WARP_STRUCTURE_OFFSET_Y-i]
                        [dxlocal-WARP_STRUCTURE_OFFSET_X+j]
                            = newblock;
                }
            }
            // is the tile entity within this chunk?
            if (dxlocal > 127 && dxlocal < 384
                    && dylocal > 127 && dylocal < 384) {
                // choose target
                int targetregiondx = 0;
                int targetregiondy = 0;
                int seed = noise4d(xregion, yregion, 2004, WORLD_SEED);
                std::mt19937_64 gen;
                gen.seed(seed);
                // every 10th warp gets a more distant destination
                if (gen() % 10 == 0) {
                    while (targetregiondx == 0 && targetregiondy == 0) {
                        targetregiondx = gen() % 15 - 7;
                        targetregiondy = gen() % 15 - 7;
                    }
                }
                // the rest of the warps can only choose between 3 destinations
                else {
                    int d = gen() % 3 - 1;
                    bool x_even = xregion % 2 == 0;
                    bool y_even = yregion % 2 == 0;
                    // split warps in 4 groups in a checker pattern
                    if (x_even && y_even) {
                        // this group is biased right
                        targetregiondx = 1;
                        targetregiondy = d;
                    }
                    if (!x_even && y_even) {
                        // this group is biased downwards
                        targetregiondx = d;
                        targetregiondy = -1;
                    }
                    if (!x_even && !y_even) {
                        // this group is biased left
                        targetregiondx = -1;
                        targetregiondy = d;
                    }
                    if (x_even && !y_even) {
                        // this group is biased upwards
                        targetregiondx = d;
                        targetregiondy = 1;
                    }
                }
                int targetregionx = xregion + targetregiondx;
                int targetregiony = yregion + targetregiondy;
                // find warp in that region
                auto target_warp = find_warp_position(
                        targetregionx, targetregiony, perlin2, WORLD_SEED);
                // unpack coordinates
                int warp_target_x = target_warp.first;
                int warp_target_y = target_warp.second;
                // store this information in tile entity
                tileEntities.push_back(new Warp(
                            global_warp_x,
                            global_warp_y,
                            warp_target_x,
                            warp_target_y));
            }
        }
    }



    // crashed ships / shipwrecks
    if (X%2==0 && Y%2==0) {
        int seed = noise4d(X, Y, 2108, WORLD_SEED)
            + 1000*noise4d(X, Y, 2110, WORLD_SEED);
        std::mt19937_64 gen;
        gen.seed(seed);
        // pick a random position inside this chunk
        int shipx = 128 + 80 + gen() % (256-160);
        int shipy = 128 + 80 + gen() % (256-160);
        bool foundposition = false;
        for (int i = 0; i < SHIP_ATTEMPTS; i++) {
            while (isGround((*blocks)[shipy][shipx])
                    && shipy > 128+60) {
                shipy--;
            }
            while (isGround((*blocks)[shipy+1][shipx]) && shipy < 128+256-60) {
                shipy++;
            }

//  -22 -20       -15       -10       -5         0         5        +10       +15       +20 +22
//18                               x                           x
//17                                             x
//16 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ A A _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//15 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H H H A _ _ _ _ _ _ A A _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//14x_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H _ _ A H _ _ _ _ _ H H H A _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ x
//13 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ x _ H _ _ _ H _ x A H _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ H
//12 _ _ _ _ _ _ _ _ _ _ _ x _ _ _ _ _ _ V _ H _ _ _ V _ _ _ A _ _ _ x _ _ _ _ _ _ _ _ _ H H _
//11 _ _ _ _ _ x _ _ _ _ _ H _ H _ _ _ _ _ _ H A _ _ V _ _ _ H _ _ _ _ _ _ _ _ _ _ x _ H H _ _
//10 _ _ _ _ _ _ _ _ _ _ _ _ H _ _ _ _ _ _ _ A H _ _ V _ _ H _ _ _ _ _ _ _ A _ _ _ _ _ H H _ _
//9  _ _ _ _ _ _ _ _ _ _ _ A H _ _ _ _ _ _ _ _ h x _ _ _ _ H _ _ _ _ _ A _ _ A A _ _ h H H _ _
//8  _ _ _ _ _ _ _ _ _ _ _ _ A _ _ _ x _ _ _ _ h _ _ _ _ _ _ x _ _ _ _ H A A H H H H H h h _ _
//7  x _ _ _ _ _ _ _ _ _ _ _ h _ _ _ _ _ _ _ _ _ h _ _ _ _ _ H H H H H A V a a i a a H h _ _ x
//6  _ _ _ _ _ _ _ _ _ _ _ _ _ h A _ _ A _ _ _ _ h H H H H H H H a a a a V a a i a a a h _ _ _
//5  H H H h h H H x h _ _ _ _ h A A A _ _ _ H H H H a i a a a a a i a a V a x i a a A H _ _ _
//4  V _ H a a V a i h H H H H A H h H h h H H a i a a i a a a a a i a a V a a i a a H A _ _ _
//3  V _ _ h a i a i a V a a a a a a a a a a a a i a a a h a a a a i a a V a a i a a H A _ _ _
//2  _ _ _ h a i a i a V a a a h a x i a a h a a i a a h a h a x a i a a a a a i A A h _ A _ _
//1  _ _ A H h H A a a V a a h a h a i a h a h a x a a a h a b a a i _ a _ H a a H H h _ _ _ _
//0  _ _ _ A A A H A a V a a a h a a i a a h a a g a a a a b b b a i _ _ _ H h h H _ _ _ _ _ _
//-1 _ _ _ _ _ A _ H h h H a a a a c i a a a i a a a a a b b b b _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
//-2 _ _ _ _ _ _ _ _ _ _ H H h h H g h H H H h H H _ H H b b b g b _ _ _ _ _ _ _ _ _ _ _ _ _ _
//-3 _ _ _ _ _ _ _ _ _ _ _ _ g _ _ _ _ _ _ _ _ _ _ _ _ _ _ b b b b _ g _ _ _ _ _ _ _ _ _ _ _ _
//-4                                       g           g
//-5
//-6
//-7
//-8                                 g                       g
//-9
//-10
//-11                                            g
//-12
//-13
//-14
//-15                                    g               g
//-16   -20       -15       -10       -5         0         5        +10       +15       +20

            Biome biome = (*biome_map)[shipy/8][shipx/4];
            Block here = (*blocks)[shipy][shipx];
            if (( (biome == Biome::appleforest && here == Block::d)
                    || (biome == Biome::apricotforest && here == Block::d)
                    || (biome == Biome::mixedforest && here == Block::d)
                    || (biome == Biome::greenmountain && here == Block::d)
                    || (biome == Biome::desert && here == Block::sand)
                    || (biome == Biome::oasis && here == Block::sand)
                    || (biome == Biome::glacier && isGround(here))
                    || (biome == Biome::flower && isGround(here))
                ) && (
                    (isGround((*blocks)[shipy-2][shipx+7])
                     && isGround((*blocks)[shipy-2][shipx-7])
                     && isGround((*blocks)[shipy-4][shipx+3])
                     && isGround((*blocks)[shipy-4][shipx-3])
                     && isAir((*blocks)[shipy+2][shipx+7])
                     && isAir((*blocks)[shipy+2][shipx-7])
                     && isAir((*blocks)[shipy+5][shipx+14])
                     && isAir((*blocks)[shipy+5][shipx-14])
                    ) || biome == Biome::glacier
                    )
                    && isGround((*blocks)[shipy-3][shipx+10])
                    && isGround((*blocks)[shipy-3][shipx-10])
                    && isGround((*blocks)[shipy-8][shipx+6])
                    && isGround((*blocks)[shipy-8][shipx-6])
                    && isGround((*blocks)[shipy-11][shipx+0])
                    && isGround((*blocks)[shipy-15][shipx+4])
                    && isGround((*blocks)[shipy-15][shipx-4])
                    && !isGround((*blocks)[shipy+1][shipx])
                    && isAir((*blocks)[shipy+7][shipx+22])
                    && isAir((*blocks)[shipy+7][shipx-22])
                    && isAir((*blocks)[shipy+8][shipx+6])
                    && isAir((*blocks)[shipy+8][shipx-6])
                    && isAir((*blocks)[shipy+9][shipx])
                    && isAir((*blocks)[shipy+11][shipx+17])
                    && isAir((*blocks)[shipy+11][shipx-17])
                    && isAir((*blocks)[shipy+12][shipx+10])
                    && isAir((*blocks)[shipy+12][shipx-10])
                    && isAir((*blocks)[shipy+13][shipx+4])
                    && isAir((*blocks)[shipy+13][shipx-4])
                    && isAir((*blocks)[shipy+14][shipx+23])
                    && isAir((*blocks)[shipy+14][shipx-23])
                    && isAir((*blocks)[shipy+17][shipx])
                    && isAir((*blocks)[shipy+18][shipx+7])
                    && isAir((*blocks)[shipy+18][shipx-7])
                    ) {
                foundposition = true;
                break;
            }
            shipx = 128 + 80 + gen() % (256-160);
            shipy = 128 + 80 + gen() % (256-160);
        }
        if (foundposition) {
            // choose ship structure
            auto shipmap = SHIP_FOREST;
            Biome biome = (*biome_map)[shipy/8][shipx/4];
            if (biome == Biome::desert) shipmap = SHIP_DESERT;
            if (biome == Biome::oasis) shipmap = SHIP_DESERT;
            if (biome == Biome::glacier) shipmap = SHIP_FROZEN;
            // decide structure orientation
            int orientation = gen() % 8;
            // flip horizontally
            bool flipx = (orientation & 1) > 0;
            // place the structure
            int height = shipmap.size();
            int width = shipmap[0].size();
            for (int ddy = 0; ddy < height; ddy++) {
                for (int ddx = 0; ddx < width; ddx++) {
                    // modify coordinates for specific orientation
                    int ddxmod = ddx;
                    int ddymod = ddy;
                    if (flipx) ddxmod = width-ddxmod-1;
                    // use modified coordinates for sampling
                    Block newblock = shipmap[ddymod][ddxmod];
                    if (newblock == Block::VOID) continue;
                    int localx = shipx - width/2 + ddx;
                    int localy = shipy + height - 1 - ddy - SHIP_OFFSET_Y;
                    if (newblock == Block::transform_into_b) {
                        // transform non-air into b
                        if (!isAir((*blocks)[localy][localx])) {
                            // transform into b
                            (*blocks)[localy][localx] = Block::b;
                        }
                    } else {
                        (*blocks)[localy][localx] = newblock;
                    }
                    // create entities if needed:
                    if (newblock == Block::chest) {
                        int seed = noise4d(X, Y, 2112, WORLD_SEED)
                            + 1000*noise4d(X, Y, 2114, WORLD_SEED);
                        int worldx = (X*256-128)+localx;
                        int worldy = (Y*256-128)+localy;
                        tileEntities.push_back(new ShipChest(
                                    worldx, worldy, seed));
                    }
                }
            }
        }
    }


    // stalactites and stalagmites
    for (int i = 5; i < 507; ++i) {
        for (int j = 1; j < 511; ++j) {
            Block here = (*blocks)[i][j];
            Block below = (*blocks)[i-1][j];
            Block above = (*blocks)[i+1][j];
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            // check if this is an ok position for stalactite
            if (here == Block::cave
                    && above == Block::B
                    && below == Block::cave) {
                // roulette for stalactite length
                double value = noise4df(x, y, 204, WORLD_SEED);
                value = value * value;
                for (int d = 0; (d+2)*STALACTITE_COST < value
                        && d < STALACTITE_MAX; d++) {
                    if ((*blocks)[i-d][j] == Block::cave) {
                        (*blocks)[i-d][j] = Block::WT;
                    }
                    else {
                        break;
                    }
                }
            }
            here = (*blocks)[i][j];
            // check if this is an ok position for icicle
            if (isAir(here)
                    && above == Block::B
                    && isAir(below)
                    && biome_here.icicle) {
                // chance of icicle
                double value = noise4df(x, y, 205, WORLD_SEED);
                for (int d = 0; (d+2)*ICICLE_COST < value
                        && d < ICICLE_MAX; d++) {
                    Block here = (*blocks)[i-d][j];
                    if (isAir(here) && here != Block::keepclear) {
                        (*blocks)[i-d][j] = Block::icicle;
                    }
                    else {
                        break;
                    }
                }
            }
            // check if this is an ok position for stalagmite
            if (here == Block::cave
                    && below == Block::B
                    && above == Block::cave) {
                int x = (X*256-128)+j;
                int y = (Y*256-128)+i;
                // roulette for stalagmite length
                double value = noise4df(x, y, 204, WORLD_SEED);
                value = value * value;
                for (int d = 0; (d+2)*STALAGMITE_COST < value
                        && d < STALAGMITE_MAX; d++) {
                    if ((*blocks)[i+d][j] == Block::cave) {
                        (*blocks)[i+d][j] = Block::WI;
                    }
                    else {
                        break;
                    }
                }
            }
        }
    }


    // treeify
    // find tree spots
    std::vector<Tree> trees;

    for (int i = 58; i < 430; ++i) {
        // in this range trees could still affect what's inside the chunk.
        for (int j = 58; j < 454; ++j) {
            // is it an okay spot?
            if(
                    ((*blocks)[i][j] == Block::air
                     || isGrassf((*blocks)[i][j]))
                    &&(!isAir((*blocks)[i-1][j])
                        && !isGrassf((*blocks)[i-1][j]))
                    &&((*blocks)[i+1][j] == Block::air
                        || isGrassf((*blocks)[i+1][j]))
                    &&((*blocks)[i+2][j] == Block::air
                        || isGrassf((*blocks)[i+2][j]))
              ) {
                // is an okay spot.
                // or at least worth considering.
                int x = (X*256-128)+j;
                int y = (Y*256-128)+i;
                // roulette for tree:
                int roulette = noise4d(x, y, 206, WORLD_SEED);

                bool yes_tree = false;
                TreeType type;
                for (size_t f = 0; f < biome_here.tree_bounds.size(); f++) {
                    if (roulette < biome_here.tree_bounds[f]) {
                        type = (TreeType)f;
                        yes_tree = true;
                        break;
                    }
                }

                if (!yes_tree) {
                    continue;
                }

                // add to tree list:
                trees.push_back(Tree {
                        .i = i,
                        .j = j,
                        .priority = noise4d(x, y, 404, WORLD_SEED),
                        .type = type,
                        });

            }
        }
    }

    // order trees by priority score
    std::sort(trees.begin(), trees.end(),
            [](const Tree &a, const Tree &b) -> bool
            {
            return a.priority > b.priority;
            });

    Treegenerator treegenerator;

    // treeify
    // attempt placing trees in this order
    // (they can block each other, that's why order is important):
    for(auto tree : trees){
        int x = (X*256-128)+tree.j;
        int y = (Y*256-128)+tree.i;

        // check if ground is ok
        Block below = (*blocks)[tree.i-1][tree.j];
        if (below != tree_props.at(tree.type).grows_on
                && below != tree_props.at(tree.type).optional_grows_on) {
            continue;
        }

        for (int attempt = 0; attempt < TREE_ATTEMPTS; ++attempt) {
            uint64_t seed_x = x;
            uint64_t seed_y = y;
            uint64_t seed_w = WORLD_SEED;
            uint64_t seed_a = attempt;

            uint64_t seed = seed_w;
            seed += seed_x << 20;
            seed += seed_y << 40;
            seed += seed_a << 60;

            std::vector<std::vector<Block>> *treemap =
                treegenerator.generate(tree.type, seed);

            // can the generated tree be placed here?
            bool yes_can_place_here = true;
            for (int i = 0; i < 39 && yes_can_place_here; ++i) {
                for (int j = 0; j < 38; ++j) {
                    // is part of the tree at this spot?
                    // (wannabetentacle doesn't count)
                    if((*treemap)[i][j] != Block::VOID
                            && (*treemap)[i][j] != Block::wannabetentacle){
                        // is part of the world at this spot?
                        // (grass doesn't count, can be overwritten)
                        Block world_block = (*blocks)[tree.i+i-1][tree.j+j-18];
                        if(!isAir(world_block)
                                && !isGrassf(world_block)
                                // another condition, but very specific
                                // allows leaves of same type
                                // to overlap anyway
                                && !(isLeaf((*treemap)[i][j])
                                        && (*treemap)[i][j] == world_block
                                        && tree_props.at(tree.type)
                                        .allow_overlap)){
                            // oopsie, can't place
                            // because tree is overlapping with the world!
                            yes_can_place_here = false;
                            break;
                        }
                    }
                }
            }

            if(yes_can_place_here){
                // yay actually place the tree
                (*blocks)[tree.i-1][tree.j] =
                    tree_props.at(tree.type).grows_on;
                for (int i = 0; i < 39; ++i) {
                    for (int j = 0; j < 38; ++j) {
                        // is part of the tree at this spot?
                        if((*treemap)[i][j] != Block::VOID
                                && (*treemap)[i][j] != Block::air) {
                            // yes!
                            int wi = tree.i+i-1;
                            int wj = tree.j+j-18;
                            if ((*treemap)[i][j] == Block::wannabetentacle) {
                                // oh wait wannabetentacle replaces only
                                // air, grass and water
                                if (!isAir((*blocks)[wi][wj])
                                        && !isGrassf((*blocks)[wi][wj])
                                        && !isWater((*blocks)[wi][wj])) {
                                    continue;
                                }
                            }
                            // place it
                            (*blocks)[wi][wj] = (*treemap)[i][j];
                        }
                    }
                }
                break;
            }

        }
    }


    // bushify
    for (int i = 58; i < 430; ++i) {
        for (int j = 58; j < 454; ++j) {
            // is it an okay spot for a bush?
            if ((*blocks)[i][j] == Block::air
                    || isGrassf((*blocks)[i][j])
                    || isLeaf((*blocks)[i][j])) {
                // is a potential bush spot

                double bush_chance;
                const std::vector<int> *bush_collection;
                if ((*blocks)[i-1][j] == Block::d
                        || (*blocks)[i-1][j] == Block::B
                        || (*blocks)[i-1][j] == Block::sand) {
                    // bush can be placed on ground
                    bush_chance = biome_here.bush_chance;
                    bush_collection = &GROUND_BUSHES;
                } else if ( (*blocks)[i+1][j] == Block::B
                        && ((*blocks)[i][j+1] == Block::B
                            || (*blocks)[i][j-1] == Block::B)) {
                    // bush can be placed on ceiling
                    bush_chance = biome_here.ceiling_bush_chance;
                    bush_collection = &CEILING_BUSHES;
                } else {
                    // no ground or ceiling to place bush on
                    continue;
                }

                int x = (X*256-128)+j;
                int y = (Y*256-128)+i;
                // roll for bush:
                double generated = noise4df(x, y, 406, WORLD_SEED);
                if (generated > bush_chance) continue;

                // choose bush template:
                int seed = noise4d(x, y, 303, WORLD_SEED);
                int chosen_ind = seed % bush_collection->size();
                int chosen = bush_collection->at(chosen_ind);

                auto bushmap = BUSH_STRUCTURES[chosen];

                // place the structure
                for (size_t dy = 0; dy < bushmap.size(); dy++) {
                    for (size_t dx = 0; dx < bushmap[0].size(); dx++) {
                        Block newblock = bushmap[dy][dx];
                        if (newblock == Block::VOID) continue;
                        int worldx = j - bushmap[0].size()/2 + dx;
                        int worldy = i + bushmap.size()/2 - dy;
                        if (newblock == Block::Aa) {
                            newblock = biome_here.bush_leaf;
                            if (worldx % 2 == 1 && worldy % 2 == 1
                                    && noise4df(worldx, worldy,
                                        407, WORLD_SEED)
                                    < biome_here.bush_berry_chance) {
                                newblock = biome_here.bush_berry;
                            }
                        }
                        Block there = (*blocks)[worldy][worldx];
                        if (isGrass(there)
                                || there == Block::fakegrass
                                || isAir(there)) {
                            (*blocks)[worldy][worldx] = newblock;
                        }
                    }
                }

                // place wood in middle
                (*blocks)[i][j] = biome_here.bush_wood;

            }
        }
    }


    { // treasure
        // what is current treasure region?
        int xregion = X<0 ? (X-3)/4 : X/4;
        int yregion = Y<0 ? (Y-3)/4 : Y/4;
        // subregion?
        int xsubregion = (X % 4 + 4) % 4;
        int ysubregion = (Y % 4 + 4) % 4;
        // seed for region?
        int seed = noise4d(xregion, yregion, 2012, WORLD_SEED)
            + 1000*noise4d(xregion, yregion, 2014, WORLD_SEED);
        std::mt19937_64 gen;
        gen.seed(seed);
        // choose chunk in this region
        int dx = gen() % 3;
        int dy = gen() % 3;
        infos.push_back(Info("treasure_chunk", X + dx - xsubregion, Y + dy - ysubregion));
        // did we choose the current chunk?
        if (dx == xsubregion && dy == ysubregion) {
            // we did! treasure spawns in this chunk.
            // try to find a spot for the chest, 10000 times.
            int treasure_sub_x;
            int treasure_sub_y;
            bool success = false;
            for (int i = 0; i < 10000; i++) {
                // x is always odd (to hide on the map)
                treasure_sub_x = 2 * (gen() % 128) + 1;
                treasure_sub_y = gen() % 256;
                Block here = (*blocks)[128+treasure_sub_y][128+treasure_sub_x];
                Block below = (*blocks)[127+treasure_sub_y][128+treasure_sub_x];
                if (( isAir(here)
                            || here == Block::water
                            || isGrass(here))
                        && ( below == Block::B
                            || below == Block::sand
                            || below == Block::d
                            || below == Block::b
                            || isLeaf(below)
                            || isWood(below)
                           )) {
                    success = true;
                    break;
                }
            }
            if (success) {
                // global coordinates
                int global_treasure_x = X*256 + treasure_sub_x;
                int global_treasure_y = Y*256 + treasure_sub_y;
                infos.push_back(Info("treasure", global_treasure_x, global_treasure_y));
                // place the chest block
                Block here = (*blocks)[128+treasure_sub_y][128+treasure_sub_x];
                (*blocks)[128+treasure_sub_y][128+treasure_sub_x] = Block::goldchest;
                // create tile entity (chest content)
                if (here == Block::water) {
                    // ocean chest
                    std::vector<double> coral_biome = CORAL_RARITY;
                    for (int k = 0; k < 9; ++k) {
                        int x = global_treasure_x;
                        int y = global_treasure_y;
                        coral_biome[k] = perlin2->GetValue(
                                x*CORAL_BIOME_FREQUENCY,
                                y*CORAL_BIOME_FREQUENCY, 400+k);
                    }
                    tileEntities.push_back(new WaterChest(
                                global_treasure_x,
                                global_treasure_y,
                                seed,
                                coral_biome,
                                CORAL_RARITY));
                }
                else if (here == Block::cave) {
                    tileEntities.push_back(new CaveChest(
                                global_treasure_x,
                                global_treasure_y,
                                seed));
                }
                else {
                    tileEntities.push_back(new TreeChest(
                                global_treasure_x,
                                global_treasure_y,
                                seed));
                }
                if (gen()%100 < 50) {
                    // attempt to spawn extra chest
                    int dx = gen() % 5 - 2;
                    int dy = gen() % 3 - 3;
                    Block here = (*blocks)[128+treasure_sub_y+dy][128+treasure_sub_x+dx];
                    if (here == Block::B
                            || here == Block::d
                            || here == Block::sand
                            || isLeaf(here)
                            || isWood(here)
                            || here == Block::DD) {
                        tileEntities.push_back(new LootChest(
                                    global_treasure_x+dx,
                                    global_treasure_y+dy,
                                    seed));
                    }
                }
            }
        }
    }
    if (X - 1 > 0 && Y - 1 > 0 && 5 - X > Y) {
        for (int k = 0; k < 28; k++) {
            if ((extra_shapes&1<<k)) {
                (*blocks)[k/4+274][k%4+284] = Block::R;
                (*blocks)[k/4+274][k%4+278] = Block::R;
                (*blocks)[k/4+274][k%4+272] = Block::R;
            }
        }
    }
    { // boring normal treasure
        // seed for treasure in this chunk
        int seed = noise4d(X, Y, 2008, WORLD_SEED)
            + 1000*noise4d(X, Y, 2010, WORLD_SEED);
        std::mt19937_64 gen;
        gen.seed(seed);
        // try to find a spot for the chest, 10000 times.
        int treasure_sub_x;
        int treasure_sub_y;
        bool success = false;
        for (int i = 0; i < 10000; i++) {
            // x is always odd (to hide on the map)
            treasure_sub_x = 2 * (gen() % 128) + 1;
            treasure_sub_y = gen() % 256;
            Block here = (*blocks)[128+treasure_sub_y][128+treasure_sub_x];
            Block below = (*blocks)[127+treasure_sub_y][128+treasure_sub_x];
            if ( here == Block::cave
                    && below == Block::B) {
                success = true;
                break;
            }
        }
        if (success) {
            // global coordinates
            int global_bad_treasure_x = X*256 + treasure_sub_x;
            int global_bad_treasure_y = Y*256 + treasure_sub_y;
            infos.push_back(Info("badtreasure", global_bad_treasure_x, global_bad_treasure_y));
            // place the chest block
            (*blocks)[128+treasure_sub_y][128+treasure_sub_x] = Block::chest;
            // create tile entity (chest content)
            tileEntities.push_back(new NormalCaveChest(
                        global_bad_treasure_x,
                        global_bad_treasure_y,
                        seed));
        }
    }


    // pebblify. what a word
    for (int i = 50; i < 462; ++i) {
        for (int j = 50; j < 462; ++j) {
            // might be a space for pebble
            double pebble_chance;
            if ((*blocks)[i][j] == Block::fakegrass
                    // allow pebble to spawn inside flowing grass
                    || (isGrass((*blocks)[i][j])
                        && (*blocks)[i-1][j] == Block::d
                        && (*blocks)[i-1][j-1] == Block::d
                        && (*blocks)[i-1][j+1] == Block::d
                        && biome_here.flowing_grass)
                    ) {
                pebble_chance = biome_here.pebble_chance;
            }
            else if ((*blocks)[i][j] == Block::cave
                    && (*blocks)[i-1][j] == Block::B
                    && (*blocks)[i-1][j-1] == Block::B
                    && (*blocks)[i-1][j+1] == Block::B
               ) {
                pebble_chance = PEBBLE_CHANCE_CAVE;
            }
            else {
                // no chance
                continue;
            }

            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            // roll for pebble
            double generated = noise4df(x, y, 300, WORLD_SEED);
            if (generated < pebble_chance) {
                (*blocks)[i][j] = Block::pebble;
            }
        }
    }


    // snowify icify iceify
    for (int i = 387; i > 124; --i) {
        // this one only goes to chunk edge
        // because we really don't need snow outside
        for (int j = 124; j < 387; ++j) {
            // check if this is an ok position for snow
            Block here = (*blocks)[i][j];
            Block below = (*blocks)[i-1][j];
            if (!isAirOrFakegrass(here)
                    || isGrassf(below)
                    || below == Block::pebble
                    || below == Block::WI
                    || below == Block::quicksand
                    || below == Block::stoneslab
                    || below == Block::snow
                    || below == Block::stick
                    || isAir(below)
               ) continue;
            if (isWater(below)) {
                if (biome_here.ice) {
                    (*blocks)[i][j] = Block::ice;
                }
                continue;
            }
            if (biome_here.snow) {
                if (biome_here.snowice) {
                    (*blocks)[i][j] = Block::ice;
                } else {
                    (*blocks)[i][j] = Block::snow;
                }
            }
        }
    }


    // flowerify
    for (int i = 124; i < 387; ++i) {
        // this one only goes to chunk edge
        // because we really don't need flowers outside
        for (int j = 124; j < 387; ++j) {
            // check if this is an ok position for flower
            Block here = (*blocks)[i][j];
            Block below = (*blocks)[i-1][j];
            Block above = (*blocks)[i+1][j];
            if (below == Block::d
                    && (isGrass(here)
                        || isAir(here))
                    && (isGrass(above)
                        || isAir(above))) {
                int x = (X*256-128)+j;
                int y = (Y*256-128)+i;
                // roulette for flower
                int chosenFlower = noise4d(x, y, 204, WORLD_SEED);
                for (size_t f = 0; f < biome_here.flower_bounds.size(); f++) {
                    if (chosenFlower < biome_here.flower_bounds[f]) {
                        (*blocks)[i][j] = Block::flowerstem;
                        (*blocks)[i+1][j] = (Block)((int)Block::flower0 + f);
                        break;
                    }
                }
            }
        }
    }


    // waterfallify
    for (int i = 100; i < 400; i+=16) {
        for (int j = 100; j < 400; j+=16) {
            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            int dxymod = noise4d(x, y, 4302, WORLD_SEED)%100;
            int dx = dxymod/10;
            int dy = dxymod%10;
            // waterfall only spawns in stone
            if ((*blocks)[i+dy][j+dx] != Block::B){
                continue;
            }
            Block left = (*blocks)[i+dy][j+dx-1];
            Block right = (*blocks)[i+dy][j+dx+1];
            Block below = (*blocks)[i+dy-1][j+dx];
            int num_stone =
                (left == Block::B)
                + (right == Block::B)
                + (below == Block::B);
            int num_air =
                isAir(left)
                + isAir(right)
                + isAir(below);
            // needs to be 2 stone
            if (num_stone != 2) continue;
            // needs to be 1 air
            if (num_air != 1) continue;
            // needs to be proper biome
            if (!biome_here.has_waterfall) continue;
            // ok place waterfall
            (*blocks)[i+dy][j+dx] = Block::water;
            WaterUpdate *w = new WaterUpdate();
            w->x = x+dx;
            w->y = y+dy;
            waterUpdates.push_back(w);
        }
    }


    { // desert treasure
        // seed for treasure in this chunk
        int seed = noise4d(X, Y, 2016, WORLD_SEED)
            + 1000*noise4d(X, Y, 2018, WORLD_SEED);
        std::mt19937_64 gen;
        gen.seed(seed);
        // try to find a spot for the chest, 10000 times.
        int treasure_sub_x;
        int treasure_sub_y;
        bool success = false;
        for (int i = 0; i < 10000; i++) {
            // x is always odd (to hide on the map)
            treasure_sub_x = 2 * (gen() % 128) + 1;
            treasure_sub_y = gen() % 256;
            int local_x = treasure_sub_x + 128;
            int local_y = treasure_sub_y + 128;
            // check if it is properly underground
            if (isGrassf((*blocks)[local_y+3][local_x])
                    && isGround((*blocks)[local_y+2][local_x])) {
                // after meeting the two conditions above, this
                // will be the last attempt. So if this is not
                // desert, there will be no chest. No further attempts
                if ((*biome_map)[local_y/8][local_x/4] == Biome::desert
                        && (*blocks)[local_y+1][local_x] == Block::sand
                        && (*blocks)[local_y][local_x] == Block::sand
                        && isGround((*blocks)[local_y-1][local_x])) {
                    success = true;
                }
                break;
            }
        }
        if (success) {
            // global coordinates
            int worldx = X*256 + treasure_sub_x;
            int worldy = Y*256 + treasure_sub_y;
            // place the chest block
            (*blocks)[128+treasure_sub_y][128+treasure_sub_x] = Block::chest;
            // create tile entity (chest content)
            tileEntities.push_back(new DesertTreasureChest(
                        worldx, worldy, seed));
        }
    }

    // decoration
    if (isAir((*blocks)[177][214])
            && isAir((*blocks)[177][298])
            && isAir((*blocks)[203][224])
            && isAir((*blocks)[203][288])
            && isAir((*blocks)[268][256])
            && (*blocks)[164][356] == Block::DD
       ){
        for (int y = 49; y < 140; y++) {
            for (int x = 60; x < 129; x++) {
                if (abs(21*y-42*x+2471) < 504
                        || (x > 96 && y > 75 && y < 84)) {
                    (*blocks)[128+y][128+x] = Block::Aa;
                    (*blocks)[128+y][384-x] = Block::Aa;
                }
            }
        }
    }


    // clouds
    for (int i = 0; i < 64; ++i) {
        for (int j = 0; j < 128; ++j) {
            if ((*biome_map)[i][j] != Biome::sky
                    && (*biome_map)[i][j] != Biome::rainbow) {
                // cloud is only in sky and rainbow biome
                continue;
            }
            // world coordinates (needed for deterministicness)
            int x = (X*256-128)+4*j;
            int y = (Y*256-128)+8*i;
            // randomize with perlin:
            double val = (perlin2->GetValue(x*CLOUD_NOISE_FREQUENCY_X,
                        y*CLOUD_NOISE_FREQUENCY_Y, 3008));
            if (val > 0.5){
                for (int di = 0; di < 8; di++) {
                    for (int dj = 0; dj < 4; dj++) {
                         // roll for cloud:
                        double generated = noise4df(x+dj, y+di, 3010, WORLD_SEED);
                        if(generated < 0.5){
                            (*blocks)[8*i+di][4*j+dj] = Block::cloud;
                        }
                    }
                }
            }
        }
    }

    // cloud cellular automata
    std::array<std::array<Block,512>,512> *blocks2 =
        new std::array<std::array<Block,512>,512>();

    for (int it = 0; it < 10; it++) {
        for (int i = 2; i < 510; ++i) {
            for (int j = 10; j < 501; ++j) {
                if ((*biome_map)[i/8][j/4] != Biome::sky
                        && (*biome_map)[i/8][j/4] != Biome::rainbow) {
                    // cloud is only in sky and rainbow biome
                    continue;
                }
                /* int sum = 0; */
                /* for (int di = -1; di <= 1; di++) { */
                    /* for (int dj = -5; dj <= 5; dj++) { */
                        /* std::cout << "+ ((*blocks)[i+"<<di<<"][j+"<<dj<<"] == Block::cloud)" << std::endl; */
                        /* sum += ((*blocks)[i+di][j+dj] == Block::cloud); */
                    /* } */
                /* } */

                // unrolled loop, and slightly modified into checker grid:
                int sum

                    = ((*blocks)[i-1][j-9] == Block::cloud)
                    + ((*blocks)[i-1][j-7] == Block::cloud)
                    + ((*blocks)[i-1][j-5] == Block::cloud)
                    + ((*blocks)[i-1][j-3] == Block::cloud)
                    + ((*blocks)[i-1][j-1] == Block::cloud)
                    + ((*blocks)[i-1][j+0] == Block::cloud)
                    + ((*blocks)[i-1][j+1] == Block::cloud)
                    + ((*blocks)[i-1][j+3] == Block::cloud)
                    + ((*blocks)[i-1][j+5] == Block::cloud)
                    + ((*blocks)[i-1][j+7] == Block::cloud)
                    + ((*blocks)[i-1][j+9] == Block::cloud)
                    + ((*blocks)[i+0][j-8] == Block::cloud)
                    + ((*blocks)[i+0][j-6] == Block::cloud)
                    + ((*blocks)[i+0][j-4] == Block::cloud)
                    + ((*blocks)[i+0][j-2] == Block::cloud)
                    + ((*blocks)[i+0][j-1] == Block::cloud)
                    + ((*blocks)[i+0][j+1] == Block::cloud)
                    + ((*blocks)[i+0][j+2] == Block::cloud)
                    + ((*blocks)[i+0][j+4] == Block::cloud)
                    + ((*blocks)[i+0][j+6] == Block::cloud)
                    + ((*blocks)[i+0][j+8] == Block::cloud)
                    + ((*blocks)[i+1][j-9] == Block::cloud)
                    + ((*blocks)[i+1][j-7] == Block::cloud)
                    + ((*blocks)[i+1][j-5] == Block::cloud)
                    + ((*blocks)[i+1][j-3] == Block::cloud)
                    + ((*blocks)[i+1][j-1] == Block::cloud)
                    + ((*blocks)[i+1][j+0] == Block::cloud)
                    + ((*blocks)[i+1][j+1] == Block::cloud)
                    + ((*blocks)[i+1][j+3] == Block::cloud)
                    + ((*blocks)[i+1][j+5] == Block::cloud)
                    + ((*blocks)[i+1][j+7] == Block::cloud)
                    + ((*blocks)[i+1][j+9] == Block::cloud);

                if ((*blocks)[i][j] == Block::cloud) {
                    if (sum < 14) {
                        (*blocks2)[i][j] = Block::air;
                    }
                    else {
                        (*blocks2)[i][j] = Block::cloud;
                    }
                }
                else if ((*blocks)[i][j] == Block::air) {
                    if (sum < 19) {
                        (*blocks2)[i][j] = Block::air;
                    }
                    else {
                        (*blocks2)[i][j] = Block::cloud;
                    }
                }
                else {
                    // this covers all weird blocks that might appear
                    // such as trees, hanging tentacles ...
                    (*blocks2)[i][j] = (*blocks)[i][j];
                }
            }
        }
        std::array<std::array<Block,512>,512> *temp = blocks;
        blocks = blocks2;
        blocks2 = temp;
    }
    delete blocks2;

    // rainbow waterfalls. rainbowify
    for (int i = 96; i < 416; i+=8) {
        for (int j = 96; j < 416; j+=8) {
            // need global coordinates
            // for deterministicness
            // and for creating water flow events
            int x = (X*256-128)+j;
            int y = (Y*256-128)+i;
            // check if biome is ok for rainbow waterfall
            bool ok = true;
            for (int di = 0; di < 4; di++) {
                if ((*biome_map)[i/8-di][j/4] != Biome::rainbow) {
                    // no
                    ok = false;
                    break;
                }
                if ((*biome_map)[i/8-di][j/4+1] != Biome::rainbow) {
                    // no
                    ok = false;
                    break;
                }
            }
            if (!ok) continue;
            // check if space below
            for (int dx = 0; dx < 8; dx++) {
                if ((*blocks)[i-1][j+dx] != Block::air) {
                    ok = false;
                    break;
                }
            }
            if (!ok) continue;
            // check if we can spawn 8 water sources touching cloud
            for (int dx = 0; dx < 8; dx++) {
                bool found_cloud = false;
                for (int dy = 0; dy < 8; dy++) {
                    if ((*blocks)[i+dy][j+dx] == Block::cloud) {
                        found_cloud = true;
                        // but there also needs to be cloud above
                        if ((*blocks)[i+dy+1][j+dx] != Block::cloud) {
                            ok = false;
                        }
                        break;
                    }
                }
                if (!found_cloud) {
                    // there was no cloud in this column! Abort!
                    ok = false;
                    break;
                }
            }
            if (!ok) continue;
            // okay we can make rainbow water here!
            // roll for rainbow
            double generated = noise4df(x, y, 300, WORLD_SEED);
            if (generated > RAINBOW_CHANCE) {
                continue;
            }

            for (int dx = 1; dx < 7; dx++) {
                for (int dy = 0; dy < 8; dy++) {
                    if ((*blocks)[i+dy][j+dx] == Block::cloud) {
                        (*blocks)[i+dy][j+dx] = Block::water;
                        WaterUpdate *w = new WaterUpdate();
                        w->x = x+dx;
                        w->y = y+dy;
                        waterUpdates.push_back(w);
                        break;
                    }
                }
            }
        }
    }

    delete local_density;
    delete density_dy;
    delete local_channel;
    delete local_gap;
    delete original_biome_map;
    // return data using the pass-by-reference struct
    chunk_job->blocks = blocks;
    chunk_job->biome_map = biome_map;
    chunk_job->tileEntities = tileEntities;
    chunk_job->waterUpdates = waterUpdates;
    chunk_job->claims = claims;
    chunk_job->infos = infos;
}
