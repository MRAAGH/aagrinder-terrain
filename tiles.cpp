// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TILES_CPP
#define TILES_CPP

#include <stdio.h>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include <ostream>

struct TileEntity {
    int x, y;
    TileEntity(int x, int y)
        : x(x), y(y) {}
    virtual void print(std::ostream *out) = 0;
    virtual ~TileEntity() {};
};

struct Sign : TileEntity {
    std::string text;
    Sign(int x, int y, std::string text)
        : TileEntity(x, y), text(text) {}
    ~Sign() {}
    void print(std::ostream *out) {
        (*out) << "{\"t\":\"sign\",\"x\":" << x
            << ",\"y\":" << y
            << ",\"d\":{\"t\":\"" << text
            << "\"},\"s\":[]}";
    }
};

struct Warp : TileEntity {
    int targetx, targety;
    Warp(int x, int y, int targetx, int targety)
        : TileEntity(x, y), targetx(targetx), targety(targety) {}
    ~Warp() {}
    void print(std::ostream *out) {
        (*out) << "{\"t\":\"warp\",\"x\":" << x
            << ",\"y\":" << y
            << ",\"d\":{\"tx\":" << targetx
            << ",\"ty\":" << targety
            << "}}";
    }
};

struct Forg : TileEntity {
    Forg(int x, int y)
        : TileEntity(x, y) {}
    ~Forg() {}
    void print(std::ostream *out) {
        (*out) << "{\"t\":\"forg\",\"x\":" << x
            << ",\"y\":" << y
            << ",\"d\":{\"know\":\"\"}}";
    }
};

struct Slot {
    Slot() {}
    virtual ~Slot() {}
    virtual void print(std::ostream *out) {
        (*out) << "null";
    }
};

struct ItemSlot : Slot {
    std::string code;
    int amount;
    ItemSlot(std::string code, int amount)
        : code(code), amount(amount) {}
    void print(std::ostream *out) override {
        if (amount == 0) Slot::print(out);
        else (*out) << "{\"code\":\"" << code
            << "\",\"amount\":" << amount << "}";
    }
};

struct GrinderItemSlot : ItemSlot {
    int durability;
    GrinderItemSlot(std::string code, int amount, int durability)
        : ItemSlot(code, amount), durability(durability){}
    void print(std::ostream *out) override {
        (*out) << "{\"code\":\"" << code
            << "\",\"amount\":" << amount
            << ",\"data\":{\"d\":" << durability
            << "}}";
    }
};

struct Chest : TileEntity {
    std::vector<Slot*> slots;
    unsigned int nslots;
    Chest(int x, int y) : TileEntity(x, y) {}
    Chest(int x, int y, std::vector<Slot*> slots, unsigned int nslots)
        : TileEntity(x, y), slots(slots), nslots(nslots) {}
    ~Chest() {
        for (auto slot : slots) {
            delete slot;
        }
    }
    void print(std::ostream *out) {
        (*out) << "{\"t\":\"chest\",\"x\":" << x
            << ",\"y\":" << y
            << ",\"d\":[";
        bool comma = false;
        for (auto it : slots) {
            if (comma) (*out) << ',';
            comma = true;
            it->print(out);
        }
        (*out) << "]}";
    }
    void zerofill_and_shuffle(std::mt19937_64 &gen) {
        while (slots.size() < nslots) {
            // fill with null slots
            slots.push_back(new Slot());
        }
        std::shuffle(std::begin(slots), std::end(slots), gen);
    }
};

struct TreeChest : Chest {
    TreeChest(int x, int y, int seed) : Chest(x, y)
    {
        nslots = 20;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("B", 30+gen()%30));
        slots.push_back(new ItemSlot("Ha", 50+gen()%20));
        slots.push_back(new ItemSlot("Hb", 30+gen()%90));
        slots.push_back(new ItemSlot("Aa", 30+gen()%30));
        slots.push_back(new ItemSlot("Aa", 20+gen()%30));
        slots.push_back(new ItemSlot("Ab", 80+gen()%30));
        slots.push_back(new ItemSlot("-", gen()%40));
        int choice = gen()%4;
        if (choice == 0) {
            slots.push_back(new ItemSlot("*0", gen()%15));
            slots.push_back(new ItemSlot("fu", 10+gen()%70));
        } else if (choice == 1) {
            slots.push_back(new ItemSlot("*1", gen()%15));
            slots.push_back(new ItemSlot("fr", 10+gen()%70));
        } else if (choice == 2) {
            slots.push_back(new ItemSlot("*2", gen()%11));
            slots.push_back(new ItemSlot("fl", 10+gen()%70));
        } else {
            slots.push_back(new ItemSlot("*3", gen()%11));
            slots.push_back(new ItemSlot("fa", 10+gen()%70));
        }
        slots.push_back(new ItemSlot("R", 1+gen()%3));
        slots.push_back(new ItemSlot("R", gen()%3));
        zerofill_and_shuffle(gen);
    }
};

struct CaveChest : Chest {
    CaveChest(int x, int y, int seed) : Chest(x, y)
    {
        nslots = 20;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("#s", gen()%10));
        slots.push_back(new ItemSlot("DD", 1));
        slots.push_back(new ItemSlot("R", 1+gen()%2));
        if (gen()%2 == 0) {
            slots.push_back(new ItemSlot("B", 50+gen()%60));
            slots.push_back(new ItemSlot("WI", gen()%10));
            slots.push_back(new ItemSlot("R", 1+gen()%5));
        } else {
            slots.push_back(new ItemSlot("B", 30+gen()%30));
            slots.push_back(new ItemSlot("B", 20+gen()%30));
            slots.push_back(new ItemSlot("WI", gen()%20));
        }
        if (gen()%2 == 0) {
            slots.push_back(new ItemSlot("Dm", 6+gen()%50));
            slots.push_back(new ItemSlot("DD", 1+gen()%2));
            slots.push_back(new ItemSlot("Dg", 3+gen()%15));
        } else {
            slots.push_back(new ItemSlot("Dm", 3+gen()%30));
            slots.push_back(new ItemSlot("R", 1+gen()%11));
            slots.push_back(new ItemSlot("Dg", 6+gen()%45));
        }
        if (gen()%2 == 0) {
            slots.push_back(new ItemSlot("WB", 16+gen()%3));
            slots.push_back(new ItemSlot("DD", gen()%2));
        } else {
            slots.push_back(new ItemSlot("-", 20+gen()%20));
            slots.push_back(new ItemSlot("R", 1));
        }
        zerofill_and_shuffle(gen);
    }
};

struct LootChest : Chest {
    LootChest(int x, int y, int seed) : Chest(x, y)
    {
        nslots = 20;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("DD", gen()%2));
        slots.push_back(new ItemSlot("R", 2+gen()%4));
        slots.push_back(new ItemSlot("=", gen()%20));
        slots.push_back(new ItemSlot("#s", gen()%8));
        slots.push_back(new ItemSlot("Dm", gen()%20));
        slots.push_back(new ItemSlot("Dg", gen()%20));
        slots.push_back(new ItemSlot("..", gen()%20));
        slots.push_back(new ItemSlot("?", gen()%2));
        slots.push_back(new ItemSlot("hg", 1));
        zerofill_and_shuffle(gen);
    }
};

struct NormalCaveChest : Chest {
    NormalCaveChest(int x, int y, int seed) : Chest(x, y)
    {
        nslots = 10;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("..", 3+gen()%3));
        if (gen()%10 < 5) {
            slots.push_back(new ItemSlot("Dm", 4+gen()%3));
            slots.push_back(new ItemSlot("..", 9+gen()%3));
        } else {
            slots.push_back(new ItemSlot("Dg", 4+gen()%7));
            slots.push_back(new ItemSlot("B", 19+gen()%3));
        }
        slots.push_back(new ItemSlot("WI", 1+gen()%3));
        int choice = gen()%20;
        if (choice < 1) {
            slots.push_back(new ItemSlot("..", 23+gen()%10));
        } else if (choice < 6) {
            slots.push_back(new ItemSlot("..", 1+gen()%3));
            slots.push_back(new ItemSlot("..", 7+gen()%3));
        } else if (choice < 10) {
            slots.push_back(new ItemSlot("|/", 1+gen()%5));
        } else if (choice < 13) {
            slots.push_back(new GrinderItemSlot("GH", 1, 100+gen()%100));
        } else if (choice < 15) {
            slots.push_back(new ItemSlot("DD", 1));
            slots.push_back(new ItemSlot("DD", gen()%2));
        } else if (choice < 16) {
            slots.push_back(new ItemSlot("R", 1));
        } else if (choice < 18) {
            slots.push_back(new ItemSlot("WB", 1+gen()%2));
        } else {
        }
        zerofill_and_shuffle(gen);
    }
};

struct DesertTreasureChest : Chest {
    DesertTreasureChest(int x, int y, int seed) : Chest(x, y)
    {
        nslots = 10;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("S", 1+gen()%3));
        slots.push_back(new ItemSlot("S", 2+gen()%3));
        slots.push_back(new ItemSlot("S", 3+gen()%3));
        slots.push_back(new ItemSlot("S", 4+gen()%3));
        slots.push_back(new ItemSlot("S", 5+gen()%3));
        slots.push_back(new ItemSlot("|/", 1+gen()%5));
        slots.push_back(new ItemSlot("|\\\\", 1+gen()%5));
        int choice = gen()%10;
        if (choice < 4) {
            slots.push_back(new ItemSlot("DD", 1));
            slots.push_back(new ItemSlot("DD", gen()%2));
        } else if (choice < 8) {
            slots.push_back(new ItemSlot("R", 1+gen()%4));
            slots.push_back(new ItemSlot("R", 1+gen()%4));
        } else {
            slots.push_back(new ItemSlot("S", 6+gen()%3));
            slots.push_back(new ItemSlot("S", 7+gen()%3));
            slots.push_back(new ItemSlot("S", 8+gen()%3));
        }
        zerofill_and_shuffle(gen);
    }
};

struct WaterChest : Chest {
    WaterChest(int x, int y, int seed, std::vector<double> coral_biome, std::vector<double> coral_rarity) : Chest(x, y)
    {
        nslots = 20;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("S", 6+gen()%10));
        slots.push_back(new ItemSlot("Xx", 2+gen()%30));
        slots.push_back(new ItemSlot("-", gen()%20));
        slots.push_back(new ItemSlot("DD", gen()%2));
        slots.push_back(new ItemSlot("R", 2+gen()%2));
        if (gen()%10 < 5) {
            slots.push_back(new ItemSlot("S", 16+gen()%10));
            slots.push_back(new ItemSlot("S", 29+gen()%10));
        } else {
            slots.push_back(new ItemSlot("Xx", 2+gen()%10));
            slots.push_back(new ItemSlot("B", 2+gen()%10));
        }
        for (size_t i = 0; i < coral_biome.size(); i++) {
            double weight = coral_biome[i] + 0.2 - coral_rarity[i];
            if (weight > 0) {
                slots.push_back(new ItemSlot("X"+std::to_string(i), (int)(weight*5)+gen()%4));
            }
        }
        zerofill_and_shuffle(gen);
    }
};

struct FortChest : Chest {
    FortChest(int x, int y, int seed) : Chest(x, y)
    {
        nslots = 10;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("..", 10+gen()%11));
        slots.push_back(new ItemSlot("WI", 5+gen()%99));
        if (gen()%10 < 4) {
            slots.push_back(new ItemSlot("Dg", 10+gen()%21));
            slots.push_back(new GrinderItemSlot("G0", 1, 200+gen()%300));
        } else {
            slots.push_back(new ItemSlot("Dm", 2+gen()%18));
            slots.push_back(new GrinderItemSlot("GB", 1, 100+gen()%150));
        }
        int choice = gen()%10;
        if (choice < 1) {
            slots.push_back(new ItemSlot("Ws", 1));
        } else if (choice < 3) {
            slots.push_back(new ItemSlot("DD", 1));
        } else if (choice < 8) {
            slots.push_back(new ItemSlot("R", 1));
        } else {
            slots.push_back(new ItemSlot("#s", gen()%9));
        }
        zerofill_and_shuffle(gen);
    }
};

struct ShipChest : Chest {
    ShipChest(int x, int y, int seed) : Chest(x, y)
    {
        nslots = 30;
        std::mt19937_64 gen;
        gen.seed(seed);
        slots.push_back(new ItemSlot("Ha", 20+gen()%20));
        slots.push_back(new ItemSlot("Ha", 20+gen()%20));
        slots.push_back(new ItemSlot("Hb", 30+gen()%20));
        slots.push_back(new ItemSlot("Hb", 10+gen()%90));
        slots.push_back(new ItemSlot("||", 30+gen()%30));
        slots.push_back(new ItemSlot("-", gen()%20));
        if (gen()%2 == 0) {
            slots.push_back(new ItemSlot("fu", 10+gen()%40));
            slots.push_back(new ItemSlot("fa", 10+gen()%40));
            slots.push_back(new ItemSlot("Ab", 20+gen()%30));
            slots.push_back(new ItemSlot("Hb", 10+gen()%10));
            slots.push_back(new ItemSlot("R", gen()%3));
        } else {
            slots.push_back(new ItemSlot("fr", 10+gen()%40));
            slots.push_back(new ItemSlot("fl", 10+gen()%40));
            slots.push_back(new ItemSlot("||", 10+gen()%20));
            slots.push_back(new ItemSlot("||", 20+gen()%10));
            slots.push_back(new ItemSlot("Ha", 10+gen()%11));
        }
        if (gen()%10 < 4) {
            slots.push_back(new ItemSlot("DD", 1));
            slots.push_back(new ItemSlot("fu", 5+gen()%30));
            slots.push_back(new ItemSlot("fu", 5+gen()%20));
            slots.push_back(new ItemSlot("fr", 10+gen()%40));
            slots.push_back(new ItemSlot("fc", 5+gen()%15));
            slots.push_back(new ItemSlot("fc", 5+gen()%15));
        } else {
            slots.push_back(new ItemSlot("R", 1+gen()%3));
            slots.push_back(new ItemSlot("fl", 10+gen()%40));
            slots.push_back(new ItemSlot("fa", 5+gen()%40));
            slots.push_back(new ItemSlot("fa", 10+gen()%10));
            slots.push_back(new ItemSlot("-", gen()%20));
            slots.push_back(new ItemSlot("fc", 10+gen()%30));
        }
        zerofill_and_shuffle(gen);
    }
};

#endif /* TILES_CPP */
