// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

/* A simple tcp socket protocol. */

#include "tcp.h"
#include <string>
#include <cstdint>
#include <iostream>
#include <string.h>
#include <sys/socket.h>

Tcp::Tcp(int buffer_size) {
    m_buffer_size = buffer_size;
    m_buffer = new char[buffer_size+1]();
}

Tcp::~Tcp() {
    delete[] m_buffer;
}

uint64_t Tcp::extract_uint64(char *buff) {
    uint8_t *ubuff = (uint8_t*)buff;
    uint64_t value = 0;
    for (int i = 3; i >= 0; i--) {
        value = value<<8;
        value |= ubuff[i];
    }
    return value;
}

void Tcp::send_buffer(int fd, const char *buff, uint64_t buff_len) {
    // send the expected length of buffer
    send(fd, (char*)&buff_len, 4, 0);
    // and send that buffer
    send(fd, buff, buff_len, 0);
}

void Tcp::send_string(int fd, std::string buff) {
    uint64_t buff_len = buff.length();
    send_buffer(fd, buff.c_str(), buff_len);
}

const char* Tcp::receive(int fd) {
    // receive message length
    char buff4[4];
    bzero(buff4, 4);
    int numrec = recv(fd, buff4, 4, MSG_WAITALL);
    uint64_t recv_len = extract_uint64(buff4);

    if (recv_len+1 > m_buffer_size) return nullptr;

    // receive that many bytes from client
    bzero(m_buffer, recv_len+1);
    recv(fd, m_buffer, recv_len, MSG_WAITALL);
    return m_buffer;
}
