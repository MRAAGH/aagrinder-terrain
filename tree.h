// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TREE_H
#define TREE_H

#include <map>
#include <string>
#include <vector>
#include "block.h"
#include "structures/structures.h"

enum class TreeType {
    apple = 0,
    pine = 1,
    mountainpine = 2,
    apricot = 3,
    almond = 4,
    avocado = 5,
    dead = 6,
    tentacle = 7,
    avocadosuper = 8,
    palm = 9,
    oasistree = 10,
    bush = 100,
    cranberry = 101,
    blueberry = 102,
};

struct TreeProperties {
    bool leaves = true;
    Block grows_on = Block::d;
    Block optional_grows_on = Block::d;
    Block leaf_block;
    bool fruit = false;
    double fruit_tree_chance = 0;
    double fruit_chance = 0;
    Block fruit_block;
    Block wood_block;
    bool allow_overlap = false;
    bool generated = true;
    bool is_bush = false;
    std::vector<std::array<std::array<Block,13>,18>> presets;
    bool allow_mirroring = false;
    Block replace_50percent_with_air = Block::air;
    double wood_decay;
    double leaf_spread;
    double branching_ini = 0;
    double branching;
    double leaf_chance;
    double leaf_chance_top;
    bool vines = false;
    double vine_chance;
    int vine_length;
};

const std::map<TreeType, TreeProperties> tree_props = {

    { TreeType::pine, TreeProperties
        {
            .generated = false,
            .presets = PINE_STRUCTURES,
        }},

    { TreeType::mountainpine, TreeProperties
        {
            .optional_grows_on = Block::B,
            .generated = false,
            .presets = PINE_STRUCTURES,
        }},

    { TreeType::apple, TreeProperties
        {
            .leaf_block = Block::Aa,
            .fruit = true,
            .fruit_tree_chance = 0.5,
            .fruit_chance = 0.08,
            .fruit_block = Block::Fa,
            .wood_block = Block::Ha,
            .generated = true,
            .wood_decay = 0.125,
            .leaf_spread = 2.2,
            .branching_ini = 0,
            .branching = 0.14,
            .leaf_chance = 0.6,
            .leaf_chance_top = 0.9,
        }},

    { TreeType::apricot, TreeProperties
        {
            .leaf_block = Block::Ap,
            .fruit = true,
            .fruit_tree_chance = 0.33,
            .fruit_chance = 0.15,
            .fruit_block = Block::Fp,
            .wood_block = Block::Hp,
            .generated = true,
            .wood_decay = 0.15,
            .leaf_spread = 2.7,
            .branching_ini = -0.1,
            .branching = 0.2,
            .leaf_chance = 0.7,
            .leaf_chance_top = 1,
        }},

    { TreeType::almond, TreeProperties
        {
            .leaf_block = Block::Al,
            .fruit = true,
            .fruit_tree_chance = 0.9,
            .fruit_chance = 0.07,
            .fruit_block = Block::Fl,
            .wood_block = Block::Hl,
            .generated = true,
            .wood_decay = 0.2,
            .leaf_spread = 3.1,
            .branching_ini = 0.05,
            .branching = 0.07,
            .leaf_chance = 0.1,
            .leaf_chance_top = 1,
        }},

    { TreeType::avocado, TreeProperties
        {
            .leaf_block = Block::Av,
            .fruit = true,
            .fruit_tree_chance = 0.9,
            .fruit_chance = 0.02,
            .fruit_block = Block::Fv,
            .wood_block = Block::Hv,
            .allow_overlap = true,
            .generated = true,
            .wood_decay = 0.09,
            .leaf_spread = 4.2,
            .branching_ini = 0.4,
            .branching = 0.005,
            .leaf_chance = 0.7,
            .leaf_chance_top = 0.98,
            .vines = true,
            .vine_chance = 0.2,
            .vine_length = 4,
        }},

    { TreeType::avocadosuper, TreeProperties
        {
            .leaf_block = Block::Av,
            .fruit = true,
            .fruit_tree_chance = 0.5,
            .fruit_chance = 0.01,
            .fruit_block = Block::Fv,
            .wood_block = Block::Hv,
            .allow_overlap = true,
            .generated = true,
            .wood_decay = 0.025,
            .leaf_spread = 6.5,
            .branching_ini = 0.4,
            .branching = 0,
            .leaf_chance = 0.4,
            .leaf_chance_top = 0.98,
            .vines = true,
            .vine_chance = 0.2,
            .vine_length = 8,
        }},

    { TreeType::dead, TreeProperties
        {
            .grows_on = Block::sand,
            .generated = false,
            .presets = DEADTREE_STRUCTURES,
        }},

    { TreeType::palm, TreeProperties
        {
            .grows_on = Block::sand,
            .generated = false,
            .presets = PALM_STRUCTURES,
            .allow_mirroring = true,
            .replace_50percent_with_air = Block::Fm,
        }},

    { TreeType::oasistree, TreeProperties
        {
            .grows_on = Block::sand,
            .generated = false,
            .presets = OASISTREE_STRUCTURES,
        }},

    { TreeType::tentacle, TreeProperties
        {
            .grows_on = Block::sand,
            .generated = false,
            .presets = TENTACLE_STRUCTURES,
        }},

    { TreeType::bush, TreeProperties
        {
            .leaf_block = Block::Ab,
            .wood_block = Block::Hb,
            .is_bush = true,
        }},

    { TreeType::cranberry, TreeProperties
        {
            .leaf_block = Block::ar,
            .wood_block = Block::Hr,
            .is_bush = true,
        }},

    { TreeType::blueberry, TreeProperties
        {
            .leaf_block = Block::au,
            .wood_block = Block::Hu,
            .is_bush = true,
        }},

};

#endif /* TREE_H */
