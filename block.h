// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BLOCK_H
#define BLOCK_H

#include <map>
#include <string>

enum class Block {
    VOID = 999, // not a real block
    air = 0,
    B = 1,
    b = 2,
    d = 3,
    water = 4,
    DD = 5,
    R = 6,
    pinkB = 7,
    X = 9, // not a real block

    grass0 = 10,
    grass1 = 11,
    grass2 = 12,
    grass3 = 13,
    grass4 = 14,
    grass5 = 15,
    grass6 = 16,
    grass7 = 17,
    grass8 = 18,
    grass9 = 19,
    grass10 = 20,
    grass11 = 21,
    grass12 = 22,
    grass13 = 23,
    grass14 = 24,
    grass15 = 25,

    fakegrass = 26, // not a real block
    cave = 27, // not a real block
    fakeair = 28, // not a real block
    keepclear = 44, // air, used to prevent icicles etc.
    transform_into_b = 45, // will turn everything to b except air

    flowerstem = 29,
    flower0 = 30,
    flower1 = 31,
    flower2 = 32,
    flower3 = 33,
    flower4 = 34,
    flower5 = 35,
    flower6 = 36,

    wire0 = 50,
    wire1 = 51,
    wire2 = 52,
    wire3 = 53,
    wire4 = 54,
    wire5 = 55,
    wire6 = 56,
    wire7 = 57,
    wire8 = 58,
    wire9 = 59,
    wire10 = 60,
    wire11 = 61,
    wire12 = 62,
    wire13 = 63,
    wire14 = 64,
    wire15 = 65,

    X0 = 70,
    X1 = 71,
    X2 = 72,
    X3 = 73,
    X4 = 74,
    X5 = 75,
    X6 = 76,
    X7 = 77,
    X8 = 78,

    sign = 90,
    chest = 91,
    goldchest = 92,
    WW = 93,
    WB = 94,
    WI = 95,
    WT = 96,
    Fp = 97,
    Fl = 98,
    Fv = 99,
    Fa = 100,
    Fr = 101,
    Fu = 102,
    WA = 109,
    WO = 110,
    Fm = 111,
    pebble = 115,
    pond = 120, // special water (is checked for grass)
    cavepond = 121, // special water (dirt spawns here)
    fakegrasspond = 122, // special water (dirt spawns here)
    wannabetentacle = 170, // tentacle that only replaces air
    wannaberock = 171, // tentacle that turns to stone

    forg = 180,
    SSPOOKY = 181,
    stoneslab = 182,

    quicksand = 124,
    fakegrassquicksand = 125,

    Aa = 130,
    Ai = 131,
    Al = 132,
    Ap = 133,
    Av = 134,
    Ab = 135,
    ar = 136,
    au = 137,
    ab = 138,
    Am = 139,
    Ha = 140,
    Hi = 141,
    Hl = 142,
    Hp = 143,
    Hv = 144,
    Hb = 145,
    Hr = 146,
    Hu = 147,
    Hm = 148,
    // NOTE: IF YOU ADD MORE WOOD, YOU MUST UPDATE isWood()!
    // ALSO SAME FOR LEAVES!

    snow = 150,
    ice = 151,
    icicle = 152,
    iceblock = 153,
    snowblock = 154,
    sand = 155,

    cloud = 190,

    Dm = 201,
    Dg = 202,

    stick = 302,
    rstick = 303,
    lstick = 304,
};

inline bool isGrass(Block b) {
    return (int)b >= (int)Block::grass0
        && (int)b <= (int)Block::grass15;
}

inline bool isGrassf(Block b) {
    return ((int)b >= (int)Block::grass0
        && (int)b <= (int)Block::grass15)
        || b == Block::fakegrass;
}

inline bool isLeaf(Block b) {
    return (int)b >= (int)Block::Aa
        && (int)b <= (int)Block::Am;
}

inline bool isWood(Block b) {
    return (int)b >= (int)Block::Ha
        && (int)b <= (int)Block::Hm;
}

inline bool isAir(Block b) {
    return b == Block::air
        || b == Block::cave
        || b == Block::fakeair
        || b == Block::keepclear
        || b == Block::wannaberock;
}

inline bool isAirOrFakegrass(Block b) {
    return b == Block::air
        || b == Block::cave
        || b == Block::fakeair
        || b == Block::keepclear
        || b == Block::wannaberock
        || b == Block::fakegrass;
}

inline bool isWire(Block b) {
    return (int)b >= (int)Block::wire0
        && (int)b <= (int)Block::wire15;
}

inline bool isWater(Block b) {
    return b == Block::water
        || b == Block::pond
        || b == Block::cavepond
        || b == Block::quicksand
        || b == Block::fakegrassquicksand
        || b == Block::fakegrasspond;
}

inline bool isCoral(Block b) {
    return b == Block::X
        || b == Block::X0
        || b == Block::X1
        || b == Block::X2
        || b == Block::X3
        || b == Block::X4
        || b == Block::X5
        || b == Block::X6
        || b == Block::X7
        || b == Block::X8;
}

inline bool isGround(Block b) {
    return b == Block::d
        || b == Block::B
        || b == Block::b
        || b == Block::sand
        || b == Block::iceblock;
}

const std::map<Block, std::pair<std::string, std::string>> block_codes = {

    /* {block from enum class, {serial code, pretty character}}, */

    {Block::air, {" ", " "}},
    {Block::water, {"w", "w"}},
    {Block::B, {"B", "B"}},
    {Block::b, {"b", "b"}},
    {Block::d, {"d", "d"}},
    {Block::DD, {"DD", "D"}},
    {Block::Dg, {"Dg", "g"}},
    {Block::Dm, {"Dm", "m"}},
    {Block::R, {"R1", "R"}},
    {Block::pinkB, {"R0", "P"}},
    {Block::X, {"X0", "X"}},
    {Block::X0, {"X0", "X"}},
    {Block::X1, {"X1", "X"}},
    {Block::X2, {"X2", "X"}},
    {Block::X3, {"X3", "X"}},
    {Block::X4, {"X4", "X"}},
    {Block::X5, {"X5", "X"}},
    {Block::X6, {"X6", "X"}},
    {Block::X7, {"X7", "X"}},
    {Block::X8, {"X8", "X"}},
    {Block::grass0, {"-0", "─"}},
    {Block::grass1, {"-1", "╶"}},
    {Block::grass2, {"-2", "╵"}},
    {Block::grass3, {"-3", "╰"}},
    {Block::grass4, {"-4", "╴"}},
    {Block::grass5, {"-5", "─"}},
    {Block::grass6, {"-6", "╯"}},
    {Block::grass7, {"-7", "┴"}},
    {Block::grass8, {"-8", "╷"}},
    {Block::grass9, {"-9", "╭"}},
    {Block::grass10, {"-a", "│"}},
    {Block::grass11, {"-b", "├"}},
    {Block::grass12, {"-c", "╮"}},
    {Block::grass13, {"-d", "┬"}},
    {Block::grass14, {"-e", "┤"}},
    {Block::grass15, {"-f", "┼"}},
    {Block::fakegrass, {" ", " "}},
    {Block::cave, {" ", " "}},
    {Block::keepclear, {" ", " "}},
    {Block::transform_into_b, {"b", "b"}},
    {Block::flowerstem, {"*|", "│"}},
    {Block::flower0, {"*0", "*"}},
    {Block::flower1, {"*1", "*"}},
    {Block::flower2, {"*2", "\""}},
    {Block::flower3, {"*3", "”"}},
    {Block::flower4, {"*4", "*"}},
    {Block::flower5, {"*5", "^"}},
    {Block::flower6, {"*6", "`"}},
    {Block::wire0, {"=0", "─"}},
    {Block::wire1, {"=1", "╶"}},
    {Block::wire2, {"=2", "╵"}},
    {Block::wire3, {"=3", "└"}},
    {Block::wire4, {"=4", "╴"}},
    {Block::wire5, {"=5", "─"}},
    {Block::wire6, {"=6", "┘"}},
    {Block::wire7, {"=7", "┴"}},
    {Block::wire8, {"=8", "╷"}},
    {Block::wire9, {"=9", "┌"}},
    {Block::wire10, {"=a", "│"}},
    {Block::wire11, {"=b", "├"}},
    {Block::wire12, {"=c", "┐"}},
    {Block::wire13, {"=d", "┬"}},
    {Block::wire14, {"=e", "┤"}},
    {Block::wire15, {"=f", "┼"}},
    {Block::sign, {"si", "▀"}},
    {Block::chest, {"hh", "h"}},
    {Block::goldchest, {"hg", "h"}},
    {Block::WW, {"WW", "W"}},
    {Block::forg, {"WF", "F"}},
    {Block::SSPOOKY, {"Ws", "s"}},
    {Block::stoneslab, {"sB", "_"}},
    {Block::WB, {"WB", "W"}},
    {Block::WI, {"WI", "I"}},
    {Block::WT, {"WT", "T"}},
    {Block::WA, {"WA", "A"}}, // tentacle
    {Block::WO, {"WO", "O"}}, // grabber
    {Block::Fp, {"fp", "p"}}, // apricot
    {Block::Fl, {"fl", "l"}}, // almond
    {Block::Fv, {"fv", "v"}}, // avocado
    {Block::Fa, {"fa", "a"}}, // apple
    {Block::Fm, {"fm", "m"}}, // palm
    {Block::Fr, {"fr", "r"}}, // cranberry
    {Block::Fu, {"fu", "u"}}, // blueberry
    {Block::Aa, {"Aa", "A"}}, // apple leaves
    {Block::Ai, {"Ai", "A"}}, // pine leaves
    {Block::Al, {"Al", "A"}}, // almond leaves
    {Block::Ap, {"Ap", "A"}}, // apricot leaves
    {Block::Av, {"Av", "A"}}, // avocado leaves
    {Block::Am, {"Am", "A"}}, // palm leaves
    {Block::Ab, {"Ab", "A"}}, // bush leaves
    {Block::ar, {"ar", "A"}}, // cranberry leaves
    {Block::au, {"au", "A"}}, // blueberry leaves
    {Block::ab, {"ab", "A"}}, // bush leaves
    {Block::Ha, {"Ha", "H"}}, // apple wood
    {Block::Hi, {"Hi", "H"}}, // pine wood
    {Block::Hl, {"Hl", "H"}}, // almond wood
    {Block::Hp, {"Hp", "H"}}, // apricot wood
    {Block::Hv, {"Hv", "H"}}, // avocado wood
    {Block::Hm, {"Hm", "V"}}, // palm wood
    {Block::Hb, {"Hb", "H"}}, // bush wood
    {Block::Hr, {"Hr", "H"}}, // cranberry wood
    {Block::Hu, {"Hu", "H"}}, // blueberry wood
    {Block::pebble, {"..", "."}},
    {Block::fakeair, {"DD", "M"}},
    {Block::pond, {"w", "w"}},
    {Block::cavepond, {"w", "w"}},
    {Block::fakegrasspond, {"w", "w"}},
    {Block::snow, {"sn", "_"}},
    {Block::ice, {"Wi", "_"}},
    {Block::icicle, {"Wl", "L"}},
    {Block::iceblock, {"Wc", "▇"}},
    {Block::snowblock, {"sN", "▇"}},
    {Block::sand, {"S", "S"}},
    {Block::wannabetentacle, {"WA", "A"}},
    {Block::cloud, {"~", "~"}},
    {Block::quicksand, {"WS", "S"}},
    {Block::fakegrassquicksand, {"WS", "S"}},
    {Block::stick, {"||", "|"}},
    {Block::rstick, {"|/", "/"}},
    {Block::lstick, {"|\\\\", "\\"}},
    {Block::VOID, {"null", "▞"}},

};


#endif /* BLOCK_H */
