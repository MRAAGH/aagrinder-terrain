// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "treegenerator.h"
#include <iostream>
#include "parameters.cpp"
#include "structures/structures.h"

Treegenerator::Treegenerator()
    : gen(), dis(0.0, 1.0) {
        map = new std::vector<std::vector<Block>>(
                40,std::vector<Block>(39, Block::VOID));
        map2 = new std::vector<std::vector<Block>>(
                40,std::vector<Block>(39, Block::VOID));
    }

Treegenerator::~Treegenerator(){
    delete map;
    delete map2;
}

void Treegenerator::cellular1(
        std::vector<std::vector<Block>> *map,
        std::vector<std::vector<double>> &probs,
        Block wood_block
        ){
    for (int i = 2; i < 39; ++i) {
        for (int j = 1; j < 38; ++j) {
            int code = 0;
            if((*map)[i-1][j+1] != Block::VOID) code += 1;
            if((*map)[i-1][j+0] != Block::VOID) code += 2;
            if((*map)[i-1][j-1] != Block::VOID) code += 4;
            if((*map)[i-2][j+1] != Block::VOID) code += 8;
            if((*map)[i-2][j+0] != Block::VOID) code += 16;
            if((*map)[i-2][j-1] != Block::VOID) code += 32;
            double prob = probs[i][code];
            (*map)[i][j] = (dis(gen) < prob) ? wood_block : Block::VOID;
        }
    }
}

std::vector<std::vector<Block>>* Treegenerator::generate(
        TreeType type, uint64_t seed){

    gen.seed(seed);

    // special case for trees that are not generated:
    if (tree_props.at(type).generated == false) {
        // clear the previous content
        for (int i = 0; i < 39; i++) {
            for (int j = 0; j < 39; j++) {
                (*map)[i][j] = Block::VOID;
            }
        }
        int chosen = gen() % tree_props.at(type).presets.size();
        int h = tree_props.at(type).presets[chosen].size();
        int w = tree_props.at(type).presets[chosen][0].size();
        int offset_x = w / 2;
        bool allow_mirroring = tree_props.at(type).allow_mirroring;
        bool mirror = allow_mirroring ? gen() % 2 : false;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                Block there = tree_props.at(type).presets[chosen][i][j];
                if (there == Block::VOID) continue;
                if (there == tree_props.at(type).replace_50percent_with_air) {
                    // 50% chance this will be replaced with air
                    if (gen()%2) {
                        there = Block::air;
                    }
                }
                // calculate position (potentially mirrored)
                int xpos = mirror ? w-1-j+18-offset_x : j+18-offset_x;
                int ypos = h-1-i;
                (*map)[ypos][xpos] = there;
            }
        }
        return map;
    }

    // special case for bushes
    if (tree_props.at(type).is_bush == true) {
        for (int i = 0; i < 39; i++) {
            for (int j = 0; j < 39; j++) {
                (*map)[i][j] = Block::VOID;
            }
        }
        int chosen = gen() % BUSH_STRUCTURES.size();
        auto bushmap = BUSH_STRUCTURES[chosen];
        int h = bushmap.size();
        int w = bushmap[0].size();
        int offset_x = w / 2;

        // place the structure
        for (size_t i = 0; i < bushmap.size(); i++) {
            for (size_t j = 0; j < bushmap[0].size(); j++) {
                Block newblock = bushmap[i][j];
                if (newblock == Block::VOID) continue;
                if (newblock == Block::Aa) {
                    newblock = tree_props.at(type).leaf_block;
                }
                (*map)[h-1-i][j+18-offset_x] = newblock;
            }
        }

        // place wood in middle
        (*map)[h-3][18] = tree_props.at(type).wood_block;

        return map;
    }

    // everything else here is for generated trees:

    // fruit on this tree?
    bool yes_fruit = false;
    if (tree_props.at(type).fruit) {
        if (dis(gen) < tree_props.at(type).fruit_tree_chance) {
            yes_fruit = true;
        }
    }

    // initialize the bottom row of the tree map
    (*map)[1] = std::vector<Block>(39, Block::VOID);
    (*map)[1][18] = tree_props.at(type).wood_block;

    // this holds the cellular automata probs, specific to every step:
    std::vector<std::vector<double>> probs(40, std::vector<double>(64, 0.0));

    // first level probabilities
    // this is not generated. I wrote it by hand.
    probs[2] = std::vector<double> {
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0, 0.0, 1.4, 666, 0.0, 0.0, 666, 0.0, // ...
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0,-0.2, 1.4, 1.4, 0.0, 0.0, 0.6, 0.0, // ..H
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0,-0.6, 1.4, 0.2,-0.6, 0.0, 0.2, 0.0, // .H.
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0,-0.6, 1.4, 0.2,-0.6, 0.0, 0.0, 0.0, // .HH
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0, 0.0, 1.4, 0.6,-0.2, 0.0, 1.4, 0.0, // H..
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0,-0.4, 1.4, 0.6,-0.4, 0.0, 0.6, 0.0, // H.H
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0,-0.6, 1.4, 0.0,-0.6, 0.0, 0.2, 0.0, // HH.
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
        0.0,-0.6, 1.4, 0.2,-0.6, 0.0, 0.2, 0.0, // HHH
    //  ...  ..H  .H.  .HH  H..  H.H  HH.  HHH     2nd
    };

    // and there is a thing that changes initial branching.
    // apply it now:
    for (int j = 0; j < 8; j++) {
        probs[2][8*j+1] += tree_props.at(type).branching_ini;
        probs[2][8*j+4] += tree_props.at(type).branching_ini;
    }

    // probabilities at other levels are gradually smaller:
    for (int i = 3; i < 39; ++i) {
        for (int j = 0; j < 64; j++) {
            // gradual decrease
            probs[i][j] = probs[i-1][j] - tree_props.at(type).wood_decay;
        }
    }

    // the probability of branchihg actually
    // gradually increases in higher levels:
    for (int i = 3; i < 39; ++i) {
        for (int j = 0; j < 8; j++) {
            // gradual increase
            // this is for the right side, which is index 8*j+1
            probs[i][8*j+1] = probs[i-1][8*j+1]+tree_props.at(type).branching;
            // and left too (index 8*j+4)
            probs[i][8*j+4] = probs[i-1][8*j+4]+tree_props.at(type).branching;
            // probability is still limited by the wood decay,
            // represented by the value in probs[i,2]
            if(probs[i][8*j+1] > probs[i][2]) {
                // limit exceeded, we need to fix
                probs[i][8*j+1] = probs[i][2];
            }
            // and same for left side
            if(probs[i][8*j+4] > probs[i][2]) {
                probs[i][8*j+4] = probs[i][2];
            }
        }
    }

    // generate H
    cellular1(map, probs, tree_props.at(type).wood_block);

    // clear leaves from previous generated tree
    std::fill(map2->begin(), map2->end(), std::vector<Block>(39, Block::VOID));

    // generate A
    for (int i = 3; i < 39; ++i) {
        for (int j = 1; j < 38; ++j) {
            if((*map)[i][j] == tree_props.at(type).wood_block
                    && (*map)[i+1][j] == Block::VOID
              ){
                double chance = tree_props.at(type).leaf_chance;
                if((*map)[i+1][j-1] == Block::VOID
                        && (*map)[i+1][j+1] == Block::VOID
                  ){
                    // this is top, so higher chance:
                    chance = tree_props.at(type).leaf_chance_top;
                }
                // roll for leaves:
                if(dis(gen) < chance){

                    // place some leaves in all 6 directions

                    int l1 = int(dis(gen)+tree_props.at(type).leaf_spread)+1;
                    int l2 = int(dis(gen)+tree_props.at(type).leaf_spread)+1;
                    int l3 = int(dis(gen)+tree_props.at(type).leaf_spread)+1;
                    int l4 = int(dis(gen)+tree_props.at(type).leaf_spread)+1;
                    int l5 = int(dis(gen)+tree_props.at(type).leaf_spread)+1;
                    int l6 = int(dis(gen)+tree_props.at(type).leaf_spread)+1;

                    // bunch of bad situations
                    if(j-l1 < 0) continue;
                    if(j-l2 < 0) continue;
                    if(j-l3 < 0) continue;
                    if(j-l4 < 0) continue;
                    if(j-l5 < 0) continue;
                    if(j-l6 < 0) continue;
                    if(j+l1 > 38) continue;
                    if(j+l2 > 38) continue;
                    if(j+l3 > 38) continue;
                    if(j+l4 > 38) continue;
                    if(j+l5 > 38) continue;
                    if(j+l6 > 38) continue;

                    for (int l = 0; l < l1; ++l) (*map2)[i+1][j+l] = tree_props.at(type).leaf_block;
                    for (int l = 0; l < l2; ++l) (*map2)[i+1][j-l] = tree_props.at(type).leaf_block;
                    for (int l = 0; l < l3; ++l) (*map2)[i][j+l+1] = tree_props.at(type).leaf_block;
                    for (int l = 0; l < l4; ++l) (*map2)[i][j-l-1] = tree_props.at(type).leaf_block;
                    for (int l = 0; l < l5; ++l) (*map2)[i-1][j+l] = tree_props.at(type).leaf_block;
                    for (int l = 0; l < l6; ++l) (*map2)[i-1][j-l] = tree_props.at(type).leaf_block;

                }
            }
        }
    }

    if(tree_props.at(type).vines) {
        // generate vines
        for (int i = 4; i < 37; ++i) {
            for (int j = 1; j < 38; ++j) {
                if((*map2)[i][j] == tree_props.at(type).leaf_block
                        && (*map2)[i-1][j] == Block::VOID
                        && (*map2)[i-1][j] == Block::VOID
                  ){
                    // roll for vine:
                    if(dis(gen)*2 < tree_props.at(type).vine_chance){
                        // place vine

                        // determine vine length
                        int l1 = int(dis(gen)+tree_props.at(type).vine_length);
                        // if vine goes beyond tree map, cut it
                        if(l1 > i) l1 = i;

                        for (int l = 0; l < l1; ++l) {
                            if((*map2)[i-l][j] == Block::VOID){
                                // block │
                                (*map2)[i-l][j] = Block::grass10;
                            }
                        }


                        // roll for half-block at the end
                        if(dis(gen) < 0.5){
                            if((*map2)[i-l1][j] == Block::VOID){
                                // block ╵
                                (*map2)[i-l1][j] = Block::grass2;
                            }
                        }

                    }
                }
            }
        }
    }

    // merge
    for (int i = 1; i < 39; ++i) {
        for (int j = 1; j < 38; ++j) {
            if((*map)[i][j] != Block::VOID){
                (*map2)[i][j] = (*map)[i][j];
            }
        }
    }

    // add fruit
    if (yes_fruit) {
        // I intentionally reversed this loop
        // so the fruit can not generate right
        // above one another
        for (int i = 37; i >= 0; --i) {
            for (int j = 1; j < 38; ++j) {
                if((*map2)[i][j] == tree_props.at(type).leaf_block
                        && ((*map2)[i+1][j] == tree_props.at(type).leaf_block
                        || (*map2)[i+1][j] == tree_props.at(type).wood_block)
                        ){
                    if (dis(gen) < tree_props.at(type).fruit_chance) {
                        // make it a fruit:
                        (*map2)[i][j] = tree_props.at(type).fruit_block;
                        // skip next block, to avoid adjacent fruits:
                        j++;
                    }
                }
            }
        }
    }

    return map2;
}

