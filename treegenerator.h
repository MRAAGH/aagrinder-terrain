// Copyright (C) 2019, 2020 MRAAGH

/* This file is part of AAGRINDER-terrain.
 * 
 * AAGRINDER-terrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER-terrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER-terrain.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TREEGENERATOR_H
#define TREEGENERATOR_H

#include <random>
#include <vector>
#include "block.h"
#include "tree.h"

class Treegenerator
{
    private:
        std::mt19937_64 gen;
        std::uniform_real_distribution<double> dis;
        std::vector<std::vector<Block>> *map, *map2;

        void cellular1(
                std::vector<std::vector<Block>> *map,
                std::vector<std::vector<double>> &probs,
                Block wood_block
                );

    public:
        Treegenerator();
        virtual ~Treegenerator();

        /* void determine_parameters(int x, int y); */

        std::vector<std::vector<Block>>* generate(TreeType type, uint64_t seed);
};

#endif /* TREEGENERATOR_H */
